# Diagramas de actividades

##### Diagrama 1 - Inicio de sesión y registro de usuario

|!["Diagrama de actividades: inicio de sesión"](./images/Diagramas_de_actividades/iniciar_sesion.png)|
|:-:|
|Imagen 1: Imagen que detalla el flujo de inicio de sesión y registro de un usuario.|

##### Diagrama 2 - Agregar autos

|!["Diagrama de actividades: registrar autos"](./images/Diagramas_de_actividades/agregar_autos.png)|
|:-:|
|Imagen 2: Imagen que muestra el proceso para registrar un nuevo auto.|

##### Diagrama 3 - Registro de usuario administrador

|!["Diagrama de actividades: agregar administrador"](./images/Diagramas_de_actividades/agregar_administradores.png)|
|:-:|
|Imagen 3: Imagen que detalla el flujo de registro de un nuevo usuario de tipo administrador.|

|!["Diagrama de actividades: rellenar esquema de vacunación"](./images/Diagramas_de_actividades/pasaporte_covid.png)|
|:-:|
|Imagen 4: Imagen que detalla el proceso para agregar el esquema de vacunación del usuario y si se desea obtener un certificado si el esquema fue aceptado.|

|!["Diagrama de actividades: registrar tarjeta de crédito"](./images/Diagramas_de_actividades/registrar_tarjeta.png)|
|:-:|
|Imagen 5: Imagen que detalla el proceso para registrar una tarjeta de crédito y verificar si fue aceptada por el banco.|

|!["Diagrama de actividades: realizar reservación"](./images/Diagramas_de_actividades/reservacion.png)|
|:-:|
|Imagen 6: Imagen que detalla el proceso para realizar una reservación.|
