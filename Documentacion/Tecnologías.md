# Tecnologías a Utilizar

## Frontend

### ReactJS

React es una biblioteca Javascript de código abierto diseñada para crear interfaces de usuario con el objetivo de facilitar el desarrollo de aplicaciones en una sola página. Es mantenido por Facebook y la comunidad de software libre.

## Backend

### Python con Flask

Flask es un framework minimalista escrito en Python que permite crear aplicaciones web rápidamente y con un mínimo número de líneas de código. Está basado en la especificación WSGI de Werkzeug y el motor de templates Jinja2 y tiene una licencia BSD.

### Golang

Para el desarrollo de backend se propone el lenguaje golang un lenguaje robusto que obrede una gran documentación que puede ser de ayuda para el desarrollo de la aplicación. Este lenguaje ofrece un gran rendimiento a diferencia de sus competidores, ayuda a administrar la memoria y posee la agilidad para procesar multiples solicitudes de forma eficiente funcionando de forma más eficiente y reduciendo costos.

## Base de Datos

### MySQL

MySQL es un sistema de gestión de bases de datos relacionales (RDBMS) de código abierto respaldado por Oracle y basado en el lenguaje de consulta estructurado (SQL). MySQL se basa en un modelo cliente-servidor. El núcleo de MySQL es el servidor MySQL, que maneja todas las instrucciones (o comandos) de la base de datos. El servidor MySQL está disponible como un programa independiente para su uso en un entorno de red cliente-servidor y como una biblioteca que puede ser incrustada (o enlazada) en aplicaciones independientes.


### Docker

Docker es un sistema operativo para contenedores. De manera similar a cómo una máquina virtual virtualiza (elimina la necesidad de administrar directamente) el hardware del servidor, los contenedores virtualizan el sistema operativo de un servidor.

### React Native

React Native es un framework JavaScript para crear aplicaciones reales nativas para iOS y Android, basado en la librearía de JavaScript React para la creación de componentes visuales, cambiando el propósito de los mismos para, en lugar de ser ejecutados en navegador, correr directamente sobre las plataformas móviles.

### Gitlab

GitLab es una suite completa que permite gestionar, administrar, crear y conectar los repositorios con diferentes aplicaciones y hacer todo tipo de integraciones con ellas, ofreciendo un ambiente y una plataforma en cual se puede realizar las varias etapas de su SDLC/ADLC y DevOps.

### Terraform

Terraform es una herramienta de orquestación de código abierto desarrollado por Hashicorp que nos permite definir nuestra infraestructura como código, esto quiere decir que es posible escribir en un fichero de texto la definición de nuestra infraestructura usando un lenguaje de programación declarativo y simple.

### Kubernetes
Teniendo en cuenta que se utilizara docker para contener los servicios desarrollados, se utilizará kubernetes, ya que este es un orquestador de contenedores, esto quiere decir que administrara la infraestructura de la aplicación facilitando el despliegue de contenedores.

### Jenkins
Se propone utilizar Jenkins para la integración continua que permitira agilizar el proceso de desarrollo del sistema.

### Google Cloud plattform

VM en google cloud platform , para levantar micro servicios y levantar frontend.