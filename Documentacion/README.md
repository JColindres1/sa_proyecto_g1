# Documentación


## Indice

1. [Analisis UX & UI](./AnalisisUX%26UI.md)
2. [Arquitectura del sistema](./Arquitectura.md)
3. [Contratos de servicio](./Contratos.md)
4. [Diagramas de actividades](./Diagramas.md)
5. [Diagrama de almacenamiento](./Diagrama_de_almacenamiento.md)
6. [Historias de usuario](./Historias.md)
7. [Indicadores](./Indicadores.md)
8. [Listado de microservicios](./Microservicios.md)
9. [Metodología a utilizar para el desarrollo del sistema](./Metodologia.md)
10. [Mockups](./Mockups.md)
11. [Modelo de branching](./Branching.md)
12. [Pipelines](./Pipelines.md)
13. [Requerimientos](./Requerimientos.md)
14. [Tecnologias a utilizar](./Tecnolog%C3%ADas.md)
15. [Seguridad de la aplicacion](./Seguridad.md)