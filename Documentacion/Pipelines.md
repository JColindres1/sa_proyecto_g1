# Pipelines

![title](images/pipeline.PNG)


## Build
Permite realizar la construcción de cada proyecto y la generación de los contenedores respectivos.

- servicio_hotel
- front_end
- servicio_vuelos
- servicio_renta
- servicio_banco
- servicio_resultados
- servicio_login
- servicio_registro
- servicio_esb

## Test 
Cada servicio tendría su respectivo test del proyecto para la validación de que cada función.

## Upimages 
En este stage se suben las imagenes al repositorio de contenedores


## Pullimages
En este stege se obtienen las imagenes y son deployadas en lás maquinas correspondientes

## Deploy 
Se publicarán las nuevas actualizaciones a producción.