# Requerimientos

## Funcionales

- El usuario será capaz de gestionar su viaje, estadía y transporte en el sistema.
- El sistema contará con diferentes tipos de usuario: Turista, Hotel, Renta Auto. Donde dependiendo de cada tipo de usuario se le asignará un rol específico. Así para poder llevar un mejor manejo de acceso a los usuarios.
- El sistema manejara 3 roles que serán: turista, servicios tercerizados y administrador. Donde cada rol tendrá que manejar diferentes permisos en el sistema.
- El usuario podrá iniciar sesión y el sistema luego de reconocer a partir de sus credenciales ingresadas, reconocerá su rol en el sistema y le dará el acceso a las vistas correspondietes dependiendo su rol.
- El usuario será capaz de poder registrarse al sistema asignandose el rol de turista o servicio tercerizado.
- El usuario administrador podrá crear otras cuentas tipo de administrador.
- Los usuarios de tipo de hotel tendrán la capacidad de crear un calendario con las reservaciones que dispondrán a los turistas.
- Los usuarios tipo turista podrán realizar búsqueda de hoteles por diferentes filtros, tales como: país, ciudad, cantidad de personas y rango de precios y/o fechas.
- Las reservaciones que quiera hacer un usuario deberá presentar una tarjeta de crédito.
- El usuario de tipo de renta de autos podrá registar los datos de vehículos a rentar ya sea de forma individual o de forma masiva.
- El usuario turista podrá reservar el vehículo a rentar o rentarlo desde ese momentos, realizando el pago correspondiente con su tarjeta de crédito.
- El sistema llevará control del pasaporte COVID, manejando la informacion de vacuna administrada, cuántas dosis del esquema se han colocado y las fechas de vacunacion.
- El sistema generará un certificado digital con código QR para validar la información que ingreso el usuario.
- Se podrán registrar la información de vuelos ya se de forma manual o de forma masiva.
- El sistema realizará las verificaciones necesarias para que los turistas puedan unicamente ir en vuelos donde la vacuna exigida por el país sea válida y que el esquema de vacunación este completo.
- El sistema manejará 2 tipos de vuelos unicamente: sólo de ida o ida y vuelta.
- El sistema validará que las tarjetas de crédito registradas son emitidas por un banco y tenga los datos necesarios (nombre, fecha de vencimiento y CVV).
- Al realizar cualquier tipo de transacción el sistema validará que el usuario cuente con el saldo dispobnible.
- El banco cobrará 4% por cada transacción realizada en la aplicación.
- El usuario tendrá la capacidad de calificar alguno de los servicios que utilizó (vuelos, reservaciones de hoteles y autos) dejando un comentario respecto al servicio utilizado.
- Las reseñas se deben de poder ver para cualquier turista que este utilizando la aplicación.
- El administrador podrá ver de forma general las reseñas por cada negocio registrado al sistema.
- El administrador tendrá un módulo en específico para ver resultados, reflejando la informacion con indicadores clave. Esto con auyuda de gráficas e información obtenida en el sistema.
- La aplicación será utilizada será una palicación web y aplicación móvil con funcionalidades similares.
- La aplicación móvil tendrá un inicio de sesión con la misma funcionalidad de la página web.
- La aplicación móvil sera unicamente para los turistas.
- En la aplicación móvil el turista podrá realizar: renta de autos, reservaciones de hoteles, compra de vuelos y generar y ver reseñas. Esto de la misma forma que en la aplicación web.
- El usuario turista podrá ver un historial de el uso a los servicios que ha utilizado.
- El usuario de servicios tercerizado podrá ver el historial de usuarios que han reservado sus hoteles, rentado sus vehiculos.


## No Funcionales

- Al momento de iniciar sesión el sistema generará un sistema que garantice la identidad del usuario y que no pueda realizar alguna función cuando no sea quien dice ser.
- Se manejarán los datos de las transacciones de forma confidencial y segura para que no se compartan los datos de tarjetas de crédito o transacciones con nadie.
- Los servicios de reserva de hoteles y renta de vehiculos estarán siempre disponibles, para que el usuario puede hacer uso de estos a cualquier momento del día.
- El sistema será capaz de manejar reservas al mismo tiempo por diferentes usuarios a una misma reservación de hotel o renta de vehículo y deberá realizar la transacción a la primera petición recibida. Notificando a ambos usuarios si se relaizo transacción o falló.
- El sistema al momento de fallo en una transacción se detendrá y regresará la transacción por completo para que se realice de nuevo y de forma segura, sin realizar ningún cobro extra y notificando al usuario el problema.
- El sistema tendrá una intefaz amigable y deducible para su fácil utilización para los usuarios.

## Casos de Uso

<table>
    <thead>
        <tr>
            <td><b>001</b></td>
            <td colspan="2"><b>Registro de usuario</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá registrarse al sistema indicando el rol que manejara siendo (turista o servicio tercederizado)</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Ingresa usuario</li>
                    <li>Ingresa nombre</li>
                    <li>Ingresa correo electronico</li>
                    <li>Ingresa contraseña</li>
                    <li>Confirma contraseña</li>
                    <li>Pulsar boton de registar</li>
                    <li>Validar datos que sean puedan ser registrados</li>
                    <li>Registar datos al sistema</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            <tr>
                <td></td>
                <td>Datos del usuario ya registrados</td>
                <td>Indicar al usuario que ya existen datos registrados</td>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo registrar</td>
            </tr>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <td><b>002</b></td>
            <td colspan="2"><b>Login</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá iniciar sesión y el sistema identificara su rol y le otorgara acceso unicamente a los modulo que tenga permitido según su rol</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Ingresa usuario</li>
                    <li>Ingresa contraseña</li>
                    <li>Pulsa boton de iniciar sesion</li>
                    <li>Sistema verifica sus credenciales</li>
                    <li>El sistema responde con el rol que tiene asignado el usuario</li>
                    <li>La aplicacion le muestra los modulos a los que tiene acceso unicamente.</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            <tr>
                <td></td>
                <td>Datos del usuario incorrenctos</td>
                <td>Indicar al usuario que la autenticación no fue válida. Vuele a intentear</td>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo iniciar sesión</td>
            </tr>
        </tr>
    </tbody>
</table>

![Caso de Uso](./images/caso1.png)

<table>
    <thead>
        <tr>
            <td><b>003</b></td>
            <td colspan="2"><b>Reservación de hotel</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá realizar la reservacion de un hotel y se relizará el cobro de dicha reservación</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Ve listado de hoteles disponibles</li>
                    <li>Selecciona el hotel al cual desea realizar la reservación</li>
                    <li>Ve las reseñas anteriores de otros usuarios</li>
                    <li>Le da click a Realizar reservación</li>
                    <li>Le mostrara las fechas disponibles para que pueda ser resevado</li>
                    <li>Selecciona las fechas de reservación que desee</li>
                    <li>Le a click a Confirmar Reservación</li>
                    <li>El sistema solicitará información de una tarjeta de crédito</li>
                    <li>El usuario llenará los datos solicitados</li>
                    <li>Le da click al boton de confirmar</li>
                    <li>El sistema guarda la reservacion y ajusta el calendario del hotel</li>
                    <li>Se le notifica al usuario que la reservacion fue realizada con éxito.</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            <tr>
                <td></td>
                <td>Datos del usuario de tarjeta incorrecto</td>
                <td>Indicar al usuario que la validacion de la tarjeta sea incorrecta. Vuele a intentear</td>
            </tr>
            <tr>
                <td></td>
                <td>Reservacion en paralelo con otro usuario</td>
                <td>Realizar la reservación al usuario que la haya hecho primero.Indicar al usuario que la reservación ya fue realizada.</td>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo realidar la reservación</td>
            </tr>
        </tr>
    </tbody>
</table>

![Caso de Uso](./images/caso2.png)

<table>
    <thead>
        <tr>
            <td><b>004</b></td>
            <td colspan="2"><b>Crear calendario de reservaciones</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá realizar creacion de un calendario con fechas disponibles para realizar reservaciones</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Servicio tercidizado(hotel)</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Click en agregar calendario</li>
                    <li>Ingresar fecha</li>
                    <li>Indicar si es valida para reservacion</li>
                    <li>Dar click en boton para guardar fecha</li>
                    <li>Guardar fecha en base de datos</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            <tr>
                <td></td>
                <td>Ingresar los datos de forma masiva</td>
                <td>El sistema leera el archivo y lo interpretara con la informacion dentro del mismo para guardar diferentes fechas</td>
            </tr>
            <tr>
                <td></td>
                <td>Error al guardar fecha de calendario</td>
                <td>Indicar al usuario que la fecha no pudo ser guardada.</td>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo realizar la operacion</td>
            </tr>
        </tr>
    </tbody>
</table>

![Caso de Uso](./images/caso3.png)

<table>
    <thead>
        <tr>
            <td><b>005</b></td>
            <td colspan="2"><b>Subir vehiculos a rentar</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá realizar creacion de un auto para realizar reservaciones</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Servicio tercidizado(reservacion de autos)</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Click en agregar auto</li>
                    <li>Ingresar datos del vehiculo</li>
                    <li>Dar click en boton para guardar vehiculo</li>
                    <li>Guardar auto en base de datos</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            <tr>
                <td></td>
                <td>Ingresar los datos de forma masiva</td>
                <td>El sistema leera el archivo y lo interpretara con la informacion dentro del mismo para guardar diferentes vehiculos</td>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo realizar la operacion</td>
            </tr>
        </tr>
    </tbody>
</table>

![Caso de Uso](./images/caso4.png)

<table>
    <thead>
        <tr>
            <td><b>006</b></td>
            <td colspan="2"><b>Reservación de auto</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá realizar la reservacion de un hotel y se relizará el cobro de dicha reservación</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Ve listado de autos disponibles</li>
                    <li>Selecciona el auto al cual desea realizar la reservación</li>
                    <li>Ve las reseñas anteriores de otros usuarios</li>
                    <li>Le da click a Realizar reservación</li>
                    <li>Le mostrara las fechas disponibles para que pueda ser resevado</li>
                    <li>Selecciona las fechas de reservación que desee</li>
                    <li>Le a click a Confirmar Reservación</li>
                    <li>El sistema solicitará información de una tarjeta de crédito</li>
                    <li>El usuario llenará los datos solicitados</li>
                    <li>Le da click al boton de confirmar</li>
                    <li>El sistema guarda la reservacion y ajusta el calendario del hotel</li>
                    <li>Se le notifica al usuario que la reservacion fue realizada con éxito.</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            <tr>
                <td></td>
                <td>Datos del usuario de tarjeta incorrecto</td>
                <td>Indicar al usuario que la validacion de la tarjeta sea incorrecta. Vuele a intentear</td>
            </tr>
            <tr>
                <td></td>
                <td>Reservacion en paralelo con otro usuario</td>
                <td>Realizar la reservación al usuario que la haya hecho primero. Indicar al usuario que la reservación ya fue realizada.</td>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo realidar la reservación</td>
            </tr>
        </tr>
    </tbody>
</table>

![Caso de Uso](./images/caso5.png)

<table>
    <thead>
        <tr>
            <td><b>007</b></td>
            <td colspan="2"><b>Registro de pasaporte COVID</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá realizar el registro de los datos de vacunacion contra COVID-19</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Ingresa al apartado de registro de pasaporte COVID</li>
                    <li>Ingresa los datos de la vacuna</li>
                    <li>Ingresa dosis y fecha que fue aministrada</li>
                    <li>Da click en guardar pasaporte</li>
                    <li>EL sistema genera un certificado con código QR para validar información</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo realidar la reservación</td>
            </tr>
        </tr>
    </tbody>
</table>

![Caso de Uso](./images/caso6.png)

<table>
    <thead>
        <tr>
            <td><b>008</b></td>
            <td colspan="2"><b>Subir vuelor</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá realizar creacion de vuelo</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Servicio tercidizado(hotel)</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Click en agregar vuelo</li>
                    <li>Ingresar datos del vuelo</li>
                    <li>Ingresar indicar si es vuelo de ida o ida y vuelta</li>
                    <li>Dar click en boton para guardar vehiculo</li>
                    <li>Guardar auto en base de datos</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            <tr>
                <td></td>
                <td>Ingresar los datos de forma masiva</td>
                <td>El sistema leera el archivo y lo interpretara con la informacion dentro del mismo para guardar diferentes vuelos</td>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo realizar la operacion</td>
            </tr>
        </tr>
    </tbody>
</table>

![Caso de Uso](./images/caso7.png)

<table>
    <thead>
        <tr>
            <td><b>009</b></td>
            <td colspan="2"><b>Reservación de vuelos</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá realizar la reservacion de un vuelo y se relizará el cobro de dicha reservación</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Ve listado de vuelos disponibles</li>
                    <li>Selecciona el vuelo al cual desea realizar la reservación</li>
                    <li>Ve las reseñas anteriores de otros usuarios</li>
                    <li>Le da click a Realizar reservación</li>
                    <li>Selecciona las fechas de vuelo que desee</li>
                    <li>Le a click a Confirmar Reservación</li>
                    <li>El sistema solicitará información de una tarjeta de crédito</li>
                    <li>El usuario llenará los datos solicitados</li>
                    <li>Le da click al boton de confirmar</li>
                    <li>El sistema guarda la reservacion y ajusta el cantidad de pasajeros del vuelo</li>
                    <li>Se le notifica al usuario que la reservacion fue realizada con éxito.</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            <tr>
                <td></td>
                <td>Datos del usuario de tarjeta incorrecto</td>
                <td>Indicar al usuario que la validacion de la tarjeta sea incorrecta. Vuele a intentear</td>
            </tr>
            <tr>
                <td></td>
                <td>Reservacion en paralelo con otro usuario y vuelo lleno</td>
                <td>Realizar la reservación al usuario que la haya hecho primero.Indicar al usuario que la reservación ya fue realizada.</td>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo realidar la reservación</td>
            </tr>
        </tr>
    </tbody>
</table>

![Caso de Uso](./images/caso8.png)

<table>
    <thead>
        <tr>
            <td><b>010</b></td>
            <td colspan="2"><b>Realizar una reseña</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá realizar reseña acerca de un servicio utilizado</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Ve listado de algun servicio seleccionado disponibles</li>
                    <li>Selecciona el servicio</li>
                    <li>Da click en realizar reseña</li>
                    <li>El sistema verifica si puede realizar la reseña</li>
                    <li>Agrega valoracion y comentario a reseña</li>
                    <li>Le a click a Confirmar</li>
                    <li>Se le notifica al usuario que la reseña fue realizada con éxito.</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            <tr>
                <td></td>
                <td>El usuario no ha utilizado el servicio</td>
                <td>Indicar al usuario que no puede realizar la reseña a un servicio que no utilizó</td>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo realidar la reseña</td>
            </tr>
        </tr>
    </tbody>
</table>

![Caso de Uso](./images/caso9.png)

<table>
    <thead>
        <tr>
            <td><b>011</b></td>
            <td colspan="2"><b>Ver resultados</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá observar de forma resumida los datos del sistema</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Administrador</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Ingresar al apartado de resultados</li>
                    <li>El sistema generará de forma resumidas datos del sistema</li>
                    <li>La aplicacion mostrara los datos recopilados de forma gráfica</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo realizar la recopilacion de datos</td>
            </tr>
        </tr>
    </tbody>
</table>

![Caso de Uso](./images/caso11.png)

<table>
    <thead>
        <tr>
            <td><b>012</b></td>
            <td colspan="2"><b>Ver historial</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">El usuario podrá ver historial de los servicios utilizados</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Selecciona apartado de historial de usos</li>
                    <li>El sitema desplegara un listado con todos los servicios utilizados por fecha</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td><b>Caso</b></td>
                <td><b>Acciones</b></td>
            </tr>
            <tr>
                <td></td>
                <td>Problema con el servidor</td>
                <td>Indicar al usuario que por un problema en el servidor no se pudo realidar la reservación</td>
            </tr>
        </tr>
    </tbody>
</table>

![Caso de Uso](./images/caso12.png)
