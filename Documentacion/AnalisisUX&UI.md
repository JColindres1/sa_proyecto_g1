## Universidad de San Carlos de Guatemala
## Facultad de ingeniería
## Escuela de ciencias y sistemas
## Software Avanzado

# Análisis UX/UI

## UX

### Web

##### Utilizable

La aplicación web tiene el propósito de facilitar en el caso de los usuarios de tipo turista puedan ver a detalle todos los servicios que estan disponibles, viendo toda su información de forma clara y ordena y con la posibilidad de realizar reservaciones en el momento que deseen. De forma que obtengan una respuesta rápida a su solicitud. Con la finalidad de que el usuario tenga una experiencia agradable se planea que la navegación sea intuitiva y el contenido sea claro y visible para todos los usuarios. 

En el caso de los usuarios de tipo servicio, tendrán la posibilidad de poder ver su información, crear un calendario con distintas reservaciones para los usuarios. Estas reservaciones se podrán crear en solo unos instantes y colocando unicamente la información necesaria e imagenes para mostrar a los usuarios el atractivo del servicio.

En cuanto al administrador se podrá visualizar información del sistema, que le permitirar posteriormente realizar un análisis en base a los indicadores claves por medio de gráficas que representen de mejor manera los datos obtenidos. 

##### Útil

La aplicación tiene como eje central la satisfacción de los usuarios de tipo turista y de los servicios.

Los turistas tendrán una plataforma en la que puedan gestionar sus viajes, reservaciones en hoteles y transporte y no solo dentro del país. La plataforma alojará información sobre varios países que ofrescan tambien estos servicios a los turistas que deseen viajar a estos países.

En cuanto a los servicios,tendrán una plataforma que les permita promocionarse y darse a conocer a los turistas, ofreciendo distintos servicios que ellos puedan consumir.

##### Encontrable

El usuario encontrará una plataforma estable que le permita gestionar sus viajes, reservaciones en hoteles o solicitar un transporte. Está contará con un menú sencillo para poder navegar entre las distintas funcionalidades del sistema, contando con iconos relacionados con las funciones y que puedan ser amigables para el usuario.

También, el sistema tendrá una extensa cantidad de servicios, es por ello que para facilitar la busqueda de estos se le proporcionará al usuario una barra de busquedá.

##### Confiable

Se mantendrá un sistema siempre actualizado y protegera la información del usuario.

El sistema tendrá apartados para realizar reseñas de los servicios que se encuentran disponibles, logrando que otros usuarios puedan ver las calificaciones y comentarios publicados por otros usuarios y confirmar la calidad del servicio.

##### Deseable

La aplicación se mantendrá actualizada en cuanto a nuevos servicios que sean agregados al sistema de esta forma el usuario siempre encontrá algo nuevo.

La aplicación mantendra la paleta de colores seleccionada en todas las vistas del sistema, esto con el fin no crear incertidumbre y confundir a los usuarios que tendrán contacto con la plataforma.

##### Accesible

La aplicación estará disponible para cualquier persona que desee buscar una nueva experiencia para poder viajar, realizar una reservación en un hotel o reservar un transporte. Teniendo una plataforma con una apariencia amigable y colores que atraigan al usuario.

El sistema también permitirá a una servicio tercero acceder a la plataforma desde la página web y ofrecer sus servicios para que estén disponibles para el usuario de tipo turista.

##### Valorable

La plataforma ofrece una gran cantidad de servicios para los usuarios que deseen viajar a otro país, realizar una reservación en un hotel que aún no conoce, o reservar un trasporte para conocer el lugar. Ofreciendo un sistema seguro y amigable para que los usuarios puedan sentirse comódos al momento de consultar cualquiera de estos servicios.

Los mismos servicios, tendrán una plataforma segura en la que puedan ofrecer sus servicios a turistas de distintas partes.  

### Aplicación móvil

##### Utilizable

La aplicación móvil permitira únicamente a los turistas tener una la posibilidad de acceder a la plataforma desde su smartphone y poder utilizar las mismas funciones que en la página web. De esta misma forma podrá realizar sus reservaciones y navegar entre los distintos servicios disponibles. 

##### Útil

Una versión especial y unicamente para los usuarios de tipo turista para que puedan acceder a la plataforma y consumir los servicios disponibles desde la comodidad de su télefono inteligente.  

##### Encontrable

Se encontraran únicamente las funciones disponibles para los usuarios de tipo turista, teniendo una facilidad para navegar entre las distintas vistas que serán agrabales para cualquier usuario. 

##### Confiable

Se tendrá una aplicación segura que proteja la información del usuario y garantice que todas los servicios con los que cuente el sistema sean eficientes. La aplicación móvil de igual forma contará con las vistas de las reseñas hechas por otros usuarios que califiquen un servicio que hayan consumido.

##### Deseable

La aplicación se mantendrá actualizada en cuanto a nuevos servicios que sean agregados al sistema de esta forma el usuario siempre encontrá algo nuevo.

La aplicación mantendra la paleta de colores seleccionada en todas las vistas del sistema, esto con el fin no crear incertidumbre y confundir a los usuarios que tendrán contacto con la plataforma.


##### Accesible

La aplicación estará disponible para cualquier persona que desee buscar una nueva experiencia para poder viajar, realizar una reservación en un hotel o reservar un transporte. Teniendo una plataforma con una apariencia amigable y colores que atraigan al usuario.

##### Valorable

La aplicación móvil ofrece una gran cantidad de servicios para los usuarios que deseen viajar a otro país, realizar una reservación en un hotel que aún no conoce, o reservar un trasporte para conocer el lugar. Ofreciendo un sistema seguro y amigable para que los usuarios puedan sentirse comódos al momento de consultar cualquiera de estos servicios.

## UI

### Web

##### Propósito del sitio

Debido a la pandemia ocasionada por el COVID-19 se detuvieron muchas actividades que afectaron la economía a nivel mundial. Guatemala no fue la excepción, y se vio afectada una de las mayores actividades económicas como lo es el sector del turismo y se necesita con urgencia ofrecer una solución para reactivar está actividad económica.

El instituto de Guatemalteco de turismo - INGUAT quieré una el desarrollo e implementación de una aplicación que permita la gestión de viajes, estadía y trasporte para los turistas en distintos países y que se generen validaciones internacionales para verificar si una persona puede ingresar a un país por sus propias normas de bioseguridad.

Es donde la aplicación "Full trip" es una solución para que los turistas puedan gestionar sus viajes, realizar reservaciones en hoteles y reservar un transporte y los usuarios puedan realizar todas estas acciones en sus dispositivos.

##### Mostrar el contenido

1. El contenido será mostrado de forma clara y con una fuente agradable y tamaño considerable para que toda la información pueda ser vista por el usuario.

2. Se utilizarán iconos que representen ciertas funcionalidades de la aplicación y que sean amigables para el usuario.

3. Los formularios solicitarán información necesaria y se dará a entender de que manera se deben de rellenar los campos.

4. Imagenes de los servicios disponibles para que el usuario pueda conocer de mejor manera el servicio.


##### Diseño gráfico funcional

Se presentará un diseño amigable y funcional para el usuario, únicamente presentando información relevante para el usuario. 

Para conocer a más detalle el diseño que podría tener la aplicación se recomienda consultar los Mockups diseñados en el archivo [Mockups.md](./Mockups.md) en la sección **aplicación web.**

### Aplicación móvil

##### Propósito del sitio

Otra parte de la solución propuesta para reactivar esta actividad económica, es el desarrollo e implemntación de una aplicación móvil de la aplicación "Full trip".

Esta aplicación móvil tiene el principal objetivo de ser consumida únicamente por los usuarios de tipo turista, que podrán disfrutar de las mismas funcionalidades del sistema en su versión web pero transformada a sus dispositivos móviles. Esto con el fin de que el usuario siempre pueda estar conectado con la aplicación y pueda acceder a la información en el momento que desee.

##### Mostrar el contenido

1. El contenido será mostrado de forma clara y con una fuente agradable y tamaño considerable para que toda la información pueda ser vista por el usuario.

2. Se utilizarán iconos que representen ciertas funcionalidades de la aplicación y que sean amigables para el usuario.

3. Los formularios solicitarán información necesaria y se dará a entender de que manera se deben de rellenar los campos.

4. Imagenes de los servicios disponibles para que el usuario pueda conocer de mejor manera el servicio.


##### Diseño gráfico funcional


Para conocer a más detalle el diseño que podría tener la aplicación se recomienda consultar los Mockups diseñados en el archivo [Mockups.md](./Mockups.md) en la sección **aplicación móvil.**