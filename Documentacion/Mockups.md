# Mockups

## Mockups Página web

* Vista registro de usuarios

![title](./images/Mockups/mk1.PNG)

* Vista Login usuario

![title](./images/Mockups/mk2.PNG)

* Vista busqueda de habitación / hotel con filtro , aqui se desplagaran todas las habitaciones con descipción y opción de ingresar a ver el detalle del mismo.

![title](./images/Mockups/mk3.PNG)

* Vista detalle de habitaciones

![title](./images/Mockups/mk4.PNG)

* Vista renta de autos en el cual de desplegan todos los autos disponibles, con posibilidad de busqueda y filtro 

![title](./images/Mockups/mk5.PNG)

* Vista de vuelos en el cual se desplegan todos los vuelos, con posibilidad de busqueda y filtro

![title](./images/Mockups/mk6.PNG)

* Vista que permite registrar las fechas en las cuales puede registrar posibles reservaciones

![title](./images/Mockups/mk7.PNG)

* Vista que permite subir reseña sobre algún servicio .

![title](./images/Mockups/mk8.PNG)

* Vista que permite registrar vacuna para esquema de vacunación

![title](./images/Mockups/mk9.PNG)

* Vista para la realización de pagos.

![title](./images/Mockups/mk10.PNG)

* Vista que permite registrar vacunas para esquema de vacunación para país.

![title](./images/Mockups/mk11.PNG)

## Mockups Móvil 

|!["Iniciar sesión"](./images/Mockups/Movil/inicio_sesion_movil.png)|
|:--:|
| Imagen: Se muestra el inicio de sesión de la aplicación móvil. |

|!["Registro de un usuario"](./images/Mockups/Movil/registro_movil.png)|
|:--:|
| Imagen: Se muestra el formulario de registro para los usuarios que deseen acceder al sistema.|

|!["Página de inicio"](./images/Mockups/Movil/inicio_movil.png)|
|:--:|
| Imagen: Se muestra el menú de inicio del sistema cuando un usuario accede.|

|!["Servicios del usuario"](./images/Mockups/Movil/servicios_movil.png)|
|:--:|
| Imagen: Muestra todos los servicios a disposición del usuario.|

|!["Realizar reservaciones"](./images/Mockups/Movil/mis_reservaciones_movil.png)|
|:--:|
| Imagen: Se muestran las reservaciones que ha adquirido recientemente.|

|!["Perfil del usuario"](./images/Mockups/Movil/ver_perfil_movil.png)|
|:--:|
| Imagen: |