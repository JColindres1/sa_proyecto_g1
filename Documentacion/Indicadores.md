# Indicadores clave para el negocio

|Indicador|Descripción |
|--|--|
|Número de usuarios registrados en la plataforma| Datos de usuarios registrados en la plataforma para indicar la cantidad de personas que consumen los servicios. |
|Países más visitados por los usuarios| Un top de los países a los cuales los usuarios de turista solicitan viajar más. |
|Servicio de renta de autos más popular por país| Servicio de renta más utilizado por los usuarios al momento de viajar a otro país.|
|Tipo de vuelo más comprado por los turistas| Cantidad de usuarios que utilizan un tipo de vuelo en especifico.|
|Top 5 de los hoteles más visitados por los usuarios| Hoteles favoritos de los usuarios al momento de viajar a algún lugar.|
|Banco preferido por los usuarios para obtener tarjeta de crédito| Top de bancos que proveen tarjetas de crédito. |
|Número de reservaciones hechas por los usuarios por mes| Cantidad de reservaciones realizadas por los usuarios en un mes.|
|Total obtenido por reservaciones en hoteles por mes| Cantidad de dinero obtenida en un mes por las reservaciones de usuarios en hoteles.|
|Total obtenido por reservaciones en renta de autos por mes|Cantidad de dinero obtenida en un mes por las reservaciones de usuarios en servicio de renta de autos.|
|Total obtenido por reservaciones en vuelos por mes|Cantidad de dinero obtenida en un mes por las reservaciones de usuarios en la compra de boletos de avión.|