terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}


provider "google" {
  credentials = file("llave.json") 
  project     = "sa-g1-343004"
  region      = "us-central1"
  zone        = "us-central1-c"
}



resource "google_compute_firewall" "firewall" {
  name    = "sa-g1"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["3000", "3500", "4000", "5000", "6000", "6500", "7000", "8000", "9000"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["firewall"]
}



resource "google_compute_instance" "vm-sa-g1" {

  name         = "sa-g1"
  machine_type = "e2-medium"
  tags         = ["firewall"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  network_interface {
    network = "default"
    access_config {
      nat_ip       = "35.208.201.71"
      network_tier = "STANDARD"
    }
  }

  metadata_startup_script = file("./config")

  depends_on = [google_compute_firewall.firewall]
}