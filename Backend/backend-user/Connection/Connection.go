package Connection

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

//Conexion con la base de datos
func MySQL_connection() (db *sql.DB, e error) {
	user := goDotEnvVariable("USER")
	pass := goDotEnvVariable("PASS")
	host := goDotEnvVariable("HOST")
	database := goDotEnvVariable("DATABASE")

	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@%s/%s", user, pass, host, database))
	if err != nil {
		log.Fatalf("Error: %v", err)
	}

	return db, nil
}

func goDotEnvVariable(key string) string {

	// Leer el archivo .env ubicado en la carpeta actual
	err := godotenv.Load(".env")

	// Si existio error leyendo el archivo
	if err != nil {
		log.Fatalf("Error cargando las variables de entorno")
	}

	// Enviar la variable de entorno que se necesita leer
	return os.Getenv(key)
}
