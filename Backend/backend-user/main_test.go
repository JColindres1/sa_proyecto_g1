package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_Ev(t *testing.T) {
	req, err := http.NewRequest("GET", "/lista/pais", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(pais_lista)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

func Test_login(t *testing.T) {
	var json = []byte(`{"Correo":"jcolindres@mail.com", "Contrasena":"qwerty"}`)

	req, err := http.NewRequest("POST", "/user/login", bytes.NewBuffer(json))

	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Login)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusAccepted)
	}
}
