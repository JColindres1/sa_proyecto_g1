package main

import (
	"strings"
	db "usuario/Connection"

	"bytes"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type User struct {
	Correo     string `json:"correo"`
	Contrasena string `json:"contrasena"`
}

type Formulario struct {
	Nombre     string
	Correo     string
	Contrasena string
	Fecha      string
	Imagen     string
	Ciudad_id  int
	Rol_id     int
}

type TC struct {
	Tc_id      int
	Ccv        int
	Fecha_v    string
	Usuario_id int
}

type Dosis struct {
	Descripcion string
	Fecha       string
	EV_id       int
	Tipo        string
	Tp_id       int
}

type Esquema struct {
	Usuario_id int
	Dosis_id   int
}

type Rol struct {
	Rol_id      int
	Descripcion string
}

type Vacuna struct {
	Vacuna_id   int
	Descripcion string
}

type Pais struct {
	Pais_id int
	Nombre  string
}

type Ciudad struct {
	Ciudad_id int
	Nombre    string
}

type auth struct {
	Usuario_id int    `json:"id_usuario"`
	Nombre     string `json:"nombre"`
	Correo     string `json:"correo"`
	Contrasena string `json:"contrasena"`
	Fecha      string `json:"fecha"`
	Imagen     string `json:"imagen"`
	Ciudad_id  int    `json:"id_ciudad"`
	Rol_id     int    `json:"rol_id"`
	Rol        string `json:"rol"`
	Mensaje    string `json:"Mensaje"`
}

type JWT struct {
	Mensaje string
	JWT     string
}

type Mensaje struct {
	Mensaje string
}

func main() {
	router := mux.NewRouter().StrictSlash(true)
	//enableCORS(router)
	router.HandleFunc("/login", Signin).Methods("POST")
	router.HandleFunc("/login", Login).Methods("GET")
	router.HandleFunc("/user/borrar", Borrar).Methods("POST")
	router.HandleFunc("/user/registro", Registro).Methods("POST")

	router.HandleFunc("/lista/rol", roles_lista)
	router.HandleFunc("/lista/pais", pais_lista)
	router.HandleFunc("/lista/ciudad/{pais}", ciudad_lista)

	/*router.HandleFunc("/user/dosis-esquema", EsquemaVac).Methods("POST")
	router.HandleFunc("/user/EV/{id}", obtenerIDev).Methods("GET")
	router.HandleFunc("/user/agregar-esquema", agregarEV).Methods("POST")
	router.HandleFunc("/user/EV/completo/{id}", obtenerEVcompleto).Methods("GET")*/
	//router.HandleFunc("/lista/vacuna", vacuna_lista)

	fmt.Println("Servidor disponible en el puerto 7000")
	log.Fatal(http.ListenAndServe(":7000", router))
}

var jwtKey = []byte("grupo1_sa")

type Claims struct {
	Correo string `json:"correo"`
	jwt.StandardClaims
}

func Borrar(w http.ResponseWriter, r *http.Request) {
	var usuario User
	solicitud, err := ioutil.ReadAll(r.Body)
	CatchError(err)

	err = json.Unmarshal([]byte(solicitud), &usuario)
	CatchError(err)

	db, err := db.MySQL_connection()
	CatchError(err)
	defer db.Close()

	var Key auth
	Key = Login_verificar(db, usuario.Correo, usuario.Contrasena)

	if Key.Usuario_id != 0 {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusAccepted)
		Key.Mensaje = "Usuario encontrado"
		switch Key.Rol_id {
		case 1:
			Key.Rol = "turista"
		case 2:
			Key.Rol = "hotel"
		case 3:
			Key.Rol = "rentadora"
		case 4:
			Key.Rol = "vuelos"
		case 5:
			Key.Rol = "administrador"
		}
		json.NewEncoder(w).Encode(Key)
	} else {
		w.Header().Set("Content-type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		Key.Mensaje = "El correo o la contraseña son incorrectos"
		json.NewEncoder(w).Encode(Key)
	}

}

func Signin(w http.ResponseWriter, r *http.Request) {
	var usuario User
	// Get the JSON body and decode into credentials
	err := json.NewDecoder(r.Body).Decode(&usuario)
	if err != nil {
		// If the structure of the body is wrong, return an HTTP error
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	db, err := db.MySQL_connection()
	CatchError(err)
	defer db.Close()

	var Key auth
	Key = Login_verificar(db, usuario.Correo, usuario.Contrasena)

	// Get the expected password from our in memory map
	//expectedPassword, ok := mapaUser[usuario.Correo]

	// If a password exists for the given user
	// AND, if it is the same as the password we received, the we can move ahead
	// if NOT, then we return an "Unauthorized" status
	if Key.Contrasena != usuario.Contrasena || Key.Correo == "" {
		w.WriteHeader(http.StatusUnauthorized)
		var respuesta Mensaje
		respuesta.Mensaje = "Las credenciales no son válidas"
		json.NewEncoder(w).Encode(respuesta)
		return
	}

	// Declare the expiration time of the token
	// here, we have kept it as 5 minutes
	expirationTime := time.Now().Add(30 * time.Minute)
	// Create the JWT claims, which includes the username and expiry time
	claims := &Claims{
		Correo: usuario.Correo,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		// If there is an error in creating the JWT return an internal server error
		w.WriteHeader(http.StatusUnauthorized)
		var respuesta Mensaje
		respuesta.Mensaje = "Ocurrió un error en el servidor"
		json.NewEncoder(w).Encode(respuesta)
		return
	}

	w.WriteHeader(http.StatusOK)
	var respuesta JWT
	respuesta.JWT = tokenString
	respuesta.Mensaje = "Ingreso con éxito"
	json.NewEncoder(w).Encode(respuesta)

	// Finally, we set the client cookie for "token" as the JWT we just generated
	// we also set an expiry time which is the same as the token itself
	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   tokenString,
		Expires: expirationTime,
	})
}

func Login(w http.ResponseWriter, r *http.Request) {

	fmt.Println(r.Header.Get("Authentication"))

	tknStr := r.Header.Get("Authentication")
	// Initialize a new instance of `Claims`
	claims := &Claims{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			w.WriteHeader(http.StatusUnauthorized)
			var respuesta Mensaje
			respuesta.Mensaje = "La sesión ha expirado"
			json.NewEncoder(w).Encode(respuesta)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		var respuesta Mensaje
		respuesta.Mensaje = "La sesión ha expirado"
		json.NewEncoder(w).Encode(respuesta)
		return
	}
	if !tkn.Valid {
		w.WriteHeader(http.StatusUnauthorized)
		var respuesta Mensaje
		respuesta.Mensaje = "La sesión ha expirado"
		json.NewEncoder(w).Encode(respuesta)
		return
	}
	// Finally, return the welcome message to the user, along with their
	// username given in the token

	db, err := db.MySQL_connection()
	CatchError(err)
	defer db.Close()

	var Key auth
	Key = Login_verificar_JWT(db, claims.Correo)

	w.WriteHeader(http.StatusOK)
	Key.Mensaje = "Ingreso con éxito"
	switch Key.Rol_id {
	case 1:
		Key.Rol = "turista"
	case 2:
		Key.Rol = "hotel"
	case 3:
		Key.Rol = "rentadora"
	case 4:
		Key.Rol = "vuelos"
	case 5:
		Key.Rol = "administrador"
	}
	json.NewEncoder(w).Encode(Key)
}

func Registro(w http.ResponseWriter, r *http.Request) {
	var form Formulario
	solicitud, err := ioutil.ReadAll(r.Body)
	CatchError(err)

	err = json.Unmarshal([]byte(solicitud), &form)
	CatchError(err)

	db, err := db.MySQL_connection()
	CatchError(err)
	defer db.Close()

	if !validarCorreo(form.Correo) {
		w.WriteHeader(http.StatusBadRequest)
		var respuesta Mensaje
		respuesta.Mensaje = "El correo es inválido o no existe!"
		json.NewEncoder(w).Encode(respuesta)
		return
	}

	if form.Fecha == "" {
		form.Fecha = "2022-04-20"
	}

	if form.Imagen == "" {
		form.Imagen = "No disponible"
	}

	if s3Imagen(form.Imagen, form.Correo) {
		form.Imagen = strings.Replace(form.Correo, "@", "%40", -1)
		form.Imagen = "https://grupo1sa2022.s3.amazonaws.com/" + form.Imagen + ".jpg"
	} else {
		w.WriteHeader(http.StatusBadRequest)
		var respuesta Mensaje
		respuesta.Mensaje = "No se pudo guardar la imagen, intentelo otra vez"
		json.NewEncoder(w).Encode(respuesta)
		return
	}

	if !Registro_verificar(db, form.Correo) {
		sentencia, err := db.Prepare("INSERT INTO Usuario (nombre,correo,contrasena,fecha,imagen,ciudad_id,rol_id) VALUES (?,?,?,?,?,?,?)")
		CatchError(err)

		res, err := sentencia.Exec(form.Nombre, form.Correo, form.Contrasena, form.Fecha, form.Imagen, form.Ciudad_id, form.Rol_id)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}

		id, err := res.LastInsertId()
		CatchError(err)

		fmt.Println(id)
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(http.StatusCreated)

	} else {
		w.WriteHeader(http.StatusAccepted)
		json.NewEncoder(w).Encode(http.StatusAccepted)
	}
}

func roles_lista(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	db, err := db.MySQL_connection()
	CatchError(err)

	defer db.Close()

	var roles []Rol

	rows, err := db.Query("SELECT rol_id, descripcion FROM Rol")
	if err != nil {
		log.Print(err)
	}
	defer rows.Close()

	for rows.Next() {
		var r Rol
		if err := rows.Scan(&r.Rol_id, &r.Descripcion); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode("{'message':'aber'}")
		}
		roles = append(roles, r)
	}
	if err = rows.Err(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("{'message':'aber'}")
	}
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(roles)
}

func pais_lista(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	db, err := db.MySQL_connection()
	CatchError(err)

	defer db.Close()

	var paises []Pais

	rows, err := db.Query("SELECT * FROM Pais")
	if err != nil {
		log.Print(err)
	}
	defer rows.Close()

	for rows.Next() {
		var p Pais
		if err := rows.Scan(&p.Pais_id, &p.Nombre); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode("{'message':'aber'}")
		}
		paises = append(paises, p)
	}
	if err = rows.Err(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("{'message':'aber'}")
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(paises)
}

func ciudad_lista(w http.ResponseWriter, r *http.Request) {
	db, err := db.MySQL_connection()
	CatchError(err)

	defer db.Close()

	var ciudades []Ciudad
	params := mux.Vars(r)
	rows, err := db.Query("SELECT ciudad_id, nombre FROM Ciudad WHERE pais_id = ?", params["pais"])
	if err != nil {
		log.Print(err)
	}
	defer rows.Close()

	for rows.Next() {
		var c Ciudad
		if err := rows.Scan(&c.Ciudad_id, &c.Nombre); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode("{'message':'aber'}")
		}
		ciudades = append(ciudades, c)
	}
	if err = rows.Err(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("{'message':'aber'}")
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(ciudades)
}

func Login_verificar(db *sql.DB, correo string, password string) auth {
	var us auth
	err := db.QueryRow("SELECT * FROM Usuario WHERE correo = ? and contrasena = ? ", correo, password).Scan(&us.Usuario_id, &us.Nombre, &us.Correo, &us.Contrasena, &us.Fecha, &us.Imagen, &us.Rol_id, &us.Ciudad_id)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Print(err)
		}
		return us
	}
	return us
}

func Login_verificar_JWT(db *sql.DB, correo string) auth {
	var us auth
	err := db.QueryRow("SELECT * FROM Usuario WHERE correo = ?", correo).Scan(&us.Usuario_id, &us.Nombre, &us.Correo, &us.Contrasena, &us.Fecha, &us.Imagen, &us.Rol_id, &us.Ciudad_id)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Print(err)
		}
		return us
	}
	return us
}

func Registro_verificar(db *sql.DB, correo string) bool {
	err := db.QueryRow("SELECT correo FROM Usuario WHERE correo = ?", correo).Scan(&correo)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Print(err)
		}
		return false
	}
	return true
}

func CatchError(err error) {
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
}

func initAWSConnection() *s3.S3 {
	awsAccessKey := "AKIAW42GYMAQ6FK36ZNM"
	awsSecretKey := "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"

	creds := credentials.NewStaticCredentials(awsAccessKey, awsSecretKey, "")

	_, err := creds.Get()

	if err != nil {
		fmt.Println(err)
	}

	s3Region := "us-east-1"

	cfg := aws.NewConfig().WithRegion(s3Region).WithCredentials(creds)

	s3Connection := s3.New(session.New(), cfg)
	return s3Connection
}

func S3UploadBase64(base64File string, objectKey string) error {
	decode, err := base64.StdEncoding.DecodeString(base64File)

	if err != nil {
		return err
	}

	awsSession := initAWSConnection()

	uploadParams := &s3.PutObjectInput{
		Bucket: aws.String("grupo1sa2022"),
		Key:    aws.String(objectKey),
		Body:   bytes.NewReader(decode),
	}

	_, err = awsSession.PutObject(uploadParams)

	return err
}

func s3Imagen(Imagen string, Correo string) bool {

	base64Image := Imagen

	keyName := Correo + ".jpg"

	b64data := base64Image[strings.IndexByte(base64Image, ',')+1:]

	err := S3UploadBase64(b64data, keyName)

	if err != nil {
		fmt.Println(err)
		return false
	}

	return true
}

func validarCorreo(Correo string) bool {

	email := Correo
	url := "https://isitarealemail.com/api/email/validate?email=" + url.QueryEscape(email)
	req, _ := http.NewRequest("GET", url, nil)
	res, err := http.DefaultClient.Do(req)

	if err != nil {
		fmt.Println(err)
		return false
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("error %v", err)
		return false
	}

	var myJson RealEmailResponse
	json.Unmarshal(body, &myJson)

	fmt.Printf(myJson.Status)

	if myJson.Status == "valid" {
		return true
	} else {
		return false
	}
}

type S3 struct {
	Imagen string
	Correo string
}

type RealEmailResponse struct {
	Status string `json:"status"`
}

/*
func EsquemaVac(w http.ResponseWriter, r *http.Request) {
	var esqVac Esquema
	solicitud, err := ioutil.ReadAll(r.Body)
	CatchError(err)

	err = json.Unmarshal([]byte(solicitud), &esqVac)
	CatchError(err)

	db, err := db.MySQL_connection()
	CatchError(err)

	defer db.Close()

	if !EsquemaVac_verificar(db, esqVac.Usuario_id, esqVac.Dosis_id) {
		sentencia, err := db.Prepare("INSERT INTO EsquemaVacunacion (usuario_id, dosis_id) VALUES (?,?)")
		CatchError(err)

		_, err = sentencia.Exec(esqVac.Usuario_id, esqVac.Dosis_id)
		if err != nil {
			log.Fatalf("Error: %v", err)
			w.WriteHeader(http.StatusBadRequest)
			var respuesta Mensaje
			respuesta.Mensaje = "No se pudo procesar tu solicitud."
			json.NewEncoder(w).Encode(respuesta)
			return
		}

		w.WriteHeader(http.StatusCreated)
		var respuesta Mensaje
		respuesta.Mensaje = "Dosis registrada con éxito."
		json.NewEncoder(w).Encode(respuesta)
		return

	} else {
		w.WriteHeader(http.StatusBadRequest)
		var respuesta Mensaje
		respuesta.Mensaje = "Ya cuentas con esa dosis."
		json.NewEncoder(w).Encode(respuesta)
		return
	}
}

func obtenerIDev(w http.ResponseWriter, r *http.Request) {
	db, err := db.MySQL_connection()
	CatchError(err)

	defer db.Close()

	var esquema []Esquema
	params := mux.Vars(r)
	rows, err := db.Query("SELECT EV_id FROM EsquemaVacunacion WHERE usuario_id = ?", params["id"])
	if err != nil {
		log.Print(err)
	}
	defer rows.Close()

	for rows.Next() {
		var e Esquema
		if err := rows.Scan(&e.Usuario_id); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode("{'message':'aber'}")
		}
		esquema = append(esquema, e)
	}
	if err = rows.Err(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("{'message':'aber'}")
	}
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(esquema)
}

func agregarEV(w http.ResponseWriter, r *http.Request) {
	var agEV Dosis
	solicitud, err := ioutil.ReadAll(r.Body)
	CatchError(err)

	err = json.Unmarshal([]byte(solicitud), &agEV)
	CatchError(err)

	db, err := db.MySQL_connection()
	CatchError(err)

	defer db.Close()

	sentencia, err := db.Prepare("INSERT INTO Dosis (descripcion,fecha,EV_id,tp_id) VALUES (?,?,?,?)")
	CatchError(err)

	res, err := sentencia.Exec(agEV.Descripcion, agEV.Fecha, agEV.EV_id, agEV.Tp_id)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}

	id, err := res.LastInsertId()
	CatchError(err)

	fmt.Println(id)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(agEV)
}

func obtenerEVcompleto(w http.ResponseWriter, r *http.Request) {
	db, err := db.MySQL_connection()
	CatchError(err)

	defer db.Close()

	var esquema []Dosis
	params := mux.Vars(r)
	rows, err := db.Query("SELECT D.descripcion, D.fecha,D.EV_id,D.tp_id,tv.descripcion FROM Dosis D, Tipo_Vacuna tv, EsquemaVacunacion EV WHERE D.tp_id = tv.tp_id AND D.EV_id = EV.EV_id AND EV.usuario_id = ?", params["id"])
	if err != nil {
		log.Print(err)
	}
	defer rows.Close()

	for rows.Next() {
		var e Dosis
		if err := rows.Scan(&e.Descripcion, &e.Fecha, &e.EV_id, &e.Tp_id, &e.Tipo); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode("{'message':'aber'}")
		}
		esquema = append(esquema, e)
	}
	if err = rows.Err(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("{'message':'aber'}")
	}
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(esquema)
}

func vacuna_lista(w http.ResponseWriter, r *http.Request) {
	db, err := db.MySQL_connection()
	CatchError(err)

	defer db.Close()

	var vacunas []Vacuna

	rows, err := db.Query("SELECT tp_id, descripcion FROM Tipo_Vacuna")
	if err != nil {
		log.Print(err)
	}
	defer rows.Close()

	for rows.Next() {
		var v Vacuna
		if err := rows.Scan(&v.Vacuna_id, &v.Descripcion); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode("{'message':'aber'}")
		}
		vacunas = append(vacunas, v)
	}
	if err = rows.Err(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("{'message':'aber'}")
	}
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(vacunas)
}

func EsquemaVac_verificar(db *sql.DB, idusuario int, iddosis int) bool {
	err := db.QueryRow("SELECT usuario_id FROM EsquemaVacunacion WHERE usuario_id = ? AND dosis_id = ?", idusuario, iddosis).Scan(&idusuario)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Print(err)
		}
		return false
	}
	return true
}
*/
