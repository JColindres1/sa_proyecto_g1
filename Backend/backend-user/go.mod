module usuario

go 1.17

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
)

require (
	github.com/aws/aws-sdk-go v1.43.42 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
)
