from http import client
import unittest
import json
import requests
from zeep import Client

class Pruebas_unitarias(unittest.TestCase):

    def test_obtener_autos(self):
        codigo_esperado = 201
        mensaje_error = "Ocurrio un error al conectar con el servidor"
        client = Client(wsdl="http://localhost:8000/?wsdl")
        respuesta = client.service.Obtener_autos()
        respuestajson = json.loads(respuesta)
        self.assertEqual(
            respuestajson["code"],
            codigo_esperado,
            mensaje_error
        )

    def test_obtener_autos_id(self):
        codigo_esperado = 201
        mensaje_error = "Ocurrio un error al conectar con el servidor"
        client = Client(wsdl="http://localhost:8000/?wsdl")
        respuesta = client.service.Obtener_autos_id(1)
        respuestajson = json.loads(respuesta)
        self.assertEqual(
            respuestajson["code"],
            codigo_esperado,
            mensaje_error
        )

    def test_obtener_autos_id(self):
        codigo_esperado = 201
        mensaje_error = "Ocurrio un error al conectar con el servidor"
        diccionario = {"cantidad_personas":"1","precio":"1000","color":"negro","marca":"SA","modelo":"2022","linea":"1semestre","imagen":"laboratorio","usuario_id":"1"}
        client = Client(wsdl="http://localhost:8000/?wsdl")
        respuesta = client.service.Registrar_auto(json.dumps(diccionario))
        respuestajson = json.loads(respuesta)
        self.assertEqual(
            respuestajson["code"],
            codigo_esperado,
            mensaje_error
        )


if __name__ == '__main__':
    unittest.main()