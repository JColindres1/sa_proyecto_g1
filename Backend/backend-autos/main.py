from decimal import Decimal
import json
from statistics import mode
from tokenize import String


from Connection.Connection import Establecer_conexion
from Connection.Connection import Establecer_conexion_usuarios
from spyne import Application
from spyne import rpc
from spyne import ServiceBase
from spyne import Iterable, Integer, Unicode
from spyne.protocol.soap import Soap11
from spyne.server.wsgi import WsgiApplication


#Verificar si una placa ya esta registrada
def Verificar_placa(identificador):
    db = Establecer_conexion()
    with db.cursor() as cursor:
        cursor.execute("SELECT auto_id FROM Automovil where placa = %s",(identificador,))
        row = cursor.fetchone()
    db.close()
    if row is not None:
        return True
    else:
        return False

#Verificar fechas de reservaciones de vehiculos
def TraducirCsv(informacion,idusuario):
    try:
        lista = []
        separador = informacion.split("\n")
        longitud = len(separador)
        filas = separador[1:longitud]
        for x in filas:
            valores = x.split(";")
            lista.append({"marca":valores[0],"linea":valores[1],"modelo":valores[2],"precio_dia":valores[3],"imagen":valores[4],"estado":valores[5],"idusuario":idusuario,"idciudad":valores[6],"placa":valores[7]}) 

        return lista
    except:
        return "Error al obtener informacion del CSV"

def Obtenernombre(id):
    try:
        nombre = ""
        db = Establecer_conexion_usuarios()
        with db.cursor() as cursor:
            cursor.execute("SELECT nombre FROM Usuario WHERE usuario_id = %s",(id,))
            resultado = cursor.fetchone()
            print("-----------------------")
            print(resultado[0])
            nombre = resultado[0]
        db.close()
        return nombre
    except:
        return "Error al obtener informacion del CSV"


class CarsService(ServiceBase):
   
    #Insertar Vehiculor
    #placa, marca, linea, modelo, precio_dia, imagen, idusuario, idciudad
    @rpc(Unicode,Unicode,Unicode,Unicode,Integer,Unicode,Integer,Integer,_returns=Unicode)
    def InsertarVehiculo(self,placa, marca,linea,modelo,precio,imagen,idusuario,idcuidad):
        try:
            if not Verificar_placa(placa):
                db = Establecer_conexion()
                with db.cursor() as cursor:
                    cursor.execute("INSERT INTO Automovil (placa,marca,linea,modelo,estado,precio_dia,imagen,idusuario,idciudad) VALUES (%s,%s,%s,%s,'1',%s,%s,%s,%s)",
                                                            (placa,marca,linea,modelo,precio,imagen,idusuario,idcuidad))
                db.commit()
                db.close()
                return str({'code':201,'mensaje': 'Automovil agregado'})
            else:
                return str({'code':400,'mensaje': 'La placa ya se encuentra registrada'}) 
        except:
            return str({'code':400,'mensaje': 'Ocurrio un error al registrar el automovil'})
        

    #Obtener todos los vehiculos
    @rpc(_returns=Unicode)
    def MostrarVehiculo(self):
        try:
            lista = []
            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("SELECT * FROM Automovil WHERE estado = 1")
                resultado = cursor.fetchall()
                for x in resultado:
                    lista.append({'auto_id':x[0],'placa': x[1], 'marca': x[2], 'linea': x[3], 'modelo': x[4], 'precio_dia': x[6], 'imagen': x[7], 'idusuario': x[8], 'idciudad': x[9]})
            db.close()
            return str({'code':200,'datos':lista})
        except:
            return str({'code':400,'mensaje':'Ocurrio un error al obtener los datos'})

    #Obtener todos los vehiculos de una rentadora de autos
    #identificador
    @rpc(Integer,_returns=Unicode)
    def MostrarDatos(self,identificador):
        try:
            lista = []
            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("SELECT * FROM Automovil where idusuario = %s",(identificador,))
                resultado = cursor.fetchall()
                for x in resultado:
                    lista.append({'auto_id':x[0],'placa': x[1], 'marca': x[2], 'linea': x[3], 'modelo': x[4], 'precio_dia': x[6], 'imagen': x[7], 'idciudad': x[9]})
            db.close()
            return str({'code':200,'datos':lista})
        except:
            return str({'code':400,'mensaje':'Ocurrio un error al obtener los datos'})

    #Rentar un vehiculo de forma parcial
    #fecha_inicio, fecha_fin, idusuario, idvehiculo, total
    @rpc(Unicode,Unicode,Integer,Integer,Unicode,Integer,_returns=Unicode)
    def Reservacion(self,fecha_inicio,fecha_fin,idusuario,idvehiculo,idtarjeta,total):
        try:
            if idtarjeta != 0:
                #print("CON TARJETA")
                db = Establecer_conexion()
                with db.cursor() as cursor:
                    cursor.execute("INSERT INTO Reserva_Auto (cantidad_dias,costo_dia,fecha_inicio,fecha_fin,idusuario,idvehiculo,total,idtarjeta) VALUES ( DATEDIFF(%s,%s), (SELECT precio_dia FROM Automovil WHERE auto_id = %s),%s,%s,%s,%s,%s,%s)",
                                                            (fecha_inicio,fecha_fin,idvehiculo,fecha_inicio,fecha_fin,idusuario,idvehiculo,total,idtarjeta))
                    cursor.execute("UPDATE Automovil SET estado = 0 WHERE auto_id = %s", (idvehiculo,))
                db.commit()
                db.close()
                return str({'code':200,'mensaje':'Reservacion completa realizada'})
            else:
                #print("SIN TARJETA")
                db = Establecer_conexion()
                with db.cursor() as cursor:
                    cursor.execute("INSERT INTO Reserva_Auto (cantidad_dias,costo_dia,fecha_inicio,fecha_fin,idusuario,idvehiculo,total,idtarjeta) VALUES ( DATEDIFF(%s,%s), (SELECT precio_dia FROM Automovil WHERE auto_id = %s),%s,%s,%s,%s,%s,%s)",
                                                            (fecha_inicio,fecha_fin,idvehiculo,fecha_inicio,fecha_fin,idusuario,idvehiculo,total,idtarjeta))
                    cursor.execute("UPDATE Automovil SET estado = 0 WHERE auto_id = %s", (idvehiculo,))
                db.commit()
                db.close()
                return str({'code':200,'mensaje':'Reservacion realizada! Confirme su reservación.'}) 
        except:
            return str({'code':400,'mensaje':'Ocurrio un error al realizar la reservacion'})


    #Completar reservacion de un vehiculo
    #idtarjeta,idreservacion
    @rpc(Integer,Integer,_returns=Unicode)
    def ConfirmarReservacion(self,idtarjeta,idreservacion):
        try:
            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("UPDATE Reserva_Auto SET idtarjeta = %s WHERE RA_id = %s",(idtarjeta,idreservacion))
            db.commit()
            db.close()
            return str({'code':200,'mensaje':'Reservacion confirmada'})
        except:
           return str({'code':400,'mensaje':'Ocurrio un error al compleetar la reservacion'}) 

    #Eliminar reservación parcial de un vehiculo
    #idreservacion
    @rpc(Integer,_returns=Unicode)
    def RechazarReservacion(self,idreservacion):
        try:
            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("UPDATE Automovil SET estado = 1 WHERE auto_id = (SELECT idvehiculo FROM Reserva_Auto WHERE RA_id = %s)",(idreservacion,))
                cursor.execute("DELETE FROM Reserva_Auto WHERE RA_id = %s", (idreservacion,))
            db.commit()
            db.close()
            return str({'code':200,'mensaje':'Reservacion elimanada'})
        except:
           return str({'code':400,'mensaje':'Ocurrio un error al compleetar la reservacion'}) 

    @rpc(Unicode,Integer,_returns=Unicode)
    def RegistrarCargaMasiva(self,datos,idusuario):
        try:
            valores = TraducirCsv(datos,idusuario)
            for x in valores:
                placa = x["placa"]
                marca = x["marca"]
                linea = x["linea"]
                modelo = x["modelo"]
                precio = x["precio_dia"]
                imagen = x["imagen"]
                estado = x["estado"]
                idciudad = x["idciudad"]
                if not Verificar_placa(placa):                    
                    db = Establecer_conexion()
                    with db.cursor() as cursor:
                        cursor.execute("INSERT INTO Automovil (placa,marca,linea,modelo,precio_dia,imagen,estado,idusuario,idciudad) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                                                            (placa,marca,linea,modelo,precio,imagen,estado,idusuario,idciudad))
                    db.commit()
                    db.close()
            return str({'code':200,'mensaje':"Automoviles registrados de forma exitosa"})
        except Exception as e:
            print(e)
            return str({'code':400,'mensaje':'Error al registrar automoviles'})

    
    @rpc(Unicode,_returns=Unicode)
    def MostarPreserva(self,idUsuario):
        try:
            lista = []
            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("SELECT A.auto_id, A.marca, A.linea, A.modelo, A.precio_dia, A.imagen, A.estado, A.idusuario, A.idciudad, A.placa, RA.RA_id FROM Automovil A, Reserva_Auto RA WHERE A.auto_id = RA.idvehiculo and RA.idtarjeta is null and RA.idusuario = %s",(idUsuario,))
                resultado = cursor.fetchall()
                for row in resultado:
                    lista.append({'idAuto': row[0], 'marca': row[1], 'linea':row[2], 'modelo':row[3], 'precio_dia':row[4], 'Imagen': row[5], 'estado':row[6], 'idUsuario': row[7], 'Ciudad': row[8], 'idplaca':row[9], 'idReservacion':row[10]})
            db.close()
            return str({'code':200,'mensaje':lista})
        except:
            return str({'code':400,'mensaje':'Error al obtener las reservaciones'})

    @rpc(Unicode,_returns=Unicode)
    def MostarReservaInmediata(self,idUsuario):
        try:
            lista = []
            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("SELECT A.auto_id, A.marca, A.linea, A.modelo, A.precio_dia, A.imagen, A.estado, A.idusuario, A.idciudad, A.placa, RA.RA_id FROM Automovil A, Reserva_Auto RA WHERE A.auto_id = RA.idvehiculo and RA.idtarjeta is not null and RA.idusuario = %s",(idUsuario,))
                resultado = cursor.fetchall()
                for row in resultado:
                    lista.append({'idAuto': row[0], 'marca': row[1], 'linea':row[2], 'modelo':row[3], 'precio_dia':row[4], 'Imagen': row[5], 'estado':row[6], 'idUsuario': row[7], 'Ciudad': row[8], 'idplaca':row[9], 'idReservacion':row[10]})
            db.close()
            return str({'code':200,'mensaje':lista})
        except:
            return str({'code':400,'mensaje':'Error al obtener las reservaciones'})

    @rpc(Integer,_returns=Unicode)
    def BusquedaAuto(self,id):
        try:
            cadena = ""
            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("SELECT A.idusuario,A.auto_id, A.placa,A.marca,A.linea,A.modelo,A.precio_dia,A.idciudad, C.nombre,P.idpais, P.nombre FROM Automovil A, Ciudad C, Pais P WHERE P.idpais = C.idpais and C.idciudad = A.idciudad and A.auto_id = %s",(id,))
                res = cursor.fetchall()
                for resultado in res:
                    cadena = {"idUsuario":Obtenernombre(resultado[0]),"id_vehiculo":resultado[1],"placa":resultado[2],"marca":resultado[3],"linea":resultado[4],"modelo":resultado[5],"precio_dia":resultado[6],"idCiudad":resultado[7],"ciudad":resultado[8],"idPais":resultado[9],"pais":resultado[10]}
            db.close()
            return str({'code':200,'mensaje':cadena})
        except:
            return str({'code':400,'mensaje':'Error al obtener los datos del automovil'})

    @rpc(_returns=Unicode)
    def Obtener_reservaciones(self):
        try:
            lista = []
            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("SELECT RA.RA_id,A.marca,A.linea,A.modelo,A.idusuario,C.nombre,P.nombre FROM Reserva_Auto RA, Automovil A, Ciudad C, Pais P WHERE RA.idvehiculo = A.auto_id and A.idciudad = C.idciudad and C.idpais = P.idpais")
                resultado = cursor.fetchall()
                for row in resultado:
                    lista.append({"ra_id":row[0],"marca":row[1],"linea":row[2],"modelo":row[3],"rentadora":Obtenernombre(row[4]),"ciudad":row[5],"pais":row[6]})
            db.close()
            return str({'code':200,'mensaje':lista})
        except:
            return str({'code':400,'mensaje':'Error al obtener los datos del automovil'})
    # ------------------------------------------------------- FASE 2 ---------------------------------------------------------------------------------------------------------------------
    @rpc(Unicode,_returns=Unicode)
    def Registrar_auto(self,formulario):
        try:
            detalles = json.loads(formulario)
            placa = detalles["placa"]
            c_personas = detalles["cantidad_personas"]
            precio = detalles["precio"]
            color = detalles["color"]
            marca = detalles["marca"]
            modelo = detalles["modelo"]
            linea = detalles["linea"]
            imagen = detalles["imagen"]
            user_id = detalles["usuario_id"] 

            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("INSERT INTO Automovil(placa,cantidad_personas,precio,color,marca,modelo,linea,imagen,usuario_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                                                    (placa,c_personas,precio,color,marca,modelo,linea,imagen,user_id))
            db.commit()
            db.close()
            respuesta = {'code':201, 'mensaje':'Automovil agregado'}
            return json.dumps(respuesta)
        except Exception as err:
            print(err)
            respuesta = {'code':400, 'mensaje':'Ocurrio un error al ingresar el automovil'}
            return json.dumps(respuesta)

    @rpc(Unicode,_returns=Unicode)
    def Registrar_carga_masiva(self,cadena):
        try:
            arreglo = cadena.split(';')
        
            for x in arreglo:
                if x != "":
                    detalles = json.loads(x)
                    c_personas = detalles["cantidad_personas"]
                    precio = detalles["precio"]
                    color = detalles["color"]
                    marca = detalles["marca"]
                    modelo = detalles["modelo"]
                    linea = detalles["linea"]
                    imagen = detalles["imagen"]
                    user_id = detalles["usuario_id"] 
                
                    
                    db = Establecer_conexion()
                    with db.cursor() as cursor:
                        cursor.execute("INSERT INTO Automovil(cantidad_personas,precio,color,marca,modelo,linea,imagen,usuario_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",
                                                            (c_personas,precio,color,marca,modelo,linea,imagen,user_id))
                    db.commit()
                    db.close()
                    
                else:
                    print("error")
                
        
            respuesta = {'code':201, 'mensaje': "Automoviles registrados correctamente"}
            return json.dumps(respuesta)
        except:
            respuesta = {'code':400, 'mensaje':'Ocurrio un error al registrar los automoviles'}
            return json.dumps(respuesta)

    @rpc(_returns=Unicode)
    def Obtener_autos(self):
        try:
            lista = []
            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("Select auto_id,cantidad_personas,precio,color,marca,modelo,linea,imagen,usuario_id from Automovil")
                result = cursor.fetchall()
                for x in result:
                    lista.append({
                        'auto_id': x[0],
                        'cantidad_personas': x[1],
                        'precio': x[2],
                        'color': x[3],
                        'marca': x[4],
                        'modelo': x[5],
                        'linea': x[6],
                        'imagen': x[7],
                        'usuario_id': x[8]
                    })
            db.close()
            respuesta = {'code':201, 'mensaje':lista}
            return json.dumps(respuesta)
        except:
            respuesta = {'code':404, 'mensaje':'Ocurrio un error al obtener la informacion'}
            return json.dumps(respuesta)

    @rpc(Integer,_returns=Unicode)
    def Obtener_autos_id(self,id):
        try:
            lista = []
            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("Select auto_id,cantidad_personas,precio,color,marca,modelo,linea,imagen from Automovil where usuario_id = %s",(id,))
                result = cursor.fetchall()
                for x in result:
                    lista.append({
                        'auto_id': x[0],
                        'cantidad_personas': x[1],
                        'precio': x[2],
                        'color': x[3],
                        'marca': x[4],
                        'modelo': x[5],
                        'linea': x[6],
                        'imagen': x[7],
                    })
            db.close()
            respuesta = {'code':201, 'data':lista}
            return json.dumps(respuesta)
        except:
            respuesta = {'code':404, 'mensaje':'Ocurrio un error al obtener la informacion'}
            return json.dumps(respuesta)

    @rpc(Integer,_returns=Unicode)
    def Obtener_reservaciones_id(self,id):
        try:
            lista = []
            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("Select R.fecha_i,R.fecha_f,R.precio, A.marca,A.modelo,A.linea from Reserva_Auto R, Automovil A where R.usuario_id = %s and R.auto_id = A.auto_id",(id,))
                result = cursor.fetchall()
                for x in result:
                    lista.append({
                        'fecha_i': str(x[0]),
                        'fecha_f': str(x[1]),
                        'precio': x[2],
                        'marca': x[3],
                        'modelo': x[4],
                        'linea': x[5], 
                    })
            db.close()
            respuesta = {'code':201, 'data':lista}
            return json.dumps(respuesta)
        except:
            respuesta = {'code':404, 'mensaje':'Ocurrio un error al obtener la informacion'}
            return json.dumps(respuesta)

  
    @rpc(Unicode,_returns=Unicode)
    def Realizar_reservacion(self,formulario):
        try:
            detalles = json.loads(formulario)
            fecha_i = detalles["fecha_i"]
            fecha_f = detalles["fecha_f"]
            estado = detalles["estado"]
            total = detalles["precio"]
            usuario_id = detalles["usuario_id"]
            auto_id = detalles["auto_id"]

            db = Establecer_conexion()
            with db.cursor() as cursor:
                cursor.execute("INSERT INTO Reserva_Auto (fecha_i,fecha_f,estado,precio,usuario_id,auto_id) VALUES (%s,%s,%s,%s,%s,%s)"
                                            , (fecha_i,fecha_f,estado,total,usuario_id,auto_id))
            db.commit()
            db.close()
            respuesta = {"code": 201, 'mensaje':'Reservación realizada'}
            return json.dumps(respuesta)
        except:
            respuesta = {"code": 400, 'mensaje':'Ocurrio un error al realizar la reservación'}
            return json.dumps(respuesta)

    
    
soap_app = Application([CarsService], 'spyne.examples.hello.soap',
                       in_protocol=Soap11(validator='lxml'),
                       out_protocol=Soap11())

wsgi_app = WsgiApplication(soap_app)

 
if __name__ == '__main__':
    import logging
    from wsgiref.simple_server import make_server
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('spyne.protocol.xml').setLevel(logging.DEBUG)
    logging.info("listening to http://127.0.0.1:9000")
    logging.info("wsdl is at: http://localhost:9000/?wsdl")
    server = make_server('0.0.0.0', 9000, wsgi_app)
    server.serve_forever()