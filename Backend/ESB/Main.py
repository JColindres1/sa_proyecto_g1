import json
import re
from urllib import response
import requests
import boto3
import uuid
import base64
import tempfile
from multiprocessing.connection import Client
from flask import Flask, jsonify, make_response, request
from flask_cors import CORS
from zeep import Client

import os
app = Flask(__name__)

CORS(app,origins=["*"])

HOST='localhost'
PORT='4000'
DEBUG=True


API_HABITACION = os.environ.get('PATH_HOTEL')
if API_HABITACION  is None:
    API_HABITACION = 'http://localhost:6500'

API_VUELO = os.environ.get('PATH_VUELO')
if API_VUELO  is None:
    API_VUELO = 'http://localhost:8000'

API_AUTO = os.environ.get('PATH_AUTO')
if API_AUTO  is None:
    API_AUTO = 'http://localhost:9000'

API_USER = os.environ.get('PATH_USER')
if API_USER  is None:
    API_USER = 'http://localhost:7000'

API_ESQUEMA = os.environ.get('PATH_ESQUEMA')
if API_ESQUEMA  is None:
    API_ESQUEMA = 'http://localhost:3500'

API_BANCO = os.environ.get('PATH_BANCO')
if API_BANCO  is None:
    API_BANCO = 'http://localhost:6000'

API_REPORTES = os.environ.get('PATH_REPORTES')
if API_REPORTES  is None:
    API_REPORTES = 'http://localhost:5000'


def Print_Message(text):
    print("======================================================================")
    print(text)
    print("======================================================================")

############################################## Servicio de Autos ###############################################
@app.route('/esb/autos/img-s3', methods=['POST'])
def guardarImg_AUTO():
    try:
        solicitud = request.get_json()
        b64 = solicitud["imagen"]
        #CREDENCIALES S3
        s3_client = boto3.client(
            's3',
            aws_access_key_id='AKIAW42GYMAQ6FK36ZNM',
            aws_secret_access_key='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',
        )

        #CONVERTIR A BINARIO LA IMAGEN 64
        ruta = "tmp-" + str(uuid.uuid4()) + ".jpg"
        x = b64.split(",")
        imagen_origen = bytearray(base64.b64decode(x[1]))

        #GUARDANDO EN BUCKET
        response = s3_client.put_object(
            Body=bytearray(imagen_origen), 
            Bucket='grupo1sa2022', 
            Key=ruta,
        )
        ruta_objeto = "https://grupo1sa2022.s3.amazonaws.com/" + ruta
        return jsonify({"success": True, "ruta": ruta_objeto}), 200, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}


@app.route('/esb/autos/registrar', methods=['POST'])
def esb_registrar_auto():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            placa = solicitud["placa"]
            marca = solicitud["marca"]
            linea = solicitud["linea"]
            modelo = solicitud["modelo"]
            precio = solicitud["precio_dia"]
            imagen = solicitud["imagen"]
            idusuario = solicitud["idusuario"]
            idciudad = solicitud["idciudad"]
            client = Client(wsdl= API_AUTO+"/?wsdl")
            respuesta = client.service.InsertarVehiculo(placa,marca,linea,modelo,precio,imagen,idusuario,idciudad)
            return respuesta
        else:
            return '',400
    except:
        return "Error"

@app.route('/esb/autos/', methods=['GET'])
def esb_obtener_datos():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            print(API_AUTO)
            client = Client(wsdl= API_AUTO+"?wsdl")
            respuesta = client.service.MostrarVehiculo()
            return respuesta
        else:
            return '',400
    except Exception as e:
        print(e)
        return "Error"

@app.route('/esb/auto/<id>', methods=['GET'])
def esb_obtener_auto(id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            print(API_AUTO)
            client = Client(wsdl= API_AUTO+"/?wsdl")
            respuesta = client.service.BusquedaAuto(id)
            return respuesta
        else:
            return '',400
    except:
        return "Error"

@app.route('/esb/autos/<int:identificador>', methods=['GET'])
def esb_obtener_vehiculos(identificador):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            client = Client(wsdl= API_AUTO+"/?wsdl")
            respuesta = client.service.MostrarDatos(identificador)
            return respuesta
        else:
            return '',400
    except:
        return "Error"

@app.route('/esb/autos/reservacion', methods=['POST'])
def esb_reservacion():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            fecha_inicio = solicitud["fecha_inicio"]
            fecha_fin = solicitud["fecha_fin"]
            idusuario = solicitud["idusuario"]
            idvehiculo = solicitud["idvehiculo"]
            idtarjeta = solicitud["idtarjeta"]
            montototal = solicitud["total"]
            client = Client(wsdl= API_AUTO+"/?wsdl")
            respuesta = client.service.Reservacion(fecha_inicio,fecha_fin,idusuario,idvehiculo,idtarjeta,montototal)
            return respuesta
        else:
            return '',400
    except Exception as e:
        print(e)
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/autos/reservacion/confirmar', methods=['PATCH'])
def esb_reservacion_confirmar():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            idtarjeta = solicitud["idtarjeta"]
            idreservacion = solicitud["idreservacion"]
            client = Client(wsdl= API_AUTO+"/?wsdl")
            respuesta = client.service.ConfirmarReservacion(idtarjeta,idreservacion)
            return respuesta
        else:
            return '',400
    except:
        return "Error"

@app.route('/esb/autos/reservacion/rechazar', methods=['DELETE'])
def esb_reservacion_rechazar():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            idreservacion = solicitud["idreservacion"]
            client = Client(wsdl= API_AUTO+"/?wsdl")
            respuesta = client.service.RechazarReservacion(idreservacion)
            return respuesta
        else:
            return '',400
    except:
        return "Error"

@app.route('/esb/autos/registrar/carga-masiva', methods=['POST'])
def esb_registar_carga():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            datos = solicitud["data"]
            idusuario = solicitud["idusuario"]
            client = Client(wsdl= API_AUTO+"/?wsdl")
            respuesta = client.service.RegistrarCargaMasiva(datos,idusuario)
            return respuesta
        else:
            return '',400
    except:
        return "Error"

@app.route('/esb/autos/prereserva/<idUsuario>', methods=['GET'])
def esb_obtener_prereservas(idUsuario):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            client = Client(wsdl= API_AUTO+"/?wsdl")
            respuesta = client.service.MostarPreserva(idUsuario)
            return respuesta
        else:
            return '',400
    except:
        return "Error"

@app.route('/esb/autos/reservacompleta/<idUsuario>', methods=['GET'])
def esb_obtener_reservas_completas(idUsuario):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            client = Client(wsdl= API_AUTO+"/?wsdl")
            respuesta = client.service.MostarReservaInmediata(idUsuario)
            return respuesta
        else:
            return '',400
    except:
        return "Error"

############################################## Servicio de Usuarios ###############################################
@app.route('/esb/users/login/borrar', methods=["POST"])
def ESB_login_borrar():
    try:

        solicitud = request.get_json()
        API_ENDPOINT = API_USER+"/user/borrar"
        reply = requests.post(url=API_ENDPOINT, json=solicitud)
        print(reply.json())
        return reply.json()        
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}


@app.route('/esb/users/login', methods=["POST"])
def ESB_login():
    try:

        solicitud = request.get_json()
        API_ENDPOINT = API_USER+"/login"
        reply = requests.post(url=API_ENDPOINT, json=solicitud)
        print(reply.json())
        return reply.json()        
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/users/login', methods=["GET"])
def ESB_login_JWT():
    try:

        solicitud = request.headers.get('Authentication')
        API_ENDPOINT = API_USER+"/login"
        reply = requests.get(url=API_ENDPOINT, headers={"Authentication": solicitud})
        print(reply.json())
        return reply.json()       
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/users/register', methods=["POST"])
def ESB_register():
    try:

        solicitud = request.get_json()
        API_ENDPOINT = API_USER+"/user/registro"
        reply = requests.post(url=API_ENDPOINT, json=solicitud)
        return reply.text
    except Exception as e:
        print(e)
        return Exception

@app.route('/esb/users/roles', methods=["GET"])
def ESB_roles():
    try:

        API_ENDPOINT = API_USER+"/lista/rol"
        reply = requests.get(url=API_ENDPOINT)
        return reply.text
    except Exception as e:
        print(e)
        return Exception

@app.route('/esb/users/paises/', methods=["GET"])
def ESB_paises():
    try:

        API_ENDPOINT = API_USER+"/lista/pais"
        reply = requests.get(url=API_ENDPOINT)
        return reply.text
    except Exception as e:
        print(e)
        return Exception

@app.route('/esb/users/ciudades', methods=["GET"])
def ESB_ciudades():
    try:
        pais = request.args.get('pais')
        API_ENDPOINT = API_USER+"/lista/ciudad/" + str(pais)
        reply = requests.get(url=API_ENDPOINT)
        return reply.text
    except Exception as e:
        print(e)
        return Exception

def getCredencialesRekognition():
	return boto3.client(
        'rekognition',
        aws_access_key_id='AKIAW42GYMAQ6FK36ZNM',
        aws_secret_access_key='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',
        region_name='us-east-1',
    )

@app.route('/esb/users/comparar', methods=["POST"])
def comparar_rostros():
    try:
        solicitud = request.get_json()
        base = solicitud["imagen"]
        foto = solicitud["correo"] + ".jpg"
        bucket = "grupo1sa2022"
        
        x = base.split(",")
        imagen_origen = bytearray(base64.b64decode(x[1]))
        rekognition_client = getCredencialesRekognition()
        response=rekognition_client.compare_faces(SimilarityThreshold=80,SourceImage={'Bytes': bytearray(imagen_origen)},TargetImage={'S3Object': {'Bucket': bucket,'Name': foto,}})
    
        for faceMatch in response['FaceMatches']:
            position = faceMatch['Face']['BoundingBox']
            similarity = str(faceMatch['Similarity'])
            print('The face at ' +
                str(position['Left']) + ' ' +
                str(position['Top']) +
                ' matches with ' + similarity + '% confidence')

        if float(similarity) > 90 :
            return jsonify({"success": True, "message": "Coincidencia encontrada"}), 200, {'Content-Type': 'application/json'}    
        return jsonify({"success": False, "message": "Su rostro no coincide"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "No se detectó rotros, inténtelo de nuevo"}), 500, {'Content-Type': 'application/json'}
	

############################################## INICIO Servicio de Habitaciones ###############################################
# Ruta para crear habitacion
@app.route('/esb/habitacion', methods=['POST'])
def crear_habitacion():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/habitacion".format(API_HABITACION)
            response = requests.post(url, data=request.data)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

#Ruta para obtener una habitacion
@app.route('/esb/habitacion/<habitacion_id>', methods=['GET'])
def obtener_habitacion(habitacion_id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/habitacion/{}".format(API_HABITACION, habitacion_id)
            response = requests.get(url, data=request.data)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

#Ruta para obtener varias habitaciones con paginacion
@app.route('/esb/habitacion', methods=['GET'])
def obtener_habitaciones():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/habitacion".format(API_HABITACION)
            response = requests.get(url, params=request.args.to_dict())
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/habitacion', methods=['PUT'])
def actualizar_habitacion():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/habitacion".format(API_HABITACION)
            response = requests.put(url, data=request.data)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/habitacion', methods=['DELETE'])
def eliminar_habitacion():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/habitacion".format(API_HABITACION)
            response = requests.delete(url, data=request.data)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

# Ruta para crear o actualizar el calendario de una habitacion
@app.route('/esb/habitacion/calendario', methods=['POST', 'PUT'])
def crear_actualizar_calendario():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/habitacion/calendario".format(API_HABITACION)
            response = requests.post(url, data=request.data)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

# Ruta para crear o actualizar el calendario de una habitacion
@app.route('/esb/habitacion/calendario/<habitacion_id>', methods=['GET'])
def obtener_calendario(habitacion_id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/habitacion/calendario/{}".format(API_HABITACION,habitacion_id)
            response = requests.get(url,params=request.args.to_dict())
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/habitacion/reserva',methods=['POST'])
def reservar_habitacion():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/habitacion/reserva".format(API_HABITACION)
            response = requests.post(url, data=request.data)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/habitacion/reserva/<RH_id>', methods=['GET'])
def obtener_reserva(RH_id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/habitacion/reserva/{}".format(API_HABITACION, RH_id)
            response = requests.get(url)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/habitacion/paises', methods=['GET'])
def obtener_paises():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/paises".format(API_HABITACION)
            response = requests.get(url)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/habitacion/ciudades/<pais_id>', methods=['GET'])
def obtener_ciudades(pais_id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/ciudades/{}".format(API_HABITACION, pais_id)
            response = requests.get(url)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/habitacion/reservacion/usuario/<usuario_id>', methods=['GET'])
def obtener_reservaciones_usuario(usuario_id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/habitacion/reservacion/usuario/{}".format(API_HABITACION,usuario_id)
            response = requests.get(url)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}


############################################## FIN Servicio de Habitaciones ###############################################

############################################## INICIO Servicio de Vuelos ###############################################
@app.route('/esb/vuelos', methods=['GET'])
def obtener_vuelos_filtro():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.args
            API_ENDPOINT = API_VUELO+"/vuelos"
            reply = requests.get(url=API_ENDPOINT, params=solicitud)
            print(reply.json())
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/flight/vaccination/<country_id>', methods=['GET'])
def get_schedule(country_id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/flight/vaccination/{}".format(API_VUELO, country_id)
            response = requests.get(url)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/flight/vaccination',methods=['POST'])
def insert_vaccine():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/flight/vaccination".format(API_VUELO)
            print(request.data)
            response = requests.post(url, data=request.data)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}    


@app.route('/esb/flight/country', methods=['GET'])
def get_countrys():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/flight/country".format(API_VUELO)
            response = requests.get(url)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}


@app.route('/esb/flight/city/<city_id>', methods=['GET'])
def get_citys(city_id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/flight/city/{}".format(API_VUELO, city_id)
            response = requests.get(url)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/esb/flight',methods=['POST'])
def set_flight():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/flight".format(API_VUELO)
            response = requests.post(url, data=request.data)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}    

@app.route('/esb/flight/charge',methods=['POST'])
def set_flights():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/flight/charge".format(API_VUELO)
            response = requests.post(url, data=request.data)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}  

@app.route('/esb/flight/validateSchedule/<country_id>',methods=['POST'])
def validate_vac(country_id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/flight/validateSchedule/{}".format(API_VUELO,country_id)
            response = requests.post(url, data=request.data)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}                    

@app.route('/esb/flight/<flight_id>', methods=['DELETE'])
def delete_flight(flight_id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/flight/{}".format(API_VUELO, flight_id)
            response = requests.delete(url)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}


@app.route('/esb/flight', methods=['GET'])
def get_flights():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            url ="{}/flight".format(API_VUELO)
            response = requests.get(url)
            print(response.json())
            return response.json()
        else:
            return '',400
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

# ------------------------------------------------------------------------------------------------------------------------
@app.route('/esb/cargar-vuelos',methods=['POST'])
def esb_cargar_vuelo():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            API_ENDPOINT = API_VUELO+"/cargar-vuelos"
            reply = requests.post(API_ENDPOINT,json=solicitud)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error en la solicitud'}

@app.route('/esb/reservar-vuelo',methods=['POST'])
def esb_reservar_vuelo():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            API_ENDPOINT = API_VUELO+"/reservar-vuelo"
            reply = requests.post(API_ENDPOINT,json=solicitud)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error en la solicitud'}

@app.route('/esb/reservar-vuelo/<id_usuario>',methods=['GET'])
def esb_obtener_vuelo(id_usuario):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENDPOINT = API_VUELO+"/reservar-vuelo/"+id_usuario
            reply = requests.get(API_ENDPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al obtener información'}

@app.route('/esb/vuelos/<id>',methods=['GET'])
def esb_obtener_vuelo_datos(id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENDPOINT = API_VUELO+"/vuelos/"+id
            reply = requests.get(API_ENDPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al obtener información'}
############################################## FIN Servicio de Vuelos ###############################################

# ------------------------------------------------------------------------------------------------------------------------------------------------- REPORTES-----------------------------------------
@app.route('/esb/reporte1', methods=['GET'])
def esb_reporte1():
    try:

        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENDPOINT = API_REPORTES+"/reporte1"
            reply = requests.get(API_ENDPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al obtener información'}

@app.route('/esb/reporte2', methods=['GET'])
def esb_reporte2():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENDPOINT = API_REPORTES+"/reporte2"
            reply = requests.get(API_ENDPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al obtener información'}

@app.route('/esb/reporte3', methods=['GET'])
def esb_reporte3():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENDPOINT = API_REPORTES+"/reporte3"
            reply = requests.get(API_ENDPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al obtener información'}

@app.route('/esb/reporte4', methods=['GET'])
def esb_reporte4():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENDPOINT = API_REPORTES+"/reporte4"
            reply = requests.get(API_ENDPOINT)
            return make_response(jsonify(reply.json()))
        return '',400
    except:
        return {'code':400,'mensaje':'Error al obtener información'}

@app.route('/esb/reporte5', methods=['GET'])
def esb_reporte5():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENDPOINT = API_REPORTES+"/reporte5"
            reply = requests.get(API_ENDPOINT)
            return make_response(jsonify(reply.json()))
        return '',400
    except:
        return {'code':400,'mensaje':'Error al obtener información'}

# ------------------------------------------------------------------------------------------------------------------------------------------------- ESQUEMA Y VACUNA -----------------------------------------

@app.route('/esb/vacuna/registrar', methods=['POST'])
def esb_vacuna_registrar():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            API_ENDPOINT = API_ESQUEMA+"/vacunas/registrar"
            reply = requests.post(API_ENDPOINT,json=solicitud)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/vacunas', methods=['GET'])
def esb_vacunas():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENPOINT = API_ESQUEMA+"/vacunas"
            reply = requests.get(API_ENPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/vacunas/dosis', methods=['POST'])
def esb_vacunadosis_registrar():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            API_ENDPOINT = API_ESQUEMA+"/vacunas/dosis"
            reply = requests.post(API_ENDPOINT,json=solicitud)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/vacunas/dosis', methods=['GET'])
def esb_vacunasdosis():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENPOINT = API_ESQUEMA+"/vacunas/dosis"
            reply = requests.get(API_ENPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}


@app.route('/esb/esquema/registrar', methods=['POST'])
def esb_esquema_registrar():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            API_ENDPOINT = API_ESQUEMA+"/esquema"
            reply = requests.post(API_ENDPOINT,json=solicitud)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/esquemas', methods=['GET'])
def esb_esquemas():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENPOINT = API_ESQUEMA+"/esquemas"
            reply = requests.get(API_ENPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/esquema/dosis', methods=['POST'])
def esb_esquema_dosis():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            API_ENDPOINT = API_ESQUEMA+"/esquema/dosis"
            reply = requests.post(API_ENDPOINT,json=solicitud)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/esquema/pais', methods=['POST'])
def esb_esquema_pais():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            API_ENDPOINT = API_ESQUEMA+"/esquema/pais"
            reply = requests.post(API_ENDPOINT,json=solicitud)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/esquema/pais/<id>', methods=['GET'])
def esb_obtener_esquema_pais(id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENDPOINT = API_ESQUEMA+"/esquema/pais/"+id
            reply = requests.get(API_ENDPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}


@app.route('/esb/esquema/dosis/<id>', methods=['GET'])
def esb_obtener_esquemadosis_pais(id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENDPOINT = API_ESQUEMA+"/esquema/dosis/"+id
            reply = requests.get(API_ENDPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/pasaporte/usuario/dosis', methods=['POST'])
def esb_pasaporte_agregar_dosis():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_data()
            API_ENDPOINT = API_ESQUEMA+"/pasaporte/usuario/dosis"
            reply = requests.post(API_ENDPOINT,json=solicitud)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/pasaporte/usuario/<id>', methods=['GET'])
def esb_obtener_pasaporte(id):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENDPOINT = API_ESQUEMA+"/pasaporte/usuario/"+id
            reply = requests.get(API_ENDPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/esquema/pais/usuario/<idpais>/<idusuario>', methods=['GET'])
def esb_validar_pasaporte_esquema(idpais,idusuario):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENDPOINT = API_ESQUEMA+"/esquema/pais/usuario/"+idpais+"/"+idusuario
            reply = requests.get(API_ENDPOINT)
            return make_response(jsonify(reply.json()))
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

# ------------------------------------------------------------------------------------------------------------------------------------------------- BANCOS -----------------------------------------
@app.route('/esb/banco/registrar/tarjeta', methods=['POST'])
def esb_banco_registrartarjeta():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            API_ENPOINT = API_BANCO+"/registrar/tarjeta"
            reply = requests.post(API_ENPOINT,json=solicitud)
            return reply.json()
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/banco/addtarjeta', methods=['POST'])
def esb_banco_agregar():
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            solicitud = request.get_json()
            API_ENPOINT = API_BANCO+"/AddTarjeta"
            reply = requests.post(API_ENPOINT,json=solicitud)
            return reply.json()
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

@app.route('/esb/banco/users/<id_usuario>/<totalCompra>', methods=['GET'])
def esb_banco_verificar(id_usuario,totalCompra):
    try:
        APIUSUARIO = API_USER+"/login"
        respuesta = requests.get(APIUSUARIO, headers={"Authentication":request.headers["Authentication"], "Access-Control-Allow-Origin":"*"})

        if respuesta.status_code == 200:
            API_ENPOINT = API_BANCO+"/users/getTarjeta/"+id_usuario+"/"+totalCompra
            reply = requests.get(API_ENPOINT)
            return reply.json()
        else:
            return '',400
    except:
        return {'code':400,'mensaje':'Error al procesar la solicitud'}

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port="4000")
