import json
from unittest import result
import mysql.connector

from flask import Flask, jsonify, request
from flask_cors import CORS  # pip install -U flask-cors

app = Flask(__name__)
CORS(app,origins=["*"])

HOST='localhost'
PORT='6000'
DEBUG=True

def connection_vuelos():
   return mysql.connector.connect(
            host = "35.225.114.145",
            user = "root",
            password = "db_g1",
            database = "Vuelos",
            port = "3309"
    )

def connection_autos():
   return mysql.connector.connect(
            host = "35.225.114.145",
            user = "root",
            password = "db_g1",
            database = "Automovil",
            port = "3308"
    )

def connection_usuarios():
   return mysql.connector.connect(
            host = "35.225.114.145",
            user = "root",
            password = "db_g1",
            database = "Usuario",
            port = "3306"
    )

def connection_hoteles():
   return mysql.connector.connect(
            host = "35.225.114.145",
            user = "root",
            password = "db_g1",
            database = "Room",
            port = "3307"
    )


def Obtener_rentadoras():
    try:
        lista = []
        db = connection_autos()
        with db.cursor() as cursor:
            cursor.execute("SELECT A.idusuario, Count(RA.idvehiculo)  FROM Automovil A, Reserva_Auto RA WHERE RA.idvehiculo = A.auto_id group by A.idusuario order by Count(RA.idvehiculo) desc limit 3")
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'id':row[0],'autos_rentados':row[1]})
        db.close()
        return lista
    except:
        return "Error"

def Obtener_nombres(jsonlista):
    try:
        lista = []
        for data in jsonlista:
            id = data["id"]
            cantidad = data["autos_rentados"]
            db = connection_usuarios()
            with db.cursor() as cursor:
                cursor.execute("SELECT nombre FROM Usuario WHERE usuario_id = %s",(id,))
                resultado = cursor.fetchone()
                lista.append({'nombre':resultado[0],'autos_rentados':cantidad})
            db.close()
        return lista
    except:
        return "Error"

@app.route('/reporte1',methods=['GET'])
def Reporte1():
    try:
        lista = Obtener_rentadoras()
        resultado = Obtener_nombres(lista)
        return json.dumps(resultado),200
    except:
        return {"mensaje":"Error al obtener informacion"},400

@app.route('/reporte2',methods=['GET'])
def Reporte2():
    try:
        lista = []
        db = connection_vuelos()
        with db.cursor() as cursor:
            cursor.execute("SELECT V.aerolinea, Count( RA.id_vuelo_ida) as boletos FROM Vuelo V, Reserva_vuelo RA WHERE RA.id_vuelo_ida = V.vuelo_id group by V.aerolinea order by Count(RA.id_vuelo_ida) desc limit 3")
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'aerolinea':row[0],'boletos':row[1]})
        db.close()
        return json.dumps(lista),200
    except:
        return {"mensaje":"Error al obtener informacion"},400

@app.route('/reporte3',methods=['GET'])
def Reporte3():
    try:
        lista = []
        db = connection_vuelos()
        with db.cursor() as cursor:
            cursor.execute("SELECT P.nombre, C.nombre, count(C.nombre)  FROM Vuelo V, Reserva_vuelo RV, Pais P, Ciudad C WHERE RV.id_vuelo_ida = V.vuelo_id and V.id_ciudad_destino = C.ciudad_id and C.pais_id = P.pais_id group by P.nombre, C.nombre order by count(C.nombre) desc limit 3")
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'aerolinea':row[0],'ciudad':row[1],'boletos':row[2]})
        db.close()
        return json.dumps(lista),200
    except:
        return {"mensaje":"Error al obtener informacion"},400

@app.route('/reporte4',methods=['GET'])
def Reporte4():
    try:
        lista = []
        db = connection_usuarios()
        with db.cursor() as cursor:
            cursor.execute("SELECT P.nombre, Count(P.nombre) FROM Usuario U, Pais P, Ciudad C WHERE U.ciudad_id = C.ciudad_id and C.pais_id = P.pais_id and U.rol_id = 1 group by P.nombre order by Count(P.nombre) desc limit 5")
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'nombre':row[0],'usuarios':row[1]})
        db.close()
        return json.dumps(lista),200
    except:
        return {"mensaje":"Error al obtener informacion"},400

def Obtener_reservaciones():
    try:
        lista = []
        db = connection_hoteles()
        with db.cursor() as cursor:
            cursor.execute("SELECT rh.usuario_id, Count(rh.usuario_id) FROM Reserva_Hotel rh group by rh.usuario_id order by Count(rh.usuario_id) desc limit 3;")
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'id':row[0],'veces_reservado':row[1]})
        db.close()
        return lista
    except:
        return "Error"

def Obtener_nombres_hoteles(jsonlista):
    try:
        lista = []
        for data in jsonlista:
            id = data["id"]
            cantidad = data["veces_reservado"]
            db = connection_usuarios()
            with db.cursor() as cursor:
                cursor.execute("SELECT nombre FROM Usuario WHERE usuario_id = %s",(id,))
                resultado = cursor.fetchone()
                lista.append({'nombre':resultado[0],'veces_reservado':cantidad})
            db.close()
        return lista
    except:
        return "Error"

@app.route('/reporte5',methods=['GET'])
def Reporte5():
    try:
        lista = Obtener_reservaciones()
        resultado = Obtener_nombres_hoteles(lista)
        return json.dumps(resultado),200
    except:
        return {"mensaje":"Error al obtener informacion"},400

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port="5000")
