# request library 
from unittest import result
from flask import Flask, jsonify, make_response, request
from flask_cors import CORS  # pip install -U flask-cors
app = Flask(__name__)
import json
CORS(app)

CORS(app,origins=["*"])

import mysql.connector

def connection():
   return mysql.connector.connect(
            host = "35.225.114.145",
            user = "root",
            password = "db_g1",
            database = "Vuelos",
            port = "3309"
    )


@app.route('/flight/vaccination/<country_id>',methods=['GET'])
def get_schedule(country_id):
    print(country_id)
    try:
        mydb = connection()
    except:
        return { 'msg': 'ERROR', 'data': 'Error al conectar la base de datos', 'code': 500 },404    
    try:
        schedule = format_schedule(country_id,mydb)


        return { 'msg': 'SUCCESS',"data":schedule,'code':200},201
    except:
        return { 'msg': 'ERROR', 'data': 'Error al insertar los datos', 'code': 400 },400
    return "hola"



@app.route('/flight/validateSchedule/<country_id>',methods=['POST'])
def validate(country_id):

    payload = json.loads(request.data)
    user_schedule = payload['data']
    print(user_schedule)
    try:
        mydb = connection()
    except:
        return { 'msg': 'ERROR', 'data': 'Error al conectar la base de datos', 'code': 500 },404    
    try:
        schedule = format_schedule_vac(country_id,mydb)
        if len(schedule) == 0 :
            return  { 'msg': 'SUCCESS',"data":True,'code':200},201
        isValid = validate_schedule(user_schedule,schedule)

        return { 'msg': 'SUCCESS',"data":isValid,'code':200},201
    except:
        return { 'msg': 'ERROR', 'data': 'Error al insertar los datos', 'code': 400 },400


def validate_schedule(user_schedule,country_schedule):
    for value in country_schedule:
        if value == user_schedule:
            return True
    return False

def format_schedule_vac(country_id,mydb):
    sql = "select DV.vacuna,DV.esquema_id,Pv.nombre  from Dosis_vacuna DV inner join Esquema_pais Ep on DV.esquema_id = Ep.esquema_id inner join Pais_vuelo Pv on Ep.pais_id = Pv.pais_id where Ep.pais_id = "+country_id
    
    mycursor = mydb.cursor()
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    print("arregloooo ")
    print(len(myresult))
    temp_schedule = []
    schedule = []
    temp_id = -1
    if len(myresult) == 0:
        return []
    for value in myresult:
        print (temp_id)
        if temp_id == -1:
            temp_id = value[1]
            
        if temp_id != value[1]:    
            schedule.append(temp_schedule)
            temp_schedule = []
            temp_id = value[1]
        

        print ("entrpe",value[0])
        temp_schedule.append(value[0])
    

    schedule.append(temp_schedule)
    print(schedule)
    return schedule    



def format_schedule(country_id,mydb):
    sql = "select DV.vacuna,DV.esquema_id,Pv.nombre  from Dosis_vacuna DV inner join Esquema_pais Ep on DV.esquema_id = Ep.esquema_id inner join Pais_vuelo Pv on Ep.pais_id = Pv.pais_id where Ep.pais_id = "+country_id

    mycursor = mydb.cursor()
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    print(myresult)
    temp_schedule = []
    schedule = []
    temp_id = -1
    for value in myresult:
        print (temp_id)
        if temp_id == -1:
            temp_id = value[1]
            
        if temp_id != value[1]:    
            schedule.append(temp_schedule)
            temp_schedule = []
            temp_id = value[1]
        

        print ("entrpe",value[0])
        temp_schedule.append({
            "vacuna":value[0],
            "id_esquema":value[1],
            "pais":value[2]
        }
        )
    

    schedule.append(temp_schedule)
    print(schedule)
    return schedule    

@app.route('/flight/vaccination',methods=['POST'])
def insert_vaccine():
    sql = "insert into Esquema_vacunacion values ()"
    print("muere acá")
    payload = json.loads(request.data)
    print(payload)
    
    vaccines = payload['vacunas']
    pais = payload['id_pais']
        
    try:
        mydb = connection()
    except:
        return { 'msg': 'ERROR', 'data': 'Error al conectar la base de datos', 'code': 500 },404

    mycursor = mydb.cursor()
    mycursor.execute(sql)
    mydb.commit()
    id_register = mycursor.lastrowid
    print(id_register)
    for vaccine in vaccines:
        print(vaccine)
        sql_vaccine = "INSERT INTO Dosis_vacuna (vacuna, esquema_id) VALUES (%s,%s)"
        val = (vaccine,id_register)   

        print(val)
        mycursor = mydb.cursor()
        mycursor.execute(sql_vaccine,val)
        mydb.commit()
    sql_vaccine = "INSERT INTO Esquema_pais (pais_id, esquema_id) VALUES (%s,%s)" 
    val = (pais,id_register)  
    print(val) 
    mycursor = mydb.cursor()
    mycursor.execute(sql_vaccine,val)
    mydb.commit()
    return { 'msg': 'SUCCESS',"data":"Registro exitoso",'code':201},201



# @app.route('/country',methods=['POST'])
# def insert_country():
#     print(request.json)
#     arr = request.json
#     mydb = connection()
#     mycursor = mydb.cursor()   
#     for value in arr:
#         sql = "INSERT INTO Pais_Vacuna (nombre,codido_pais) VALUES (%s,%s)"
    
#         val = (value['label'],value['value'])
#         print (val);
#         mycursor.execute(sql, val)
#         mydb.commit()
#         print (value['label'])

#     return "hola"




@app.route('/flight/country',methods=['GET'])
def get_country():
    sql = "SELECT * FROM Pais_vuelo"
    try:
        mydb = connection()
    except:
        return { 'msg': 'ERROR', 'data': 'Error al conectar la base de datos', 'code': 500 },404
    try:
        mycursor = mydb.cursor()
        mycursor.execute(sql)
        myresult = mycursor.fetchall()
        flights = []
        for x in myresult:
            flights.append({
            "id":x[0],
            "value": x[1],
             "label": x[1]
            })
        print(flights)
        return { 'msg': 'SUCCESS',"data":flights,'code':201},201
    except:
        return { 'msg': 'ERROR', 'data': 'Error al insertar los datos', 'code': 400 },400

@app.route('/flight/city/<city_id>',methods=['GET'])
def get_city(city_id):
    sql = "SELECT * FROM Ciudad_vuelo where pais_id =" + city_id
    try:
        mydb = connection()
    except:
        return { 'msg': 'ERROR', 'data': 'Error al conectar la base de datos', 'code': 500 },404
    try:
        mycursor = mydb.cursor()
        mycursor.execute(sql)
        myresult = mycursor.fetchall()
        flights = []
        for x in myresult:
            flights.append({
            "id":x[0],
            "value": x[1],
            "label": x[1],
            })
        return { 'msg': 'SUCCESS',"data":flights,'code':201},201
    except:
        return { 'msg': 'ERROR', 'data': 'Error al insertar los datos', 'code': 400 },400        
    


@app.route('/flight',methods=['POST'])
def set_flight():
    print(request.json)
    payload = json.loads(request.data)
    print(payload)
    sql = "INSERT INTO Vuelo (nombre_viaje, precio,cantidad_personas,pasajeros,tipo,ciudad_origen,ciudad_destino,estado) VALUES (%s, %s,%s, %s,%s, %s,%s, %s)"
    val = (payload['nombre_viaje'],payload['precio'], payload['cantidad_personas'],0,payload['tipo'],payload['ciudad_origen'],payload['ciudad_destino'],1)
    print(val)
    print(sql)
    mydb = connection()
    mycursor = mydb.cursor()
    mycursor.execute(sql, val)
    mydb.commit()
    print(mycursor.rowcount, "record inserted.")
    return {"data":"data"}


@app.route('/flight/charge',methods=['POST'])
def set_flights():
    # print(request.json)
    print("post vuelos")
    payload = json.loads(request.data)
    flights = payload
    for value in flights:
        insert_flight(value)
    return {"data":"data"}

def insert_flight(value):
    sql = "INSERT INTO Vuelo (nombre_viaje, precio,cantidad_personas,pasajeros,tipo,ciudad_origen,ciudad_destino,estado) VALUES (%s, %s,%s, %s,%s, %s,%s, %s)"
    val = (value['nombre_viaje'],value['precio'], value['cantidad_personas'],0,value['tipo'],int(value['ciudad_origen']),int(value['ciudad_destino']),1)
    print(val)
    mydb = connection()
    mycursor = mydb.cursor()    
    mycursor.execute(sql, val)
    mydb.commit()
    print(mycursor.rowcount, "record inserted.")    

# @app.route('/flight/<flight_id>',methods=['GET'])
# def get_flight(flight_id):
#     sql = "SELECT * FROM Viajes WHERE viaje_id = " + flight_id
#     # mycursor.execute(sql)
#     # myresult = mycursor.fetchall() 
#     # return {"data":myresult}

@app.route('/flight/<flight_id>',methods=['DELETE'])
def delete_flight(flight_id):
    # "SELECT * FROM Viajes WHERE viaje_id = " + flight_id
    sql ="UPDATE Vuelo SET estado = 0 WHERE viaje_id = " + flight_id
    # sql =" DELETE FROM Viajes"
    print (sql);
    try:
        mydb = connection()
    except:
        return { 'msg': 'ERROR', 'data': 'Error al conectar la base de datos', 'code': 404 },404
    try:   
        mycursor = mydb.cursor()    
        mycursor.execute(sql)
        mydb.commit()
        #myresult = mycursor.fetchall() 
        print("eliminado")
        return { 'msg': 'SUCCESS',"data":'registro eliminado','code':200},200
    except:
        return { 'msg': 'ERROR', 'data': 'Error al eliminar los datos', 'code': 400 },400

@app.route('/flight',methods=['GET'])
def get_flights():
    sql = "select V.viaje_id,V.nombre_viaje,V.precio,V.cantidad_personas,V.pasajeros,V.tipo,CV.nombre,CV.pais_id,CVV.nombre,CVV.pais_id,V.estado  from Vuelo V inner join Ciudad_vuelo CV on V.ciudad_origen = CV.ciudad_id inner join Ciudad_vuelo CVV on V.ciudad_destino = CVV.ciudad_id where V.estado = 1"
    try:
        mydb = connection()
    except:
        return { 'msg': 'ERROR', 'data': 'Error al conectar la base de datos', 'code': 500 },404
    try:
        mycursor = mydb.cursor()
        mycursor.execute(sql)
        myresult = mycursor.fetchall()
        flights = []
        for x in myresult:
            flights.append({
            "id":x[0],
            "flight_name": x[1],
            "price": x[2],
            "people": x[3],
            "passenger":x[4],
            "type": x[5],
            "city_from": x[6],
            "country_from": x[7],
            "city_to": x[8],
            "country_to": x[9],
            "state":x[10]
            })
   
        return { 'msg': 'SUCCESS',"data":flights,'code':201},201
    except:
        return { 'msg': 'ERROR', 'data': 'Error al insertar los datos', 'code': 400 },400




# @app.route('flight/reserva',methods=['POST'])
# def set_res():
#     sql = "INSERT INTO Reserva_Vuelo (reseña,calificacion,precio,usuario_id,viaje_id) VALUES (%s, %s,%s, %s,%s)"
#     val = (request.json['reseña'],request.json['calificacion'], request.json['precio'],request.json['usuario_id'],request.json['viaje_id'])
#     # mycursor.execute(sql, val)
#     return {"data":"data"}





# ----------------------------------------------------------- FASE 3 ---------------------------------------------------------------------------
@app.route('/vuelos',methods=['GET'])
def Obtener_vuelos_filtro():
    try:
        lista = []
        id_ciudad_origen = request.args['id_ciudad_origen']
        id_ciudad_destino = request.args['id_ciudad_destino']
        precio_mayor = request.args['precio_mayor']
        precio_menor = request.args['precio_menor']
        fecha_salida = request.args['fecha_salida']
        consulta = ico = icd = pm = fs = ""
        if int(id_ciudad_origen) > 0:
            ico = "AND V.id_ciudad_origen = " + id_ciudad_origen
        if int(id_ciudad_destino) > 0:
            icd = "AND V.id_ciudad_destino = " + id_ciudad_destino
        if int(precio_mayor) > 0:
            pm = "AND V.precio BETWEEN " + precio_menor + " AND " +precio_mayor
        if fecha_salida != '':
            fs = "AND DATE(V.salida) =  '" + fecha_salida + "'"
        consulta = """
            SELECT V.aerolinea, V.salida, V.vuelo_id, PO.pais_id, PO.nombre, CO.ciudad_id, CO.nombre, PD.pais_id, PD.nombre, CD.ciudad_id, CD.nombre, V.precio, V.capacidad, V.id
            FROM Vuelos.Vuelo V, Vuelos.Ciudad CD, Vuelos.Ciudad CO, Vuelos.Pais PD, Vuelos.Pais PO
            WHERE V.id_ciudad_destino = CD.ciudad_id
            AND V.id_ciudad_origen = CO.ciudad_id
            AND CO.pais_id = PO.pais_id
            AND CD.pais_id = PD.pais_id
        """
        db = connection()
        with db.cursor() as cursor:
            cursor.execute(consulta + ico + icd + pm + fs)
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'aerolinea':row[0],'salida':row[1].strftime("%Y-%m-%d %H:%M"),'id_vuelo':row[2],'id_pais_origen':row[3],'nombre_pais_origen':row[4],'id_ciudad_origen':row[5],'nombre_ciudad_origen':row[6],'id_pais_destino':row[7],'nombre_pais_destino':row[8],'id_ciudad_destino':row[9],'nombre_ciudad_destino':row[10],'precio':row[11],'capacidad':row[12],'id':row[13]})
        db.close()
        return make_response(jsonify(lista)),200
    except:
        return {"mensaje":"Error al obtener informacion"},400


def Verificar(identificador):
    db = connection()
    with db.cursor() as cursor:
        cursor.execute("SELECT vuelo_id FROM Vuelo where id = %s",(identificador,))
        row = cursor.fetchone()
    db.close()
    if row is not None:
        return True
    else:
        return False

@app.route('/cargar-vuelos',methods=['POST'])
def Cargar_vuelo():
    try:
        payload = json.loads(request.data)
        for vuelo in payload:
            if not Verificar(vuelo['id']):
                sql = "INSERT INTO Vuelo (aerolinea,salida,id_ciudad_origen,id_ciudad_destino,precio,capacidad,id) VALUES (%s,%s,%s,%s,%s,%s,%s)"
                val = (vuelo['aerolinia'],vuelo['salida'],vuelo['id_ciudad_origen'],vuelo['id_ciudad_destino'],vuelo['precio'],vuelo['capacidad'],vuelo['id'])
                db = connection()
                with db.cursor() as cursor:
                    cursor.execute(sql,val)
                db.commit()
                db.close() 
        return {"mensaje":"Informacion registrada exitosamente."},200
    except:
        return {"mensaje":"Error al registrar informacion"},400

@app.route('/reservar-vuelo',methods=['POST'])
def Reservar_vuelo():
    try:
        payload = json.loads(request.data)
        for vuelo in payload:
            sql = "INSERT INTO Reserva_vuelo (id_vuelo_ida,id_vuelo_vuelta,id_usuario) VALUES (%s,%s,%s)"
            val = (vuelo['id_vuelo_ida'],vuelo['id_vuelo_vuelta'],vuelo['id_usuario'])
            db = connection()
            with db.cursor() as cursor:
                cursor.execute(sql,val)
            db.commit()
            db.close() 
        return {"mensaje":"Reservacion realizada."},200
    except:
        return {"mensaje":"Error al realizar la reservacion"},400

@app.route('/reservar-vuelo/<id_usuario>',methods=['GET'])
def Obtener_reservacion(id_usuario):
    try:
        lista = []
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("SELECT R.id_vuelo_ida, R.id_usuario, V.aerolinea, C1.nombre, C2.nombre, V.salida FROM Reserva_vuelo R, Vuelo V, Ciudad C1, Ciudad C2 WHERE R.id_vuelo_ida = V.vuelo_id and V.id_ciudad_origen = C1.ciudad_id and V.id_ciudad_destino = C2.ciudad_id and  R.id_usuario = %s", (id_usuario,))
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'id_vuelo':row[0],'id_usuario':row[1],'aerolinea':row[2],'ciudad_origen':row[3],'ciudad_destino':row[4],'fecha':row[5]})
        db.close()
        return make_response(jsonify(lista)),200
    except Exception as err:
        return {"mensaje":"Error al recolectar informacion"},400

@app.route('/vuelos/<id>',methods=['GET'])
def Obtener_vuelos(id):
    try:
        cadena = ""
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("SELECT V.aerolinea, V.salida, V.vuelo_id, P2.pais_id, C1.ciudad_id, C1.nombre, C2.ciudad_id, C2.nombre, V.precio, V.id  FROM Vuelo V, Ciudad C1, Ciudad C2, Pais P1, Pais P2 WHERE V.vuelo_id = %s and V.id_ciudad_origen = C1.ciudad_id and V.id_ciudad_destino = C2.ciudad_id and C1.pais_id = P1.pais_id and C2.pais_id = P2.pais_id",(id,))
            resultado = cursor.fetchall()
            for row in resultado:
                cadena = {"aerolinea":row[0],"salida":row[1],"id_vuelo":row[2], "id_pais_destino":row[3],"id_ciudad_origen":row[4], "nombre_ciudad_origen":row[5],"id_ciudad_destino":row[6],"nombre_ciudad_destino":row[7],"precio":row[8],"id":row[9]}
        db.close()
        return cadena,200
    except Exception as e:
        print(e)
        return {"mensaje":"Error al recolectar informacion"},400        

# ----------------------------------------------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port="8000")