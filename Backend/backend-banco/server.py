import json
from os import stat
from telnetlib import STATUS
from unittest import result
import flask
import mysql.connector

from flask import Flask, flash, jsonify, request
from flask_cors import CORS  # pip install -U flask-cors

app = Flask(__name__)
CORS(app,origins=["*"])

HOST='localhost'
PORT='6000'
DEBUG=True

def connection():
   return mysql.connector.connect(
            host = "35.225.114.145",
            user = "root",
            password = "db_g1",
            database = "Usuario",
            port = "3306"
    )

@app.route('/registrar/tarjeta',methods=['POST'])
def Registrar_tarjeta_banco():
    try:
        solicitud = json.loads(request.data)
        nombre = solicitud["nombre"]
        numero = solicitud["numero"]
        cvv = solicitud["cvv"]
        fecha = solicitud["fecha"]
        monto = solicitud["monto"]
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("INSERT INTO TarjetaCredito (nombre,numero,cvv,fecha,monto) VALUES (%s,%s,%s,%s,%s)",
                                                (nombre,numero,cvv,fecha,monto))
        db.commit()
        db.close()
        return {'mensaje':'Tarjeta registrada de forma exitosa'}, 200
    except:
        return {'mensaje':'Error al agregar la tarjeta de credito'} , 400


@app.route('/AddTarjeta',methods=['POST'])
def Registrar_tarjeta():
    try: 
        solicitud = json.loads(request.data)
        nombre = solicitud["nombre"]
        numero = solicitud["numero"]
        cvv = solicitud["cvv"]
        fecha = solicitud["fecha"]
        idusuario = solicitud["idUsuario"]
        monto = solicitud["monto"]
        if monto < 0:
            db = connection()
            with db.cursor() as cursor:
                cursor.execute("UPDATE TarjetaCredito SET usuario_id = %s WHERE numero = %s",
                                                    (idusuario,numero))
            db.commit()
            db.close()
            return {"mensaje":"Tarjeta agregada al usuario."},200
        else:
            db = connection()
            with db.cursor() as cursor:
                cursor.execute("INSERT INTO TarjetaCredito (nombre,numero,cvv,fecha,usuario_id,monto) VALUES (%s,%s,%s,%s,%s,%s)",
                                                    (nombre,numero,cvv,fecha,idusuario,monto))
            db.commit()
            db.close()
            return {"mensaje":"Tarjeta agregada de forma exitosa"},200
    except:
        return {"mensaje":"Error al agregar la tarjeta de credito"},400

@app.route('/users/getTarjeta/<id_usuario>/<totalCompra>',methods=['GET'])
def Verificar_tarjeta(id_usuario,totalCompra):
    try: 
        if Verificar_Saldo(id_usuario, totalCompra):
            db = connection()
            with db.cursor() as cursor:
                cursor.execute("UPDATE TarjetaCredito SET monto = (monto - (%s * 0.04))  WHERE usuario_id = %s" ,(totalCompra,id_usuario))
            db.commit()
            db.close()
            return {"mensaje":True},200
        else:
            return {"mensaje":False},200
    except:
        return {"mensaje":"No se tiene registrada una tarjeta de credito"},400

def Verificar_Saldo(id,saldo):
    try: 
        print("entro a la verificacion")
        bandera = True
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("SELECT monto FROM TarjetaCredito WHERE usuario_id = %s",(id,))
            resultado = cursor.fetchone()
            if (float(resultado[0]) - float(saldo)) < 0:
                bandera = False
        db.close()
        return bandera
    except:
        return False


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port="6000")
