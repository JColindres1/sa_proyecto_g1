from unittest import result
from flask import Flask, request, jsonify
import json
from flask_cors import CORS
from flask_mysqldb import MySQL
from dotenv import dotenv_values
from math import ceil
from datetime import date

env = dotenv_values(".env")

app = Flask(__name__)
CORS(app, origins=["*"])

# Informacion de MySQL
app.config['MYSQL_HOST'] = "35.225.114.145"
app.config['MYSQL_USER'] = "root"
app.config['MYSQL_PASSWORD'] = "db_g1"
app.config['MYSQL_PORT'] = 3307
app.config['MYSQL_DB'] = "Room"
app.config['MYSQL_CURSORCLASS'] = "DictCursor"

#Conexion a mysql
mysql = MySQL(app)

# Variables para indicar como correr el servidor
HOST='0.0.0.0'
PORT='6500'
DEBUG=True

def print_message(message):
    print("======================================================================")
    print(message)
    print("======================================================================")

def execute(sql, params):
    try:
        cursor = mysql.connection.cursor()
        cursor.execute(sql, params)
        print(cursor.__dict__)
        mysql.connection.commit()
        return {
            'id': cursor.lastrowid,
            'results': cursor.fetchall()
        }
    except Exception as e:
        mysql.connection.rollback()
        print(e)
        return False


@app.route("/")
def verifyService():
    return 'Server Running in {0}:{1}!'.format(str(HOST), PORT)

# Ruta para crear habitacion
@app.route('/habitacion', methods=['POST'])
def crear_habitacion():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        # Verificamos que venga la petición completa
        if not ('nombre' in payload and 'precio' in payload and 'cantidad_personas' in payload and 'descripcion' in payload and 'ciudad_id' in payload and 'estado' in payload and 'usuario_id' in payload):
            print_message("Petición con datos incompletos!")
            return jsonify({"message": "Petición con datos incompletos!", "success": False}), 422, {'Content-Type': 'application/json'}

        # Petición recibida
        print_message(payload)

        # Generación de comando SQL
        sql = "INSERT INTO Habitacion (nombre, precio,cantidad_personas,descripcion,estado,usuario_id,ciudad_id) VALUES (%s, %s,%s, %s,%s, %s,%s)"
        val = (payload['nombre'],payload['precio'], payload['cantidad_personas'],payload['descripcion'],payload['estado'],payload['usuario_id'],payload['ciudad_id'])

        result = execute(sql, val)
        if(result):
            return jsonify({"success": True, "habitacion": result['id'], "message": "Habitación registrada"}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Habitación no se pudo registrar"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

#Ruta para obtener una habitacion
@app.route('/habitacion/<habitacion_id>', methods=['GET'])
def obtener_habitacion(habitacion_id):
    try:
        # Petición recibida
        print_message(habitacion_id)

        # Generación de comando SQL
        sql = "SELECT * FROM Habitacion h INNER JOIN Ciudad c ON c.ciudad_id = h.ciudad_id INNER JOIN Pais p ON p.pais_id = c.pais_id WHERE habitacion_id = %s AND estado = 1"
        val = (habitacion_id,)

        result = execute(sql, val)
        if(result):
            return jsonify({"success": True, "habitacion": (result['results'][0] if len(result['results'])>0 else {})}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Habitación no encontrada"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

#Ruta para obtener varias habitaciones con paginacion
@app.route('/habitacion', methods=['GET'])
def obtener_habitaciones():
    try:
        # Obtener argumentos para paginacion
        args = request.args

        # Parametros de paginacion
        page = args.get('page', 1, int)
        limit = args.get('limit', 20, int)
        offset = (page - 1) * limit

        # Parametros de búsqueda
        pais = '%' + args.get('pais', '', str) + '%'
        ciudad = '%' + args.get('ciudad', '', str) + '%'
        personas = args.get('personas', 0, int)
        op = args.get('op', '>', str)
        precio_min = args.get('min', 0, float)
        precio_max = args.get('max', 999999999, float)
        fecha_inicio = args.get('fecha_inicio', '', str)
        fecha_fin = args.get('fecha_fin', '', str)
        nombre = '%' + args.get('nombre', '', str) + '%'
        usuario_id = args.get('usuario_id', 0, int)

        # Generación de comando SQL
        sql = '''SELECT  h.*, c.*, p.* FROM Habitacion h
        INNER JOIN Ciudad c ON c.ciudad_id = h.ciudad_id
        INNER JOIN Pais p ON p.pais_id = c.pais_id
        WHERE p.nombre_pais LIKE %s AND c.nombre_ciudad LIKE %s AND h.nombre LIKE %s
        AND cantidad_personas {} %s  AND precio BETWEEN %s AND %s AND h.estado = 1'''.format(op)
        sql += " AND h.habitacion_id IN (SELECT habitacion_id FROM Calendario WHERE fecha_i BETWEEN DATE(%s) AND DATE(%s) AND estado = 1 GROUP BY habitacion_id) " if fecha_inicio and usuario_id == 0 else ''
        sql += " AND h.habitacion_id IN (SELECT habitacion_id FROM Calendario WHERE fecha_i BETWEEN DATE(%s) AND DATE(%s) AND estado = 1 GROUP BY habitacion_id) AND usuario_id = %s" if fecha_inicio and usuario_id !=0 else ''
        sql += " AND usuario_id = %s" if not fecha_inicio and usuario_id != 0 else ''
        sql += ' LIMIT %s, %s'
        if(fecha_inicio and usuario_id == 0):
            val = (pais, ciudad, nombre, personas, precio_min, precio_max, fecha_inicio, fecha_fin, offset, limit)
        elif (fecha_inicio and usuario_id != 0):
            val = (pais, ciudad, nombre, personas, precio_min, precio_max, fecha_inicio, fecha_fin, usuario_id, offset, limit)
        elif (not fecha_inicio and usuario_id != 0):
            val = (pais, ciudad, nombre, personas, precio_min, precio_max, usuario_id, offset, limit)
        else:
            val = (pais, ciudad, nombre, personas, precio_min, precio_max, offset, limit)
        search_result = execute(sql, val)


        if(search_result):
            # Si se obtuvo resultado se puede obtener la paginacion
            sql = '''SELECT COUNT(*) as cantidad FROM Habitacion h
            INNER JOIN Ciudad c ON c.ciudad_id = h.ciudad_id
            INNER JOIN Pais p ON p.pais_id = c.pais_id
            WHERE p.nombre_pais LIKE %s AND c.nombre_ciudad LIKE %s AND h.nombre LIKE %s AND cantidad_personas {} %s  AND precio BETWEEN %s AND %s AND h.estado = 1'''.format(op)
            sql += " AND h.habitacion_id IN (SELECT habitacion_id FROM Calendario WHERE fecha_i BETWEEN DATE(%s) AND DATE(%s) AND estado = 1 GROUP BY habitacion_id) " if fecha_inicio and usuario_id == 0 else ''
            sql += " AND h.habitacion_id IN (SELECT habitacion_id FROM Calendario WHERE fecha_i BETWEEN DATE(%s) AND DATE(%s) AND estado = 1 GROUP BY habitacion_id) AND usuario_id = %s" if fecha_inicio and usuario_id !=0 else ''
            sql += " AND usuario_id = %s" if not fecha_inicio and usuario_id != 0 else ''
            if(fecha_inicio and usuario_id == 0):
                val = (pais, ciudad, nombre, personas, precio_min, precio_max, fecha_inicio, fecha_fin)
            elif (fecha_inicio and usuario_id != 0):
                val = (pais, ciudad, nombre, personas, precio_min, precio_max, fecha_inicio, fecha_fin, usuario_id)
            elif (not fecha_inicio and usuario_id != 0):
                val = (pais, ciudad, nombre, personas, precio_min, precio_max, usuario_id)
            else:
                val = (pais, ciudad, nombre, personas, precio_min, precio_max)
            search_pagination = execute(sql, val)

            pagination = {
                "page": page,
                "total_pages": ceil(search_pagination['results'][0]['cantidad'] / limit),
                "total_result": search_pagination['results'][0]['cantidad'],
                "offset": offset,
                "limit": limit

            }
            return jsonify({"success": True, "habitaciones": search_result['results'], "pagination": pagination}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Habitación no encontrada"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/habitacion', methods=['PUT'])
def actualizar_habitacion():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        # Verificamos que venga la petición completa
        if not ('habitacion_id' in payload and 'nombre' in payload and 'precio' in payload and 'cantidad_personas' in payload and 'descripcion' in payload and 'ciudad_id' in payload and 'estado' in payload):
            print_message("Petición con datos incompletos!")
            return jsonify({"message": "Petición con datos incompletos!", "success": False}), 422, {'Content-Type': 'application/json'}

        # Petición recibida
        print_message(payload)

        # Verificar que exista la habitacion
        habitacion = obtener_habitacion(payload['habitacion_id'])

        if not habitacion:
            print_message("Habitación no encontrada para actualizar")
            return jsonify({"message": "Habitación no encontrada para actualizar", "success": False}), 200, {'Content-Type': 'application/json'}


        # Generación de comando SQL
        sql = "UPDATE Habitacion SET nombre = %s, precio = %s, cantidad_personas =%s , descripcion =%s, ciudad_id =%s, estado =%s WHERE habitacion_id = %s"
        val = (payload['nombre'],payload['precio'], payload['cantidad_personas'],payload['descripcion'],payload['ciudad_id'],payload['estado'],payload['habitacion_id'])

        result = execute(sql, val)
        if(result):
            return jsonify({"success": True, "message": "Habitación actualizada"}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Habitación no se pudo actualizar"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/habitacion', methods=['DELETE'])
def eliminar_habitacion():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        # Verificamos que venga la petición completa
        if not 'habitacion_id' in payload:
            print_message("Petición con datos incompletos!")
            return jsonify({"message": "Petición con datos incompletos!", "success": False}), 422, {'Content-Type': 'application/json'}

        # Petición recibida
        print_message(payload)

        # Verificar que exista la habitacion
        habitacion = obtener_habitacion(payload['habitacion_id'])

        if not habitacion:
            print_message("Habitación no encontrada para elimnar")
            return jsonify({"message": "Habitación no encontrada para elimnar", "success": False}), 200, {'Content-Type': 'application/json'}


        # Generación de comando SQL
        sql = "UPDATE Habitacion SET  estado = 0 WHERE habitacion_id = %s"
        val = (payload['habitacion_id'],)

        result = execute(sql, val)
        if(result):
            return jsonify({"success": True, "message": "Habitación eliminada"}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Habitación no se pudo eliminar"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

# Ruta para crear o actualizar el calendario de una habitacion
@app.route('/habitacion/calendario', methods=['POST', 'PUT'])
def crear_actualizar_calendario():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        # Verificamos que venga la petición completa
        if not ('habitacion_id' in payload and 'fechas' in payload ):
            print_message("Petición con datos incompletos!")
            return jsonify({"message": "Petición con datos incompletos!", "success": False}), 422, {'Content-Type': 'application/json'}

        # Petición recibida
        print_message(payload)

        # Verificar que exista la habitacion
        habitacion = obtener_habitacion(payload['habitacion_id'])

        if not habitacion:
            print_message("Habitación no encontrada para crear calendario")
            return jsonify({"message": "Habitación no encontrada para crear calendario", "success": False}), 200, {'Content-Type': 'application/json'}

        creadas = []
        actualizadas = []
        for fecha in payload['fechas']:
            # Verificar si la fecha exite
            sql = "SELECT * FROM Calendario WHERE fecha_i = %s AND habitacion_id = %s"
            val = (fecha['fecha'], payload['habitacion_id'])
            result = execute(sql, val)

            # si ya esta creado lo actualiza, sino lo crea
            if(len(result['results']) > 0):
                # Generación de comando SQL
                sql = "UPDATE Calendario SET estado = %s WHERE id_calendario = %s"
                val = (fecha['estado'], result['results'][0]['id_calendario'])
                result = execute(sql, val)
                actualizadas.append(fecha['fecha'])
            else:
                # Generación de comando SQL
                sql = "INSERT INTO Calendario (fecha_i, estado, habitacion_id) VALUES (%s, %s, %s)"
                val = (fecha['fecha'],fecha['estado'], payload['habitacion_id'])
                result = execute(sql, val)
                creadas.append(fecha['fecha'])

        if(len(creadas) > 0 or len(actualizadas) > 0):
            return jsonify({"success": True,"fechas_creadas": creadas, "fechas_actualizadas": actualizadas, "message": "Calendario registrado"}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Calendario no se pudo registrar"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

# Ruta para crear o actualizar el calendario de una habitacion
@app.route('/habitacion/calendario/<habitacion_id>', methods=['GET'])
def obtener_calendario(habitacion_id):
    try:
        # Obtener argumentos para paginacion
        args = request.args

        # Parametros de búsqueda
        fecha_inicio = args.get('fecha_inicio', '', str)
        fecha_fin = args.get('fecha_fin', '', str)

        # Generación de comando SQL
        sql = "SELECT id_calendario, DATE_FORMAT(fecha_i, \"%%Y/%%m/%%d\") as fecha_i, estado FROM Calendario WHERE habitacion_id = %s "
        sql += " AND fecha_i BETWEEN DATE(%s) AND DATE(%s) " if fecha_inicio else ''
        val = (habitacion_id, fecha_inicio, fecha_fin) if fecha_inicio else (habitacion_id,)
        search_result = execute(sql, val)


        if(search_result):
            return jsonify({"success": True, "calendario": search_result['results']}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Habitación no encontrada"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/habitacion/reserva',methods=['POST'])
def reservar_habitacion():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        # Verificamos que venga la petición completa
        if not ('habitacion_id' in payload and 'usuario_id' in payload and 'fecha_inicio' in payload and 'fecha_fin' in payload):
            print_message("Petición con datos incompletos!")
            return jsonify({"message": "Petición con datos incompletos!", "success": False}), 422, {'Content-Type': 'application/json'}

        # Petición recibida
        print_message(payload)

        # Verificar que exista la habitacion
        habitacion = obtener_habitacion(payload['habitacion_id'])

        if not habitacion:
            print_message("Habitación no encontrada para la reservacion")
            return jsonify({"message": "Habitación no encontrada para la reservacion", "success": False}), 200, {'Content-Type': 'application/json'}

        # Generación de comando SQL
        sql = "SELECT id_calendario, DATE_FORMAT(fecha_i, \"%%Y/%%m/%%d\") as fecha_i, estado FROM Calendario WHERE habitacion_id = %s AND DATE(fecha_i) BETWEEN DATE(%s) AND DATE(%s)"
        val = (payload['habitacion_id'], payload['fecha_inicio'], payload['fecha_fin'])
        fechas = execute(sql,val)

        ocupadas = []
        disponibles = []
        fecha_fin = payload['fecha_fin'].split('/')
        fecha_inicio = payload['fecha_inicio'].split('/')
        cant_dias = date(int(fecha_fin[0]), int(fecha_fin[1]), int(fecha_fin[2])) - date(int(fecha_inicio[0]), int(fecha_inicio[1]), int(fecha_inicio[2]))
        if len(fechas['results']) == 0 or len(fechas['results']) != cant_dias.days + 1 :
            return jsonify({"success": False,  "message": "Fechas no disponibles!"}), 200, {'Content-Type': 'application/json'}
        for fecha in fechas['results']:
            if fecha['estado'] == 0:
                ocupadas.append(fecha['fecha_i'])
            else:
                disponibles.append(fecha['fecha_i'])

        if len(ocupadas) > 0:
            return jsonify({"success": False, "fechas_ocupadas": ocupadas, "fechas_dispobibles": disponibles, "message": "No se pudo realizar la reservación ya que hay fechas ocupadas!"}), 200, {'Content-Type': 'application/json'}

        # Generación de comando SQL
        sql = "INSERT INTO Reserva_Hotel (usuario_id,habitacion_id, fecha_inicio, fecha_fin) VALUES (%s,%s,%s,%s)"
        val = (payload['usuario_id'], payload['habitacion_id'],payload['fecha_inicio'], payload['fecha_fin'])

        result = execute(sql, val)
        if(result):
            for fecha in fechas['results']:
                print(fecha['id_calendario'])
                # Generación de comando SQL
                sql = "UPDATE Calendario SET estado = 0 WHERE id_calendario = %s"
                val = (fecha['id_calendario'],)
                execute(sql,val)

            return jsonify({"success": True, "reserva": result['id'], "message": "Reserva realizada!"}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Habitación no se pudo reservar"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/habitacion/reserva/<RH_id>', methods=['GET'])
def obtener_reserva(RH_id):
    try:
        # Generación de comando SQL
        sql = '''SELECT RH_id, calificacion, DATE_FORMAT(fecha_inicio, \"%%Y/%%m%%d\") as fecha_inicio,
        DATE_FORMAT(fecha_fin, \"%%Y/%%m%%d\") as fecha_fin, reseña, h.*
        FROM Reserva_Hotel r
        INNER JOIN Habitacion h ON h.habitacion_id = r.habitacion_id
        WHERE RH_id = %s'''
        val = (RH_id,)

        result = execute(sql, val)
        if(result):
            return jsonify({"success": True, "reserva": result['results'][0]}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Habitación no encontrada"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/paises', methods=['GET'])
def obtener_paises():
    # try:
    # Generación de comando SQL
    sql = 'SELECT * FROM Pais'#
    val = ()

    result = execute(sql, val)
    if(result):
        return jsonify({"success": True, "paises": result['results']}), 200, {'Content-Type': 'application/json'}
    else:
        return jsonify({"success": False, "message": "Habitación no encontrada"}), 400, {'Content-Type': 'application/json'}
    # except Exception as e:
    #     print(e)
    #     return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/ciudades/<pais_id>', methods=['GET'])
def obtener_ciudades(pais_id):
    try:
        # Generación de comando SQL
        sql = 'SELECT * FROM Ciudad WHERE pais_id = %s'#
        val = (pais_id,)

        result = execute(sql, val)
        if(result):
            return jsonify({"success": True, "ciudades": result['results']}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Habitación no encontrada"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}

@app.route('/habitacion/reservacion/usuario/<usuario_id>')
def obtener_reservaciones_usuarios(usuario_id):
    try:
        sql = 'SELECT * FROM Reserva_Hotel WHERE usuario_id = %s'
        val = (usuario_id,)
        result = execute(sql,val)

        if (result):
            return jsonify({"success": True, "reservaciones": result['results']}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Habitación no encontrada"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'}


''' @app.route('/habitacion/reserva',methods=['PUT'])
def resena_habitacion():
    try:
        # Convertir data recibida en el request a json
        payload = json.loads(request.data)

        # Verificamos que venga la petición completa
        if not ('RH_id' in payload and 'reseña' in payload and 'calificacion' in payload):
            print_message("Petición con datos incompletos!")
            return jsonify({"message": "Petición con datos incompletos!", "success": False}), 422, {'Content-Type': 'application/json'}

        # Petición recibida
        print_message(payload)

        # Verificar que exista la reserva
        reserva = obtener_reserva(payload['RH_id'])

        if not reserva:
            print_message("Reserva no encontrada para la reseña")
            return jsonify({"message": "Reserva no encontrada para la reseña", "success": False}), 200, {'Content-Type': 'application/json'}

        # Generación de comando SQL
        sql = "UPDATE Reserva_Hotel SET reseña = %s, calificacion =%s WHERE RH_id = %s"
        val = (payload['resena'], payload['calificacion'], payload['RH_id'])

        result = execute(sql, val)
        if(result):
            return jsonify({"success": True,"message": "Reseña realizada"}), 200, {'Content-Type': 'application/json'}
        else:
            return jsonify({"success": False, "message": "Reseña no se pudo realizar"}), 400, {'Content-Type': 'application/json'}
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": "Hubo un problema en el servidor, intentelo más tarde"}), 500, {'Content-Type': 'application/json'} '''


if __name__ == "__main__":
    app.run(HOST, PORT, DEBUG)
