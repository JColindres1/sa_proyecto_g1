from random import randrange
import unittest
import json
import requests

class Unit_test(unittest.TestCase):
    #--------------------------------------- PRUEBAS AUTOS ----------------------------------------
    def test_esb_mostrar_vehiculos(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los datos"
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/autos/")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    def test_esb_autos_servicio(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los datos"
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/autos/1")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    def test_esb_autos_prereserva(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los datos"
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/autos/prereserva/3")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )
    
    #--------------------------------------- PRUEBAS ESQUEMA --------------------------------------
    def test_esb_vacunas_obtener(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener las vacunas."
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/vacunas")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    def test_esb_vacunas_obtener_dosis(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener las dosis de una vacuna."
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/vacunas/dosis")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    def test_esb_esquemas_obtener(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los esquemas."
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/esquemas")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    #--------------------------------------- PRUEBAS VUELOS ---------------------------------------
    def test_esb_obtener_vuelos(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los vuelos."
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/flight")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        ) 
    
    def test_esb_obtener_reservaciones(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener las reservaciones de los vuelos"
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/reservar-vuelo/1")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        ) 
     
    def test_esb_obtener_vuelos_ciudades(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los vuelos por ciudades"
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/flight/city/3")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        ) 

    #--------------------------------------- PRUEBAS REPORTES -------------------------------------
    def test_esb_reporte1(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los datos el reporte 1."
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/reporte1")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    def test_esb_reporte2(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los datos el reporte 2."
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/reporte2")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    def test_esb_reporte3(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los datos el reporte 3."
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/reporte3")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    #--------------------------------------- PRUEBAS HABITACIONES ---------------------------------
    def test_esb_habitacion_obtener(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los datos de la habitacion."
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/habitacion/1")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    def test_esb_habitacion(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener las habitaciones registradas"
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/habitacion")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    def test_esb_habitacion_paises(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener las ciudades de un pais"
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/habitacion/ciudades/2")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    #--------------------------------------- PRUEBAS USUARIOS -------------------------------------
    def test_esb_usuarios_roles(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los tipos de usuarios."
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/users/roles")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )

    def test_esb_usuarios_paises(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener los paises registrados."
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/users/paises/")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )
    
    def test_esb_usuarios_ciudades(self):
        codigo_esperado = 200
        mensaje_error = "Ocurrio un error al obtener todas las ciudades."
        respuesta = requests.get(url="http://104.154.167.229:4000/esb/users/ciudades")
        self.assertEqual(
            respuesta.status_code,
            codigo_esperado,
            mensaje_error
        )


if __name__ == '__main__':
    unittest.main()