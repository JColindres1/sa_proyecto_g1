import json
import mysql.connector

from flask import Flask, jsonify, request
from flask_cors import CORS  # pip install -U flask-cors

app = Flask(__name__)
CORS(app,origins=["*"])

HOST='localhost'
PORT='3500'
DEBUG=True

def connection():
   return mysql.connector.connect(
            host = "35.225.114.145",
            user = "root",
            password = "db_g1",
            database = "Vuelos",
            port = "3309"
    )

@app.route('/vacunas/registrar',methods=['POST'])
def Registrar_vacunas():
    try:
        solicitud = json.loads(request.data)
        nombre = solicitud["nombre_vacuna"]
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("INSERT INTO Vacuna (nombre_vacuna) VALUES (%s)", (nombre,))
        db.commit()
        db.close()

        return {"mensaje":"Vacuna registrada exitosamente"},200
    except Exception as e:
        print(e)
        return {"mensaje":"Error al registrar la vacuna"},400


@app.route('/vacunas',methods=['GET'])
def Obtener_vacunas():
    try:
        lista = []
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("SELECT * FROM Vacuna")
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'id_vacuna':row[0],'nombre_vacuna':row[1]})
        db.close()
        return json.dumps(lista),200
    except:
        return {"mensaje":"Error al obtener vacunas"},400

@app.route('/vacunas/dosis',methods=['POST'])
def Registrar_dosis():
    try:
        solicitud = json.loads(request.data)
        idvacuna = solicitud["id_vacuna"]
        tipo = solicitud["tipo"]
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("INSERT INTO Dosis (idvacuna,tipo) VALUES (%s,%s)", (idvacuna,tipo))
        db.commit()
        db.close()
        return {"mensaje":"Dosis registrada"},200
    except:
        return {"mensaje":"Error al registrar la dosis"},400

@app.route('/vacunas/dosis',methods=['GET'])
def Obtener_dosis():
    try:
        lista = []
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("SELECT D.iddosis, D.tipo, V.nombre_vacuna FROM Dosis D, Vacuna V WHERE D.idvacuna = V.idvacuna")
            resultado = cursor.fetchall()
        db.close()
        for row in resultado:
            lista.append({'id_dosis':row[0],'nombre_vacuna':row[2],'tipo': row[1]})

        return json.dumps(lista),200
    except:
        return {"mensaje":"Error al obtener vacunas"},400

@app.route('/esquema',methods=['POST'])
def Crear_esquema():
    try:
        solicitud = json.loads(request.data)
        nombre = solicitud["nombre"]
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("INSERT INTO Esquema (nombre_esquema) VALUES (%s)", (nombre,))
        db.commit()
        db.close()
        return {"mensaje":"Esquema registrado"},200
    except:
        return {"mensaje":"Error al registrar el esquema"},400
    

@app.route('/esquemas',methods=['GET'])
def Obtener_esquema():
    try:
        lista = []
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("SELECT * FROM Esquema")
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'id_esquema':row[0],'nombre_esquema':row[1]})
        db.close()
        return json.dumps(lista),200
    except:
        return {"mensaje":"Error al obtener esquemas"},400

@app.route('/esquema/dosis',methods=['POST'])
def Asignar_esquema_dosis():
    try:
        solicitud = json.loads(request.data)
        idesquema = solicitud["id_esquema"]
        iddosis = solicitud["id_dosis"]
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("INSERT INTO VacunaEsquema (idesquema,iddosis) VALUES (%s,%s)", (idesquema,iddosis))
        db.commit()
        db.close()
        return {"mensaje":"Esquema registrado"},200
    except:
        return {"mensaje":"Error al asignar información al esquema"},400


@app.route('/esquema/dosis/<id>',methods=['GET'])
def Obtener_esquema_dosis(id):
    try:
        lista = []
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("SELECT VE.iddosis, V.nombre_vacuna, D.tipo FROM VacunaEsquema VE, Vacuna V, Dosis D WHERE  VE.iddosis = %s and VE.iddosis = D.iddosis and D.idvacuna = V.idvacuna",(id,))
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'id_dosis':row[0],'nombre_vacuna':row[1], 'tipo':row[2]})
        db.close()
        return json.dumps(lista),200
    except:
        return {"mensaje":"Error al obtener información"},400


@app.route('/esquema/pais',methods=['POST'])
def Anexar_esquema():
    try:
        solicitud = json.loads(request.data)
        idesquema = solicitud["id_esquema"]
        idpais = solicitud["id_pais"]

        db = connection()
        with db.cursor() as cursor:
            cursor.execute("INSERT INTO EsquemaPais (idesquema,idpais) VALUES (%s,%s)", (idesquema,idpais))
        db.commit()
        db.close()
        return {"mensaje":"Esquema asignado exitosamente."},200
    except:
        return {"mensaje":"Error al asignar información al esquema."},400

@app.route('/esquema/pais/<id>',methods=['GET'])
def Obtener_esquema_pais(id):
    try:
        lista = []
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("SELECT EP.idpais, P.nombre, EP.idesquema, E.nombre_esquema FROM EsquemaPais EP, Pais P, Esquema E WHERE EP.idpais = %s and EP.idpais = P.pais_id and EP.idesquema = E.idesquema;",(id,))
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'id_pais':row[0],'nombre_pais':row[1], 'id_esquema':row[2],'nombre_esquema': row[3]})
        db.close()
        return json.dumps(lista),200
    except:
        return {"mensaje":"Error al obtener información"},400

@app.route('/pasaporte/usuario/dosis',methods=['POST'])
def AgregarDosis():
    try:
        solicitud = json.loads(request.data)
        idusuario = solicitud["id_usuario"]
        iddosis = solicitud["id_dosis"]

        db = connection()
        with db.cursor() as cursor:
            cursor.execute("INSERT INTO UsuarioDosis (usuario_id,dosis_id) VALUES (%s,%s)", (idusuario,iddosis))
        db.commit()
        db.close()
        return {"mensaje":"Dosis añadida correctamente a tu pasaporte."},200
    except:
        return {"mensaje":"Error al asignar información al pasaporte."},400

@app.route('/pasaporte/usuario/<id>',methods=['GET'])
def Obtener_Pasaporte(id):
    try:
        lista = []
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("SELECT D.iddosis, V.nombre_vacuna, D.tipo FROM Vacuna V, Dosis D, UsuarioDosis UD WHERE D.idvacuna = V.idvacuna AND UD.dosis_id = D.iddosis AND UD.usuario_id = %s",(id,))
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append({'id_dosis':row[0],'nombre_vacuna':row[1], 'tipo':row[2]})
        db.close()
        return json.dumps(lista)
    except:
        return {"mensaje":"Error al obtener información"},400

@app.route('/esquema/pais/usuario/<idpais>/<idusuario>',methods=['GET'])
def Validar_esquema(idpais,idusuario):
    try:
        lista = []
        db = connection()
        with db.cursor() as cursor:
            cursor.execute("SELECT DISTINCT * FROM UsuarioDosis UD WHERE UD.usuario_id = %s",(idusuario,))
            resultado = cursor.fetchall()
            for row in resultado:
                lista.append(row[1])
            cursor.execute("SELECT * FROM EsquemaPais EP WHERE EP.idpais = %s",(idpais,))
            resultado = cursor.fetchall()
            for row in resultado:
                cursor.execute("SELECT EP.idesquema, COUNT(D.iddosis) FROM Vuelos.EsquemaPais EP, Vuelos.Esquema E, Vuelos.VacunaEsquema VE, Vuelos.Dosis D WHERE  EP.idesquema = E.idesquema AND VE.idesquema = E.idesquema AND VE.iddosis = D.iddosis AND EP.idpais = %s AND EP.idesquema = %s GROUP BY EP.idesquema",(idpais,row[0]))
                resultado0 = cursor.fetchone()
                cursor.execute("SELECT D.iddosis FROM EsquemaPais EP, Esquema E, VacunaEsquema VE, Dosis D WHERE  EP.idesquema = E.idesquema AND VE.idesquema = E.idesquema AND VE.iddosis = D.iddosis AND EP.idpais = %s AND EP.idesquema = %s",(idpais,row[0]))
                resultado = cursor.fetchall()
                contador = 0
                for row in resultado:
                    element_exist = row[0] in lista
                    if element_exist:
                        contador = contador + 1
                    if contador == resultado0[1]:
                        return {"mensaje":"Validado"},200
        db.close()
        return {"mensaje":"Esquema no validado"},400
    except:
        return {"mensaje":"Error al obtener información"},400

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port="3500")
