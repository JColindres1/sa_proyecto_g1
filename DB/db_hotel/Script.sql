CREATE DATABASE `Room`;

USE `Room` ;

-- -----------------------------------------------------
-- Table `Room`.`Pais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Room`.`Pais` (
  `pais_id` INT NOT NULL AUTO_INCREMENT,
  `nombre_pais` VARCHAR(50) NOT NULL,
  
PRIMARY KEY (`pais_id`)
);

-- -----------------------------------------------------
-- Table `Room`.`Ciudad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Room`.`Ciudad` (
  `ciudad_id` INT NOT NULL AUTO_INCREMENT,
  `nombre_ciudad` VARCHAR(50) NOT NULL,
  `pais_id` INT NOT NULL,

PRIMARY KEY (`ciudad_id`),
FOREIGN KEY (`pais_id`) REFERENCES `Room`.`Pais` (`pais_id`)
);

-- -----------------------------------------------------
-- Table `Room`.`Habitacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Room`.`Habitacion` (
  `habitacion_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(150) NOT NULL,
  `precio` FLOAT NOT NULL,
  `cantidad_personas` INT NOT NULL,
  `descripcion` TEXT NOT NULL,
  `estado` INT NOT NULL,
  `usuario_id` INT NOT NULL,
  `ciudad_id` INT NOT NULL,
  
PRIMARY KEY (`habitacion_id`),
FOREIGN KEY (`ciudad_id`) REFERENCES `Room`.`Ciudad` (`ciudad_id`)
    );

-- -----------------------------------------------------
-- Table `Room`.`Calendario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Room`.`Calendario` (
  `id_calendario` INT NOT NULL AUTO_INCREMENT,
  `fecha_i` DATE NOT NULL,
  `estado` INT NOT NULL,
  `habitacion_id` INT NOT NULL,
  
PRIMARY KEY (`id_calendario`),
FOREIGN KEY (`habitacion_id`) REFERENCES `Room`.`Habitacion` (`habitacion_id`)
);

-- -----------------------------------------------------
-- Table `Room`.`Reserva_Hotel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Room`.`Reserva_Hotel` (
  `RH_id` INT NOT NULL AUTO_INCREMENT,
  `reseña` TEXT NULL DEFAULT NULL,
  `calificacion` INT NULL DEFAULT NULL,
  `usuario_id` INT NOT NULL,
  `habitacion_id` INT NOT NULL,
  `fecha_inicio` DATE NOT NULL,
  `fecha_fin` DATE NOT NULL,

PRIMARY KEY (`RH_id`),
FOREIGN KEY (`habitacion_id`) REFERENCES `Room`.`Habitacion` (`habitacion_id`)
);