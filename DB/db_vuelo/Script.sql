CREATE DATABASE `Vuelo`;

USE `Vuelo`;

-- -----------------------------------------------------
-- Table `Vuelo`.`Pais_vuelo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Vuelo`.`Pais_vuelo` (
  `pais_id` INT NOT NULL,
  `nombre` VARCHAR(50) NOT NULL,
  
PRIMARY KEY (`pais_id`)
);


-- -----------------------------------------------------
-- Table `Vuelo`.`Ciudad_vuelo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Vuelo`.`Ciudad_vuelo` (
  `ciudad_id` INT NOT NULL,
  `nombre` VARCHAR(50) NOT NULL,
  `pais_id` INT NOT NULL,
  
PRIMARY KEY (`ciudad_id`),
FOREIGN KEY (`pais_id`) REFERENCES `Vuelo`.`Pais_vuelo` (`pais_id`)
);


-- -----------------------------------------------------
-- Table `Vuelo`.`Esquema_vacunacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Vuelo`.`Esquema_vacunacion` (
  `esquema_id` INT NOT NULL AUTO_INCREMENT,

PRIMARY KEY (`esquema_id`)
);


-- -----------------------------------------------------
-- Table `Vuelo`.`Dosis_vacuna`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Vuelo`.`Dosis_vacuna` (
  `dosis_id` INT NOT NULL AUTO_INCREMENT,
  `vacuna` VARCHAR(15) NULL DEFAULT NULL,
  `esquema_id` INT NOT NULL,
  
PRIMARY KEY (`dosis_id`),
FOREIGN KEY (`esquema_id`) REFERENCES `Vuelo`.`Esquema_vacunacion` (`esquema_id`)
);


-- -----------------------------------------------------
-- Table `Vuelo`.`Esquema_pais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Vuelo`.`Esquema_pais` (
  `pais_id` INT NOT NULL,
  `esquema_id` INT NOT NULL,

FOREIGN KEY (`pais_id`) REFERENCES `Vuelo`.`Pais_vuelo` (`pais_id`),
FOREIGN KEY (`esquema_id`) REFERENCES `Vuelo`.`Esquema_vacunacion` (`esquema_id`)
);


-- -----------------------------------------------------
-- Table `Vuelo`.`Vuelo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Vuelo`.`Vuelo` (
  `viaje_id` INT NOT NULL AUTO_INCREMENT,
  `nombre_viaje` VARCHAR(150) NOT NULL,
  `precio` DECIMAL(10,0) NOT NULL,
  `cantidad_personas` INT NOT NULL,
  `pasajeros` INT NOT NULL,
  `tipo` VARCHAR(50) NOT NULL,
  `ciudad_origen` INT NOT NULL,
  `ciudad_destino` INT NOT NULL,
  `estado` INT NOT NULL,

PRIMARY KEY (`viaje_id`),
FOREIGN KEY (`ciudad_origen`) REFERENCES `Vuelo`.`Ciudad_vuelo` (`ciudad_id`),
FOREIGN KEY (`ciudad_destino`) REFERENCES `Vuelo`.`Ciudad_vuelo` (`ciudad_id`)
);