CREATE DATABASE `Automovil`;

USE `Automovil`;

-- -----------------------------------------------------
-- Table `Automovil`.`Automovil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Automovil`.`Automovil` (
  `auto_id` INT NOT NULL AUTO_INCREMENT,
  `cantidad_personas` INT NOT NULL,
  `precio` FLOAT NOT NULL,
  `color` VARCHAR(20) NOT NULL,
  `marca` VARCHAR(20) NOT NULL,
  `modelo` VARCHAR(15) NOT NULL,
  `linea` VARCHAR(50) NOT NULL,
  `imagen` VARCHAR(500) NOT NULL,
  `usuario_id` INT NOT NULL,

PRIMARY KEY (`auto_id`)
);

-- -----------------------------------------------------
-- Table `Automovil`.`Reserva_Auto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Automovil`.`Reserva_Auto` (
  `RA_id` INT NOT NULL AUTO_INCREMENT,
  `fecha_i` DATE NOT NULL,
  `fecha_f` DATE NOT NULL,
  `resena` TEXT NULL DEFAULT NULL,
  `calificacion` INT NULL DEFAULT NULL,
  `estado` INT NOT NULL,
  `precio` FLOAT NOT NULL,
  `usuario_id` INT NOT NULL,
  `auto_id` INT NOT NULL,

PRIMARY KEY (`RA_id`),
FOREIGN KEY (`auto_id`) REFERENCES `Automovil`.`Automovil` (`auto_id`)
);