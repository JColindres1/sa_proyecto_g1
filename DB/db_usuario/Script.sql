CREATE DATABASE `Usuario`;

USE `Usuario`;

-- -----------------------------------------------------
-- Table `Usuario`.`Pais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Usuario`.`Pais` (
  `pais_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  
PRIMARY KEY (`pais_id`)
);

-- -----------------------------------------------------
-- Table `Usuario`.`Ciudad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Usuario`.`Ciudad` (
  `ciudad_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  `pais_id` INT NOT NULL,

PRIMARY KEY (`ciudad_id`),
FOREIGN KEY (`pais_id`) REFERENCES `Usuario`.`Pais` (`pais_id`)
);

-- -----------------------------------------------------
-- Table `Usuario`.`Tipo_Vacuna`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Usuario`.`Tipo_Vacuna` (
  `tp_id` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(50) NOT NULL,
  
PRIMARY KEY (`tp_id`)
);

-- -----------------------------------------------------
-- Table `Usuario`.`Rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Usuario`.`Rol` (
  `rol_id` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NOT NULL,
  
PRIMARY KEY (`rol_id`)
);

-- -----------------------------------------------------
-- Table `Usuario`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Usuario`.`Usuario` (
  `usuario_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  `correo` VARCHAR(150) NOT NULL,
  `contrasena` VARCHAR(50) NOT NULL,
  `fecha` DATE NULL,
  `imagen` VARCHAR(150) NULL,
  `rol_id` INT NOT NULL,
  `ciudad_id` INT NOT NULL,

PRIMARY KEY (`usuario_id`),
FOREIGN KEY (`rol_id`) REFERENCES `Usuario`.`Rol` (`rol_id`),
FOREIGN KEY (`ciudad_id`) REFERENCES `Usuario`.`Ciudad` (`ciudad_id`)
);

-- -----------------------------------------------------
-- Table `Usuario`.`EsquemaVacunacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Usuario`.`EsquemaVacunacion` (
  `EV_id` INT NOT NULL AUTO_INCREMENT,
  `usuario_id` INT NOT NULL,
  `certificado` BINARY(1) NULL DEFAULT NULL,
  
PRIMARY KEY (`EV_id`),
FOREIGN KEY (`usuario_id`) REFERENCES `Usuario`.`Usuario` (`usuario_id`)
);

-- -----------------------------------------------------
-- Table `Usuario`.`Dosis`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Usuario`.`Dosis` (
  `dosis_id` INT NOT NULL AUTO_INCREMENT,
  `descripcion` TEXT NOT NULL,
  `fecha` DATE NOT NULL,
  `EV_id` INT NOT NULL,
  `tp_id` INT NOT NULL,

PRIMARY KEY (`dosis_id`),
FOREIGN KEY (`tp_id`) REFERENCES `Usuario`.`Tipo_Vacuna` (`tp_id`),
FOREIGN KEY (`EV_id`) REFERENCES `Usuario`.`EsquemaVacunacion` (`EV_id`)
);

-- -----------------------------------------------------
-- Table `Usuario`.`Tarjeta_Credito`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Usuario`.`Tarjeta_Credito` (
  `tc_id` INT NOT NULL AUTO_INCREMENT,
  `ccv` INT NOT NULL,
  `fecha_v` DATE NOT NULL,
  `saldo` FLOAT NOT NULL,
  `usuario_id` INT NOT NULL,

PRIMARY KEY (`tc_id`),
FOREIGN KEY (`usuario_id`) REFERENCES `Usuario`.`Usuario` (`usuario_id`)
);