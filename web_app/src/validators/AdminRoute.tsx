import React from 'react';
import { Redirect, Route } from 'react-router-dom';

export default function AdminRoute({
	children,
	component: Component,
	...rest
}) {
	let userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo'))
		: null;
	return (
		<Route
			{...rest}
			render={(props) =>
				userInfo && userInfo.rol_id === 2 ? (
					<Component {...props} />
				) : userInfo && userInfo.rol_id !== 2 ? (
					<Redirect
						to={{
							pathname: '/home ',
							state: { from: props.location },
						}}
					/>
				) : (
					<Redirect
						to={{
							pathname: '/login',
							state: { from: props.location },
						}}
					/>
				)
			}
		/>
	);
}
