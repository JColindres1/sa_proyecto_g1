import React from 'react';
import { Redirect, Route } from 'react-router-dom';

export default function TuristaRoute({
	children,
	component: Component,
	...rest
}) {
	let userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo'))
		: null;
	return (
		<Route
			{...rest}
			render={(props) =>
				userInfo && [2,1].includes(userInfo.rol_id) ? (
					<Component {...props} />
				) : userInfo && ![2,1].includes(userInfo.rol_id) ? (
					<Redirect
						to={{
							pathname: '/rooms',
							state: { from: props.location },
						}}
					/>
				) : (
					<Redirect
						to={{
							pathname: '/login',
							state: { from: props.location },
						}}
					/>
				)
			}
		/>
	);
}
