import { Form, Col, Button, Container, Card } from 'react-bootstrap';
import { useIonAlert } from '@ionic/react';
import NavBar from '../../components/NavBar/NavBar';
import React, { useEffect ,useState} from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { API_USERS } from '../../helpers/endpoint';
import './Login.css';
import { Camera, CameraResultType } from '@capacitor/camera';
import { IonContent, IonDatetime, IonItem, IonInput, IonButton, IonModal, IonIcon, IonPopover, IonLabel, IonText } from '@ionic/react';
import { defineCustomElements } from '@ionic/pwa-elements/loader';
import { FcCompactCamera } from "react-icons/fc";
const Login: React.FC = () => {
	defineCustomElements(window);
	const [present] = useIonAlert();
	const history = useHistory();
    const [imgB64,setImgB64] = useState('');
	useEffect(() => {
		const obj = JSON.parse(localStorage.getItem('userInfo'));
		if (obj?.rol_id === 1) {
			history.push('/turista/home');
		} else if (obj?.rol_id === 2) {
			history.push('/admin/home');
		} else if (obj?.rol_id === 3) {
			history.push('/rooms');
		} else if (obj?.rol_id === 4) {
			history.push('/cars/home');
		} else if (obj?.rol_id === 5) {
			history.push('/flight_register');
		}
	}, [history]);

    let handleSubmit = async (event) => {
        event.preventDefault();
	 let response = await axios
		.post(`${API_USERS}/login`, {
			correo: event.target.elements.formBasicEmail.value,
			contrasena: event.target.elements.formBasicPassword.value
		})
		
		if(response.status == 200){
		    console.log(response.data.JWT)
			localStorage.setItem('bearer', response.data.JWT);
		}

		let userResponse = await axios
		.get(`${API_USERS}/login`)
        
	
		if(userResponse.status == 200 && userResponse.data.rol_id > 0){
			localStorage.setItem('userInfo', JSON.stringify(userResponse.data));
			console.log(userResponse.data)

			let data = {
				correo:userResponse.data.correo,
				imagen:imgB64
			}

			console.log(data);
             if(userResponse.data.rol_id !== 2 && userResponse.data.imagen !== "No disponible"){
			  let campare = await axios.post(`${API_USERS}/comparar`,data)
			  console.log(campare);
			 }
			//console.log(campare);
			present({
				cssClass: 'my-css',
				header: '¡Éxito!',
				message: 'Usuario logeado con éxito',
				buttons: [
					{ text: 'Ok', handler: (d) => console.log('ok pressed') },
				],
			}).then(()=>window.location.reload())
			if (userResponse.data.rol_id === 1) {
				history.push('/turista/home');
			}
			else if (userResponse.data.rol_id === 2) {
				history.push('/admin/home');
			}else if(userResponse.data.rol_id === 4){
				history.push('/cars/home');
			}
		} else {
			present({
				cssClass: 'my-css',
				header: '¡Alerta!',
				message: 'Credenciales erróneas',
				buttons: [
					{ text: 'Ok', handler: (d) => window.location.reload() },
				],
			})
		}


		

		return;


        axios
            .post(`${API_USERS}/login`, {
                Correo: event.target.elements.formBasicEmail.value,
                Contrasena: event.target.elements.formBasicPassword.value
            })
            .then((response) => {
                console.log(response);
                const obj = JSON.parse(response.data.message)
                if (obj.Usuario_id > 0) {
                    localStorage.setItem('userInfo', JSON.stringify(obj));
                    localStorage.setItem('bearer', response.data.bearer);
                    present({
                        cssClass: 'my-css',
                        header: '¡Éxito!',
                        message: 'Usuario logeado con éxito',
                        buttons: [
                            { text: 'Ok', handler: (d) => console.log('ok pressed') },
                        ],
                    }).then(()=>window.location.reload())
                    if (obj.Rol_id === 1) {
                        history.push('/turista/home');
                    }
                    else if (obj.Rol_id === 2) {
                        history.push('/admin/home');
                    }else if(obj.Rol_id === 4){
                        history.push('/cars/home');
                    }
                }
                else {
                    present({
                        cssClass: 'my-css',
                        header: '¡Alerta!',
                        message: 'Credenciales erróneas',
                        buttons: [
                            { text: 'Ok', handler: (d) => window.location.reload() },
                        ],
                    })
                }
            });
    }


    const takePicture = async () => {
        const image = await Camera.getPhoto({
            quality: 90,
            allowEditing: false,
            resultType: CameraResultType.DataUrl
            });
            var imageUrl = image;
             console.log(imageUrl);
            // Can be set to the src of an image now
            console.log(imageUrl.dataUrl);
            setImgB64(imageUrl.dataUrl);
      };

				


	return (
		<>
			<NavBar />
			<Container>
				<h1 className='title-custom'>Login</h1>
				<Col sm={{ span: 6, offset: 3 }}>
					<Card className='login-card'>
						<div className='margin-login'>
							<Form onSubmit={handleSubmit}>
								<Form.Group
									className='mb-3'
									controlId='formBasicEmail'
									style={{ textAlign: 'start' }}>
									<Form.Label className='form-text'>
										Usuario:
									</Form.Label>
									<Form.Control
										type='email'
										placeholder='Enter email'
									/>
								</Form.Group>

								<Form.Group
									className='mb-3'
									controlId='formBasicPassword'
									style={{ textAlign: 'start' }}>
									<Form.Label className='form-text'>
										Password:
									</Form.Label>
									<Form.Control
										type='password'
										placeholder='Password'
									/>
								</Form.Group>
								<IonButton color="primary" onClick={() => takePicture()}><FcCompactCamera size={30}/></IonButton>
								<Button variant='primary' type='submit'>
									Login
								</Button>
								<Form.Group>
									<Form.Label className='form-sub-text'>
										No posees una cuenta? registrate{' '}
										<a href='/register'>aquí</a>
									</Form.Label>
								</Form.Group>
							</Form>
						</div>
					</Card>
				</Col>
			</Container>
		</>
	);
};

export default Login;
