import NavBar from '../../components/NavBar/NavBar';
import React, { useEffect,useState } from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { useHistory } from 'react-router-dom';
import { IonContent } from '@ionic/react';
import FormRegister from '../../components/FormRegister/FormRegister';
import WebCam from "react-webcam";
import { useIonAlert } from '@ionic/react';

const Register: React.FC = () => {
    const history = useHistory();
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(null);
    const onChange = (dates) => {
        const [start, end] = dates;
        setStartDate(start);
        setEndDate(end);
    };
    const [selectedValue1, setSelectedValue1] = useState();
    const [selectedValue2, setSelectedValue2] = useState();
    const [selectedValue3, setSelectedValue3] = useState();
    const [popoverDate2, setPopoverDate2] = useState('');
    const [popoverDate, setPopoverDate] = useState('');
    const [present] = useIonAlert();
    const videoConstraints = { width: 400, height: 400, facingMode: "user" };
    useEffect(() => {
        const obj = JSON.parse(localStorage.getItem("userInfo"))
        if (obj?.Rol_id === 1) {
            history.push('/turista/home')
        }
        else if (obj?.Rol_id === 2) {
            history.push('/admin/home')
        }
        else if (obj?.Rol_id === 3) {
            history.push('/rooms')
        }
        else if (obj?.Rol_id === 4) {
            history.push('/cars/home')
        }
        else if (obj?.Rol_id === 5) {
            history.push('/flight_register')
        }
    }, []);

	return (
		<>
			<NavBar />
			<IonContent>
				<FormRegister formType={1} />
			</IonContent>
		</>
	);
};

export default Register;
