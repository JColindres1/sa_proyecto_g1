import ExploreContainer from '../../components/Explore/ExploreContainer';
import NavBar from '../../components/NavBar/NavBar';
import logo from '../../assets/10966.jpg';

import './Home.css';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router';

const Home: React.FC = () => {
	const history = useHistory();
	let userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo'))
		: null;

	useEffect(() => {
		if (userInfo) {
			switch (userInfo.rol_id) {
				case 1:
					history.push('/turista/home');
					break;
				case 2:
					history.push('/admin/home');
					break;
				case 3:
					history.push('/rooms');
					break;
				case 4:
					history.push('/cars/home');
					break;
				case 5:
					history.push('/flight_register');
					break;
				default:
					break;
			}
		}
	}, [userInfo, history]);

	return (
		<>
			<NavBar />
			<ExploreContainer title='Travel' imgIcon={logo} name='Log In' />
		</>
	);
};

export default Home;
