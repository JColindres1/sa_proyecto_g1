
import NavBar from '../../components/NavBar/NavBar';
import React from 'react';
import { IonContent } from '@ionic/react';
import "react-datepicker/dist/react-datepicker.css";
import FormRegister from '../../components/FormRegister/FormRegister';
const HomeA: React.FC = () => {
    return (
        <>
            <NavBar />
            <IonContent>
            <FormRegister formType={2}/>
            </IonContent>
        </>
    );
};

export default HomeA;