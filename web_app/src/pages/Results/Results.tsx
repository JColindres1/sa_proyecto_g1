import { IonContent } from '@ionic/react';
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import DoughnutChart from '../../components/Graficas/DoughnutChart';
import HorizontalBarChart from '../../components/Graficas/HorizontalBarChart';
import PieChart from '../../components/Graficas/PieChart';
import VerticalBarChart from '../../components/Graficas/VerticalBarChart';
import NavBar from '../../components/NavBar/NavBar';

export default function Results(props: any) {
	return (
		<>
			<NavBar />
			<IonContent>
				<Row className='w-100 my-5 justify-content-center'>
					<h1 className='w-auto p-0 m-0'>Resultados</h1>
				</Row>
				<Row className='w-100 my-5 justify-content-center px-2 px-md-5 mx-0'>
					<Col
						xs={12}
						md={6}
						lg={4}
						className='d-flex align-items-center justify-content-center mb-5'>
						<div className='d-flex flex-column justify-content-center align-items-center w-100 w-md-75 h-auto'>
							<h2 className='p-0 m-0 mb-2 text-center'>
								Top 3 Rentadoras Más Utilizadas
							</h2>
							<PieChart reporte={1} />
						</div>
					</Col>
					<Col
						xs={12}
						md={6}
						lg={4}
						className='d-flex align-items-center justify-content-center mb-5'>
						<div className='d-flex flex-column justify-content-center align-items-center w-100 w-md-75 h-auto'>
							<h2 className='p-0 m-0 mb-2 text-center'>
								Top 3 Aerolineas Más Utilizadas
							</h2>
							<DoughnutChart reporte={2} />
						</div>
					</Col>
					<Col
						xs={12}
						md={6}
						lg={4}
						className='d-flex align-items-center justify-content-center mb-5'>
						<div className='d-flex flex-column justify-content-center align-items-center w-100 w-md-75 h-auto'>
							<h2 className='p-0 m-0 mb-2 text-center'>
								Top 3 Ciudades Más Visitadas
							</h2>
							<PieChart reporte={3} />
						</div>
					</Col>
					<Col
						xs={12}
						md={6}
						lg={4}
						className='d-flex align-items-center justify-content-center mb-5'>
						<div className='d-flex flex-column justify-content-center align-items-center w-100 w-md-75 h-auto'>
							<h2 className='p-0 m-0 mb-2 text-center'>
								Top 5 Paises Con Más Usuarios
							</h2>
							<HorizontalBarChart reporte={4} />
						</div>
					</Col>
					<Col
						xs={12}
						md={6}
						lg={4}
						className='d-flex align-items-center justify-content-center mb-5'>
						<div className='d-flex flex-column justify-content-center align-items-center w-100 w-md-75 h-auto'>
							<h2 className='p-0 m-0 mb-2 text-center'>
								Top 3 Hoteles Más Reservados
							</h2>
							<VerticalBarChart reporte={5} />
						</div>
					</Col>
				</Row>
			</IonContent>
		</>
	);
}
