import NavBar from '../../../components/NavBar/NavBar'
import React, { useEffect }  from 'react';
import { Container, Form,Row,Col, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { useIonAlert } from '@ionic/react';

const Menu_register: React.FC = () => {

    const [present] = useIonAlert();
    const history = useHistory();

    const cerrarsesion = () =>{
        localStorage.clear();
        history.push('/home');
    }

    useEffect(() => {
        const obj = JSON.parse(localStorage.getItem("userInfo"))
        if (localStorage.getItem("userInfo") === null) {
            history.push('/home')
        }
        else if (obj.rol_id !==4) {
            present({
                cssClass: 'my-css',
                header: '¡Oops!',
                message: 'No tienes permisos para esta vista',
                buttons: [
                    { text: 'Ok', handler: (d) => console.log('ok pressed') },
                ],
            })
            if (obj.rol_id === 2) {
                history.push('/admin/home')
            }
            else if (obj.rol_id === 3) {
                history.push('/rooms')
            }
            else if (obj.rol_id === 1) {
                history.push('/turista/home')
            }
            else if (obj.rol_id === 5) {
                history.push('/flight_register')
            }
        }
    }, []);

    return(
        <>
            <NavBar/>
            <Container>
                <Form>
                    <Row className='mb-5'>
                        <h1> Seleccionar tipo de entrada de registro</h1>
                    </Row>
                    <Row className='mb-5'>
                        <Col sm={5}>
                            <Button variant="success" onClick={() => history.push('/cars/register')} >
                                Entrada Manual
                            </Button>
                        </Col>
                        <Col sm={2}>
                            <Button variant="success" onClick={() => history.push('/cars/register_file')}>
                                Carga masiva
                            </Button>
                        </Col>
                        <Col sm={4}>
                            <Button variant="success" onClick={() => history.push('/cars/all_cars')}>
                                Ver automoviles
                            </Button>
                        </Col>
                    </Row>
                    <Button variant="secondary" onClick={cerrarsesion}>
                                Cerrar sesion
                    </Button>
                </Form>
            </Container>
        </>
    );
};

export default Menu_register; 