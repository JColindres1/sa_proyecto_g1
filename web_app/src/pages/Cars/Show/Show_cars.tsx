import NavBar from '../../../components/NavBar/NavBar'
import React ,{useEffect, useState}  from 'react';
import { Container, Form,Row, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import {useIonAlert,IonCard,IonCardHeader,IonCardContent,IonCardSubtitle,IonCardTitle,IonContent} from '@ionic/react';
import axios from 'axios';
import { API_AUTOS } from '../../../helpers/endpoint';
import CardContent from '../../../components/CardContent/CardContent'
const Show_cars: React.FC = () => {

    const [autos,setAutos] = useState([]);
    const [present] = useIonAlert();
    
    const history = useHistory();

    const Obtener_vehiculos = () =>{
        const objeto = JSON.parse(localStorage.getItem("userInfo"));

        axios({
           method: "GET",
           url: `${API_AUTOS}`+objeto.id_usuario
        }).then((response)=>{
            //console.log(response.data.data);
            console.log(response);
            console.log(response.data)
            let data_response  =  JSON.parse(response.data.replaceAll('\'','"'));
            console.log(data_response.datos);
            let textList = []
            for(let car of data_response.datos){
                let text = {
                  title : `${car.marca} ${car.modelo}`,
                  subtitle: `${car.linea}  IDE ${car.placa}`,
                  description: ` Precio: ${car.precio_dia}  Ciudad :${car.idciudad}`,
                  img: car.imagen,
                  obj: car
                }
                textList.push(text);  
            }
            console.log(textList);
            setAutos(textList);
        });
    }

    useEffect(() =>{
        

        const obj = JSON.parse(localStorage.getItem("userInfo"))
        if (localStorage.getItem("userInfo") === null) {
            history.push('/home')
        }
        else if (obj.rol_id !==4) {
            present({
                cssClass: 'my-css',
                header: '¡Oops!',
                message: 'No tienes permisos para esta vista',
                buttons: [
                    { text: 'Ok', handler: (d) => console.log('ok pressed') },
                ],
            })
            if (obj.rol_id === 2) {
                history.push('/admin/home')
            }
            else if (obj.rol_id === 3) {
                history.push('/rooms')
            }
            else if (obj.rol_id === 1) {
                history.push('/turista/home')
            }
            else if (obj.rol_id === 5) {
                history.push('/flight_register')
            }
        }

        Obtener_vehiculos();
    },[])

    const get_reservation = (obj) => {
        console.log("object: ")

    }

    return(
        
        <>
           <NavBar />
            <IonContent className='background'>
                <Container className='d-flex w-100 h-100 justify-content-start align-items-center flex-column '
                    style={{ paddingTop: '56px' }}>
                    <CardContent content={autos} get_reservation={get_reservation} title="Automóviles Disponibles"/>

                </Container>
            </IonContent>
        </>
    );
};

export default Show_cars; 