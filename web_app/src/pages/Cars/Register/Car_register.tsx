import React from 'react';
import NavBar from '../../../components/NavBar/NavBar';
import { Card, Container, Row } from 'react-bootstrap';
import CarForm from '../../../components/CarForm/CarForm';

export default function RegisterCar() {

	let userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo'))
		: null;
	return (
		<>
			<NavBar />
			<Container
				className='d-flex w-100 h-100 overflow-auto justify-content-start align-items-start flex-column overflow-auto'
				style={{ marginTop: '56px' }}>
				<Row className='d-flex w-100 justify-content-center my-4'>
					<h3>Registro de Autos</h3>
				</Row>
				<Row className='d-flex w-100 justify-content-center mb-4'>
				<Card className="login-card p-4">
					<CarForm action='create' user={userInfo} />
					</Card>
				</Row>
			</Container>
		</>
	);
}
