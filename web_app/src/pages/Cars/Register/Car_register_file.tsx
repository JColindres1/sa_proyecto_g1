import NavBar from '../../../components/NavBar/NavBar';
import React, { useState } from 'react';
import { Container, Form, Row, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { API_AUTOS } from '../../../helpers/endpoint';

const Car_register_file: React.FC = () => {
	const [cadena, setCadena] = useState('');
	const [arreglo, setArreglo] = useState([]);

	const Cargar_datos = () => {
		const objeto = JSON.parse(localStorage.getItem('userInfo'));

		let fileUpload: any = document.getElementById('fileupload');
		var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
		if (regex.test(fileUpload.value.toLowerCase())) {
			if (typeof FileReader !== 'undefined') {
				var reader = new FileReader();
				reader.onload = function (e: any) {
					//console.log(e.target.result);
					var rows = e.target.result?.split('\r\n');
					let lista_autos = [];
					var informacion = '';
					//console.log(rows);
					let cont = 0;
					for (let value of rows) {
						if (cont !== 0) {
							let data = value.split(',');
							if (value !== '') {
								let objeto_auto = {
									cantidad_personas: data[0],
									precio: parseFloat(data[1]),
									color: data[2],
									marca: data[3],
									modelo: data[4],
									linea: data[5],
									imagen: 'imagen',
									usuario_id: objeto.Usuario_id,
								};

								informacion +=
									JSON.stringify(objeto_auto) + ';';
								lista_autos.push(objeto_auto);
							}
						}
						cont++;
					}
					//console.log(informacion);
					setCadena(informacion);
					setArreglo(lista_autos);
				};
				reader.readAsText(fileUpload.files[0]);
			} else {
				alert('This browser does not support HTML5.');
			}
		} else {
			alert('Ingrese un archivo CSV');
		}
	};

	const Registrar_autos = () => {
		let mensaje = { datos: cadena };
		alert(cadena);
		axios({
			method: 'POST',
			url: `${API_AUTOS}/registrar/carga/`,
			data: mensaje,
			headers: {
				'Content-Type': 'application/json',
			},
		}).then((response) => {
			alert(response.data.mensaje);
		});
	};

	const history = useHistory();

	return (
		<>
			<NavBar />
			<Container>
				<br />
				<Form>
					<Row className='mb-5'>
						<h1>Carga Masiva de datos</h1>
					</Row>
					<Form.Group className='mb-3'>
						<Form.Label>Seleccione un archivo</Form.Label>
						<Form.Control
							id='fileupload'
							type='file'></Form.Control>
						<br />
						<Button
							variant='success'
							onClick={(e) => {
								e.preventDefault();
								Cargar_datos();
							}}>
							Cargar datos
						</Button>
					</Form.Group>
					<Button variant='primary' onClick={Registrar_autos}>
						Registrar automóviles
					</Button>{' '}
					{'   '}
					<Button
						variant='danger'
						onClick={() => history.push('/cars/home')}>
						Regresar
					</Button>
				</Form>
			</Container>
		</>
	);
};

export default Car_register_file;
