import React, { useEffect, useState } from 'react';
import { Button, Container, Row } from 'react-bootstrap';
import { useParams, useHistory } from 'react-router';
import axios from 'axios';

import NavBar from '../../../components/NavBar/NavBar';
import Calendar from '../../../components/Calendar/Calendar';
import swal from 'sweetalert';
import { API_HOTEL } from '../../../helpers/endpoint';

export default function RoomCalendar(props: any) {
	let userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo'))
		: null;
	const { habitacion_id } = useParams<{ habitacion_id: string }>();
	const history = useHistory();

	const [calendarios, setCalendarios] = useState([]);
	const [calendario, setCalendario] = useState([]);
	const [month, setMonth] = useState('2022-03');

	useEffect(() => {
		axios
			.get(`${API_HOTEL}/calendario/${habitacion_id}`)
			.then((res) => res.data)
			.then((res) => {
				console.log(res);
				res?.success && setCalendarios(res.calendario);
			})
			.catch(handleError);
	}, [habitacion_id]);

	const handleError = (error: any) => {
		if (error?.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error?.response.data);
			console.log(error?.response.status);
			console.log(error?.response.headers);
		} else if (error?.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error?.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error);
			console.log('Error', error?.message);
		}
		console.log(error?.config);
	};

	const saveCalendar = () => {
		const [year, m] = month.split('-');
		const cal = document.querySelectorAll('#calendario .bg-primary');
		const fechas = Array.from(cal).map(
			(c) => `${year}/${m}/${c.innerHTML}`
		);
		const data = {
			habitacion_id,
			fechas: calendario
				.flat()
				.filter((dia) => dia.value !== '')
				.map((dia) => ({
					fecha: dia.value,
					estado: fechas.includes(dia.value) ? 0 : 1,
				})),
		};
		axios
			.post(`${API_HOTEL}/calendario`, data)
			.then((res) => res.data)
			.then((res) => {
				if (res.success)
					swal(
						'Actualizado!',
						'Calendario Actualizado',
						'success'
					).then(() => window.location.reload());
			})
			.catch(handleError);
	};

	return (
		<>
			<NavBar />
			<Container
				className='d-flex w-100 h-100 overflow-auto justify-content-start align-items-start flex-column overflow-auto'
				style={{ marginTop: '56px' }}>
				<Row className='d-flex w-100 justify-content-center my-4'>
					<h3>Calendario</h3>
				</Row>
				<Row className='d-flex w-100 flex-row justify-content-between my-4'>
					<div className='d-flex w-auto'>
						<Button
							type='button'
							onClick={() =>
								history.push(`/rooms/view/${habitacion_id}`)
							}>
							Regresar
						</Button>
					</div>
					<div className='d-flex w-auto'>
						<div className='d-flex w-auto me-2'>
							<input
								type='month'
								id='start'
								name='start'
								min='2022-01'
								value={month}
								style={{ background: 'white' }}
								onChange={(e) => setMonth(e.target.value)}
							/>
						</div>
						{userInfo && [2, 3].includes(userInfo?.rol_id) && (
							<Button
								type='button'
								onClick={() => saveCalendar()}>
								Guardar
							</Button>
						)}
					</div>
				</Row>
				<Row className='d-flex w-100 flex-row justify-content-center my-4'>
					<Calendar
						month={month}
						calendarios={calendarios}
						calendario={calendario}
						setCalendario={setCalendario}
					/>
				</Row>
			</Container>
		</>
	);
}
