import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import axios from 'axios';
import { useLocation, useHistory } from 'react-router-dom';
import { FaPlus } from 'react-icons/fa';

import NavBar from '../../../components/NavBar/NavBar';
import RoomFilter from '../../../components/RoomFilter/RoomFilter';
import RoomList from '../../../components/RoomList/RoomList';
import SearchBar from '../../../components/SearchBar/SearchBar';
import Loading from '../../../components/Loading/Loading';
import PaginationRoomList from '../../../components/PaginationRoomList/PaginationRoomList';
import { API_HOTEL } from '../../../helpers/endpoint';

export default function RoomsScreen() {
	let userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo'))
		: null;
	const { search, pathname } = useLocation();
	const history = useHistory();

	const [rooms, setRooms] = useState([]);
	const [loading, setLoading] = useState(false);
	const [pagination, setPagination] = useState({ page: 1 });
	const [nombre, setNombre] = useState('');
	const [pais, setPais] = useState('');
	const [ciudad, setCiudad] = useState('');
	const [personas, setPersonas] = useState(0);
	const [min, setMin] = useState(0);
	const [max, setMax] = useState(0);
	const [fecha_inicio, setFechaInicio] = useState('');
	const [fecha_fin, setFechaFin] = useState('');

	useEffect(() => {
		if (userInfo?.rol_id === 3 && pathname === '/rooms') {
			if (!search.includes('usuario_id'))
				window.location.href = `/rooms?usuario_id=${userInfo?.id_usuario}`;
		}
	}, [userInfo, search, pathname]);

	useEffect(() => {
		setLoading(true);
		axios
			.get(`${API_HOTEL}${search}`)
			.then((res) => res.data)
			.then((res) => {
				console.log(res);
				setRooms(res.habitaciones);
				setPagination(res.pagination);
				setLoading(false);
			})
			.catch(handleError);
	}, [search]);

	useEffect(() => {
		const groupParamsByKey = (params) =>
			[...params.entries()].reduce((acc, [key, val]) => {
				if (acc.hasOwnProperty(key)) {
					// if the current key is already an array, we push the value to it
					if (Array.isArray(acc[key])) {
						acc[key] = [...acc[key], val];
					} else {
						// if it's not an array, we will convert it into an array and add the current value to it
						acc[key] = [acc[key], val];
					}
				} else if (val !== '') {
					// plain assignment if no special case is present
					acc[key] = val;
				}

				return acc;
			}, {});
		const paramsToObject = (params) => {
			try {
				const urlParams = new URLSearchParams(params);
				const paramsObj = groupParamsByKey(urlParams);
				return paramsObj;
			} catch (e) {
				console.log(e);
				return {};
			}
		};

		const params = paramsToObject(search);
		console.log(params);
		params?.nombre && setNombre(params?.nombre);
		params?.pais && setPais(params?.pais);
		params?.ciudad && setCiudad(params?.ciudad);
		params?.personas && setPersonas(params?.personas);
		params?.min && setMin(params?.min);
		params?.max && setMax(params?.max);
		params?.fecha_inicio && setFechaInicio(params?.fecha_inicio);
		params?.fecha_fin && setFechaFin(params?.fecha_fin);
	}, [search]);

	const handleError = (error: any) => {
		if (error?.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error?.response.data);
			console.log(error?.response.status);
			console.log(error?.response.headers);
		} else if (error?.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error?.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error);
			console.log('Error', error?.message);
		}
		console.log(error?.config);
	};

	const handleSubmit = (e: any) => {
		e.preventDefault();
		const data = {
			nombre,
			pais,
			ciudad,
			personas,
			min,
			max,
			fecha_inicio,
			fecha_fin,
			page: pagination?.page || 1,
			usuario_id: userInfo?.rol_id === 3 ? userInfo.id_usuario : 0,
		};
		const params = Object.fromEntries(
			Object.entries(data).filter(([_, v]) => !['', 0].includes(v))
		);

		const uri = Object.entries(params)
			.map(([k, v]) => `${k}=${encodeURIComponent(v)}`)
			.join('&');
		window.location.href = `/rooms?${uri}`;
	};

	return (
		<>
			<NavBar />
			<Container
				className='d-flex w-100 h-100 justify-content-start align-items-center flex-column overflow-auto'
				style={{ paddingTop: '56px' }}>
				<Row className='d-flex w-100 justify-content-center my-4'>
					<h3>Habitaciones</h3>
				</Row>
				<Form onSubmit={handleSubmit} className='w-100 h-auto'>
					<Row className='d-flex w-100 justify-content-center my-4'>
						<Col md={12} lg={4}>
							<div className='d-flex flex-column w-100 m-0 justify-content-start align-items-center'>
								<RoomFilter
									pais={pais}
									setPais={setPais}
									ciudad={ciudad}
									setCiudad={setCiudad}
									personas={personas}
									setPersonas={setPersonas}
									min={min}
									setMin={setMin}
									max={max}
									setMax={setMax}
									fecha_inicio={fecha_inicio}
									setFechaInicio={setFechaInicio}
									fecha_fin={fecha_fin}
									setFechaFin={setFechaFin}
								/>
								<div className='d-none d-lg-flex flex-row w-100 m-0 justify-content-end align-items-center mt-3 '>
									<Button type='submit' variant='info'>
										Buscar
									</Button>
								</div>
							</div>
						</Col>
						<Col md={12} lg={8}>
							<Row className='text-start d-flex flex-row w-100 m-0 justify-content-start align-items-start'>
								<div className='d-flex flex-row w-100 m-0 justify-content-start align-items-center p-0'>
									{userInfo && [2,3].includes(userInfo?.rol_id) && (
										<div>
											<Button
												variant='primary'
												onClick={() =>
													history.push(
														`/rooms/register`
													)
												}>
												<FaPlus
													style={{
														fontSize: '1.2rem',
														color: 'black',
													}}
												/>
											</Button>
										</div>
									)}
									<SearchBar
										searchText={nombre}
										setSearchText={setNombre}
										placeholder='Nombre de habitacion'
									/>
									<div>
										<Button type='submit' variant='info'>
											Buscar
										</Button>
									</div>
								</div>
							</Row>
							{pagination && Object.keys(pagination).length > 0 && (
								<Row className='d-flex w-100 m-0 p-0'>
									<PaginationRoomList
										pagination={pagination}
									/>
								</Row>
							)}
							<Row>
								{loading && <Loading name='crescent' />}
								{rooms && rooms.length > 0 && (
									<RoomList rooms={rooms} />
								)}
							</Row>
							{pagination && Object.keys(pagination).length > 0 && (
								<Row className='d-flex flex-row w-100 m-0'>
									<PaginationRoomList
										pagination={pagination}
									/>
								</Row>
							)}
						</Col>
					</Row>
				</Form>
			</Container>
		</>
	);
}
