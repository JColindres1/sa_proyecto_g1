import React, { useState, useEffect } from 'react';
import { Container, Row, Button, Col } from 'react-bootstrap';
import axios from 'axios';
import { useHistory, useParams } from 'react-router';
import { format, parseISO } from 'date-fns';
import {
	IonButton,
	IonDatetime,
	IonIcon,
	IonInput,
	IonItem,
	IonLabel,
	IonPopover,
} from '@ionic/react';

import NavBar from '../../../components/NavBar/NavBar';

import { calendar } from 'ionicons/icons';
import { API_HOTEL, API_BANCO } from '../../../helpers/endpoint';
import swal from 'sweetalert';
import ListaReseñas from '../../../components/Reseñas/ListaReseñas';
import ListaReservas from '../../../components/Reservaciones/ListaReservas';
import Reseña from '../../../components/Reseñas/Reseña';

export default function ViewRoom() {
	const history = useHistory();
	const { habitacion_id } = useParams<{ habitacion_id: string }>();
	const [room, setRoom] = useState({
		nombre: '',
		precio: 0,
		cantidad_personas: 0,
		descripcion: '',
		estado: 1,
		usuario_id: 1,
		ciudad_id: 0,
		pais_id: null,
		nombre_pais: null,
		nombre_ciudad: null,
		habitacion_id: null,
	});
	const [modalShow, setModalShow] = useState(false);

	const [fecha_inicio, setFechaInicio] = useState('');
	const [fecha_fin, setFechaFin] = useState('');
	const formatDate = (value: string) => {
		return format(parseISO(value), 'yyyy/MM/dd');
	};

	const userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo')!)
		: null;

	useEffect(() => {
		axios
			.get(`${API_HOTEL}/${habitacion_id}`)
			.then((res) => res.data)
			.then((res) => {
				console.log(res);
				if (res.success) {
					setRoom(res.habitacion);
				} else {
					setRoom({
						nombre: '',
						precio: 0,
						cantidad_personas: 0,
						descripcion: '',
						estado: 1,
						usuario_id: 1,
						ciudad_id: 0,
						pais_id: null,
						nombre_pais: null,
						nombre_ciudad: null,
						habitacion_id: null,
					});
				}
			})
			.then(handleError);
	}, [habitacion_id]);

	const handleError = (error: any) => {
		if (error?.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error?.response.data);
			console.log(error?.response.status);
			console.log(error?.response.headers);
		} else if (error?.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error?.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error);
			console.log('Error', error?.message);
		}
		console.log(error?.config);
	};

	const realizarReservacion = () => {
		if (!fecha_inicio || !fecha_fin) {
			swal(
				'No se pudo realizar la reservación',
				'Debe indicar las fechas de reserva',
				'info'
			);
		} else {
			const monto =
				room.precio *
				(Math.abs(
					new Date(fecha_fin).getTime() -
						new Date(fecha_inicio).getTime()
				) /
					(1000 * 60 * 60 * 24) +
					1);
			swal({
				title: 'Confirmar Reservación',
				text: `La reservación está del ${fecha_inicio} al ${fecha_fin} con un costo de Q${monto.toFixed(
					2
				)}`,
				icon: 'info',
				buttons: ['Cancelar', 'Confirmar'],
				dangerMode: false,
			}).then((confirm) => {
				console.log(confirm);
				if (confirm) {
					axios
						.get(
							`${API_BANCO}/users/${userInfo?.id_usuario}/${monto}`
						)
						.then((res) => res.data)
						.then((res) => {
							if (res?.mensaje) {
								axios
									.post(`${API_HOTEL}/reserva`, {
										habitacion_id,
										usuario_id: 1,
										fecha_inicio,
										fecha_fin,
									})
									.then((res) => res.data)
									.then((res) => {
										console.log(res);
										if (res.success) {
											swal(
												'Reservación Generada',
												res.message,
												'success'
											).then(() =>
												history.push('/rooms')
											);
										} else {
											swal(
												'Reservación No Generada',
												res.message,
												'error'
											);
										}
									});
							} else if (res.code === 200 && !res?.mensaje) {
								swal(
									'No se pudo realizar la reservación',
									'No tiene suficiente saldo en su cuenta',
									'error'
								);
							} else {
								swal(
									'No se pudo realizar la reservación',
									res.mensaje,
									'error'
								);
							}
						});
				}
			});
		}
	};

	return (
		<>
			<NavBar />
			<Container
				className='d-flex w-100 h-100 overflow-auto justify-content-start align-items-start flex-column align-self-start'
				style={{ position: 'relative' }}>
				<Row className='d-flex w-100 justify-content-center my-4'>
					<h3>Habitación</h3>
				</Row>
				<Row className='mb-4 px-3'>
					<Button
						type='button'
						onClick={() => history.push(`/rooms`)}
						className='w-auto'>
						Regresar
					</Button>
				</Row>
				<Row className='d-flex w-100 justify-content-center mb-4'>
					<Col xs={12} md={6}>
						<div className='d-flex flex-column w-50 mb-3 justify-content-start'>
							<div className='d-flex flex-column w-100 justify-content-center align-items-start mb-3'>
								<div className='me-2'>
									<strong>Nombre:</strong>
								</div>
								<div className='ms-2 text-start'>
									{room?.nombre}
								</div>
							</div>
							<div className='d-flex flex-column w-100 justify-content-center align-items-start mb-3'>
								<div className='me-2'>
									<strong>Límite:</strong>
								</div>
								<div className='ms-2 text-start'>
									{room?.cantidad_personas} personas
								</div>
							</div>
							<div className='d-flex flex-column w-100 justify-content-center align-items-start mb-3'>
								<div className='me-2'>
									<strong>País:</strong>
								</div>
								<div className='ms-2 text-start'>
									{room?.nombre_pais}
								</div>
							</div>
							<div className='d-flex flex-column w-100 justify-content-center align-items-start mb-3'>
								<div className='me-2'>
									<strong>Ciudad:</strong>
								</div>
								<div className='ms-2 text-start'>
									{room?.nombre_ciudad}
								</div>
							</div>
							<div className='d-flex flex-column w-100 justify-content-center align-items-start mb-3'>
								<div className='me-2'>
									<strong>Descripcion:</strong>
								</div>
								<div className='ms-2 text-start'>
									{room?.descripcion}
								</div>
							</div>
							<div className='d-flex flex-column w-100 justify-content-center align-items-start mb-3'>
								<div className='me-2'>
									<strong>Precio:</strong>
								</div>
								<div className='ms-2 text-start'>
									Q.{room?.precio.toFixed(2)}
								</div>
							</div>
						</div>
					</Col>
					<Col xs={12} md={6}>
						<div className='d-flex flex-column w-100 mb-3 justify-content-start align-items-end'>
							<div className='d-flex flex-row w-100 justify-content-center mb-3'>
								<strong>Realizar Reservación:</strong>
							</div>
							<div className='d-flex flex-row w-100 justify-content-end mb-3'>
								<div className='p-2'>
									<IonLabel>Fecha Inicial</IonLabel>
									<IonItem>
										<IonInput
											id='date-input-1'
											value={fecha_inicio}
										/>
										<IonButton
											fill='clear'
											id='open-date-input-1'>
											<IonIcon icon={calendar} />
										</IonButton>
										<IonPopover
											trigger='open-date-input-1'
											showBackdrop={false}>
											<IonDatetime
												presentation='date'
												onIonChange={(ev) =>
													setFechaInicio(
														formatDate(
															ev.detail.value!
														)
													)
												}
											/>
										</IonPopover>
									</IonItem>
								</div>
								<div className='p-2'>
									<IonLabel>Fecha Final</IonLabel>
									<IonItem>
										<IonInput
											id='date-input-2'
											value={fecha_fin}
										/>
										<IonButton
											fill='clear'
											id='open-date-input-2'>
											<IonIcon icon={calendar} />
										</IonButton>
										<IonPopover
											trigger='open-date-input-2'
											showBackdrop={false}>
											<IonDatetime
												presentation='date'
												onIonChange={(ev) =>
													setFechaFin(
														formatDate(
															ev.detail.value!
														)
													)
												}
											/>
										</IonPopover>
									</IonItem>
								</div>
							</div>
							<div className='d-flex flex-row w-100 justify-content-end'>
								<Button
									variant='primary'
									className='me-2'
									onClick={() =>
										history.push(
											`/rooms/calendar/${habitacion_id}`
										)
									}>
									Calendario
								</Button>
								<Button
									variant='success'
									onClick={() => realizarReservacion()}>
									Reservar
								</Button>
							</div>
						</div>
					</Col>
				</Row>
				<Row className='w-100 my-2 justify-content-end'>
					<Button
						variant='primary'
						className='w-auto'
						onClick={() => setModalShow(true)}>
						Realizar Reseña
					</Button>
					<Reseña
						show={modalShow}
						onHide={() => setModalShow(false)}
					/>
				</Row>
				<Row className='w-100 my-2'>
					<ListaReseñas type={'habitacion'} id={habitacion_id} />
				</Row>
				{userInfo?.rol_id === 3 && (
					<Row className='w-100 mb-5'>
						<ListaReservas type={'habitacion'} id={habitacion_id} />
					</Row>
				)}
			</Container>
		</>
	);
}
