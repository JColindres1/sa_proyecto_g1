import React, { useState, useEffect } from 'react';
import { Container, Row } from 'react-bootstrap';
import axios from 'axios';
import { useParams } from 'react-router';

import NavBar from '../../../components/NavBar/NavBar';
import RoomForm from '../../../components/RoomForm/RoomForm';
import { API_HOTEL } from '../../../helpers/endpoint';

export default function UpdateRoom() {
	const {habitacion_id} = useParams<{habitacion_id: string}>();
	console.log(habitacion_id);
	const [room, setRoom] = useState({});

	useEffect(() => {
		axios
			.get(`${API_HOTEL}/${habitacion_id}`)
			.then((res) => res.data)
			.then((res) => {
				console.log(res);
				if (res.success) {
					setRoom(res.habitacion);
				} else {
					setRoom({});
				}
			})
			.then(handleError);
	}, [habitacion_id]);

	const handleError = (error: any) => {
		if (error?.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error?.response.data);
			console.log(error?.response.status);
			console.log(error?.response.headers);
		} else if (error?.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error?.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error);
			console.log('Error', error?.message);
		}
		console.log(error?.config);
	};

	return (
		<>
			<NavBar />
			<Container
				className='d-flex w-100 h-100 overflow-auto justify-content-start align-items-start flex-column overflow-auto'
				style={{ marginTop: '56px' }}>
				<Row className='d-flex w-100 justify-content-center my-4'>
					<h3>Actualizar Habitación</h3>
				</Row>
				<Row className='d-flex w-100 justify-content-center mb-4'>
					<RoomForm action='update' room={room} />
				</Row>
			</Container>
		</>
	);
}
