import React from 'react';
import { Container } from 'react-bootstrap';
import NavBar from '../../components/NavBar/NavBar';
import { IonContent } from '@ionic/react';
import ExploreContainer from '../../components/Explore/ExploreContainer';

import logo from '../../assets/8621.jpg';
const InicioT: React.FC = () => {
	return (
		<>
			<NavBar />
			<ExploreContainer
				title='Turista'
				imgIcon={logo}
				name='Ver Habitaciones'
                route='/rooms'
			/>
		</>
	);
};

export default InicioT;
