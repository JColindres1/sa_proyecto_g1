import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import NavBar from '../../components/NavBar/NavBar';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { useIonAlert } from '@ionic/react';
import { API_VACUNAS, API_PASAPORTE } from '../../helpers/endpoint';
import { IonContent } from '@ionic/react';
import Select from 'react-select';
import { IonList, IonItem, IonLabel } from '@ionic/react';

const InicioT: React.FC = () => {
	const [data, setData] = useState<any>({
		id_dosis: 0,
		dosis: '',
	});
	const [loading, setLoading] = useState<any>(false);
	const [dosis, setDosis] = useState<any>([]);
	const [pasaporte, setPasaporte] = useState<any>([]);

	const userInfo = JSON.parse(localStorage.getItem('userInfo') || '{}');

	const [present] = useIonAlert();

	const handleError = (error: any) => {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error.response.data);
			console.log(error.response.status);
			console.log(error.response.headers);
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error.message);
		}
		console.log(error.config);
		setLoading(false);
	};

	useEffect(() => {
		setLoading(true);
		axios
			.get(`${API_VACUNAS}/dosis`)
			.then((res) => res.data)
			.then((res) => setDosis(res))
			.catch(handleError)
			.finally(() => setLoading(false));
		axios
			.get(`${API_PASAPORTE}/usuario/${userInfo.id_usuario}`)
			.then((res) => setPasaporte(res.data))
			.catch(handleError)
			.finally(() => setLoading(false));
	}, [userInfo.id_usuario]);

	const sendData = () => {
		axios
			.post(`${API_PASAPORTE}/usuario/dosis`, {
				id_usuario: userInfo.id_usuario,
				id_dosis: data.id_dosis,
			})
			.then((res) => res.data)
			.then((res) => {
				console.log(res);
				present({
					cssClass: 'my-css',
					header: '¡Éxito!',
					message: res,
					buttons: [
						{
							text: 'Ok',
							handler: (d) => console.log('ok pressed'),
						},
					],
				});
				axios
					.get(`${API_PASAPORTE}/usuario/${userInfo.id_usuario}`)
					.then((res) => {
						setPasaporte(res.data);
					})
					.catch(handleError);
			})
			.catch(handleError);
	};
	return (
		<>
			<IonContent>
				<NavBar />
				<Container
					className='d-flex w-100 h-100 overflow-auto justify-content-start align-items-start flex-column overflow-auto'
					style={{ marginTop: '56px' }}>
					<Row className='d-flex w-100 justify-content-center my-4'>
						<h1 className='w-auto p-0 m-0 text-center'>
							Esquema de turista
						</h1>
					</Row>
					<Row className='w-100'>
						<Col xs={12} md={6} className='mb-5'>
							<div className='d-flex w-100 justify-content-center my-4'>
								<h2 className='w-auto p-0 m-0 text-center'>
									Seleccionar Dosis
								</h2>
							</div>
							<Row className='d-flex w-100'>
								<div className='d-flex w-100 justify-content-center my-2'>
									<Select
										className='basic-single w-100'
										classNamePrefix='select'
										isLoading={loading}
										isClearable
										isSearchable
										name='dosis'
										value={{
											value: data?.id_dosis,
											label: data?.dosis,
										}}
										onChange={(vacuna) => {
											setData({
												id_dosis: vacuna.value,
												dosis: vacuna.label,
											});
										}}
										options={dosis?.map((dosis: any) => ({
											value: dosis?.id_dosis,
											label: `${dosis?.nombre_vacuna} - ${dosis?.tipo}`,
										}))}
									/>
								</div>
							</Row>
							<Row className='d-flex w-100'>
								<div className='d-flex w-100 justify-content-end my-2'>
									<Button onClick={() => sendData()}>
										Agregar
									</Button>
								</div>
							</Row>
						</Col>
						<Col xs={12} md={6} className='mb-5'>
							<div className='d-flex w-100 justify-content-center my-4'>
								<h2 className='w-auto p-0 m-0 text-center'>
									Dosis Registradas
								</h2>
							</div>
							<Row className='d-flex w-100  my-4'>
								<IonList>
									{pasaporte.length > 0 &&
										pasaporte.map((pasaporte: any) => (
											<IonItem key={pasaporte.id_dosis}>
												<IonLabel className='justify-content-center text-center'>
													{`${pasaporte.nombre_vacuna} - ${pasaporte.tipo}`}
												</IonLabel>
											</IonItem>
										))}
								</IonList>
							</Row>
						</Col>
					</Row>
				</Container>
			</IonContent>
		</>
	);
};

export default InicioT;
