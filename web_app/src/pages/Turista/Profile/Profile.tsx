import React, { useState, useEffect } from 'react';
import { Container, Form, Row, Col, Button } from 'react-bootstrap';
import NavBar from '../../../components/NavBar/NavBar';
import { useHistory } from 'react-router-dom';
import axios from "axios";
import { useIonAlert } from '@ionic/react';
import { API_USERS } from '../../../helpers/endpoint';
import { IonContent } from '@ionic/react';
import Profile from '../../../components/Profile/Profile';
const Tourist_profile: React.FC = () => {

    const history = useHistory();
    const [present] = useIonAlert();

    let verificarEsquema = () => {
        const config = {
            headers: { Authorization: `Bearer ${localStorage.getItem("bearer")}` }
        };
        const obj = JSON.parse(localStorage.getItem("userInfo"))
        axios
            .post(`${API_USERS}/esquema`, {
                //va el json a enviar en post
                Usuario_id: obj.Usuario_id
            }, config)
            .then((response) => {
                if (response.data === `{'message':'Ya cuentas con tu esquema habilitado'}`) {
                    localStorage.setItem("mostrarEV", "true")
                }
                else {
                    localStorage.setItem("mostrarEV", "false")
                }
                history.push('/turista/esquema')
            });
    }

    return (
        <>
          <IonContent>
            <NavBar />
    
            <Profile/>
   
            </IonContent>
        </>
    );
};

export default Tourist_profile;