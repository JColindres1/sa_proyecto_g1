import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import axios from 'axios';
import { useLocation /* useHistory */ } from 'react-router-dom';
/* import { FaPlus } from 'react-icons/fa'; */

import NavBar from '../../../components/NavBar/NavBar';
/* import RoomFilter from '../../../components/RoomFilter/RoomFilter'; */
import SearchBar from '../../../components/SearchBar/SearchBar';
import Loading from '../../../components/Loading/Loading';
/* import PaginationRoomList from '../../../components/PaginationRoomList/PaginationRoomList'; */
import { API_HOTEL, API_ESB } from '../../../helpers/endpoint';
import ServiceList from '../../../components/ServiceList/ServiceList';

const reservas = [
	{
		id_reserva: 1,
		nombre: 'Habitacion 1',
		estado: 1,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		fecha_pago: '2020-05-02',
		tipo: 'Habitacion',
		precio: 100,
	},
	{
		id_reserva: 2,
		nombre: 'Auto 1',
		estado: 0,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		tipo: 'Auto',
		precio: 100,
	},
	{
		id_reserva: 3,
		nombre: 'Vuelo 1',
		estado: 1,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		fecha_pago: '2020-05-02',
		tipo: 'Vuelo',
		precio: 100,
	},
];

export default function Reservaciones() {
	let userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo'))
		: null;
	const { search } = useLocation();
	/* 	const history = useHistory(); */

	const [services, setServices] = useState({
		hoteles: [],
		autos: [],
		vuelos: [],
	});
	const [loading, setLoading] = useState(false);
	/* 	const [pagination, setPagination] = useState({ page: 1 }); */
	const [nombre, setNombre] = useState('');

	useEffect(() => {
		setLoading(true);
		axios
			.get(`${API_HOTEL}/reservacion/${userInfo.id_usuario}`)
			.then((res) => res.data)
			.then((res) => {
				console.log(res);
				setServices({ ...services, hoteles: res });
			})
			.catch(handleError)
			.finally(() => setLoading(false));
		setLoading(true);
		axios
			.get(`${API_ESB}/reservar-vuelo/${userInfo.id_usuario}`)
			.then((res) => res.data)
			.then((res) => {
				console.log(res);
				setServices({ ...services, vuelos: res });
			})
			.catch(handleError)
			.finally(() => setLoading(false));
		setLoading(true);
		axios
			.get(`${API_ESB}/autos/reservacompleta/${userInfo.id_usuario}`)
			.then((res) => res.data)
			.then((res) => {
				console.log(res);
				res =
					typeof res === 'string'
						? JSON.parse(res.replaceAll("'", '"'))
						: res;
				setServices({
					...services,
					autos: typeof res === 'object' ? res.mensaje : res,
				});
			})
			.catch(handleError)
			.finally(() => setLoading(false));
		return () => {
			setServices({
				hoteles: [],
				autos: [],
				vuelos: [],
			});
		};
	}, [search]);

	useEffect(() => {
		const groupParamsByKey = (params) =>
			[...params.entries()].reduce((acc, [key, val]) => {
				if (acc.hasOwnProperty(key)) {
					// if the current key is already an array, we push the value to it
					if (Array.isArray(acc[key])) {
						acc[key] = [...acc[key], val];
					} else {
						// if it's not an array, we will convert it into an array and add the current value to it
						acc[key] = [acc[key], val];
					}
				} else if (val !== '') {
					// plain assignment if no special case is present
					acc[key] = val;
				}

				return acc;
			}, {});
		const paramsToObject = (params) => {
			try {
				const urlParams = new URLSearchParams(params);
				const paramsObj = groupParamsByKey(urlParams);
				return paramsObj;
			} catch (e) {
				console.log(e);
				return {};
			}
		};

		const params = paramsToObject(search);
		console.log(params);
		params?.nombre && setNombre(params?.nombre);
	}, [search]);

	const handleError = (error: any) => {
		if (error?.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error?.response.data);
			console.log(error?.response.status);
			console.log(error?.response.headers);
		} else if (error?.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error?.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error);
			console.log('Error', error?.message);
		}
		console.log(error?.config);
		setLoading(false);
	};

	const handleSubmit = (e: any) => {
		e.preventDefault();
		const data = {
			nombre,
			usuario_id: userInfo.id_usuario,
		};
		const params = Object.fromEntries(
			Object.entries(data).filter(([_, v]) => !['', 0].includes(v))
		);

		const uri = Object.entries(params)
			.map(([k, v]) => `${k}=${encodeURIComponent(v)}`)
			.join('&');
		window.location.href = `/turista/reservaciones?${uri}`;
	};

	return (
		<>
			<NavBar />
			<Container
				className='d-flex w-100 h-100 justify-content-start align-items-center flex-column overflow-auto'
				style={{ paddingTop: '56px' }}>
				<Row className='d-flex w-100 justify-content-center my-4'>
					<h3>Reservaciones</h3>
				</Row>
				<Form onSubmit={handleSubmit} className='w-100 h-auto'>
					<Row className='d-flex w-100 justify-content-center my-4'>
						<Col xs={12}>
							{/* <Row className='text-start d-flex flex-row w-100 m-0 justify-content-start align-items-start'>
								<div className='d-flex flex-row w-100 m-0 justify-content-start align-items-center p-0'>
									<SearchBar
										searchText={nombre}
										setSearchText={setNombre}
										placeholder='Nombre de servicio'
									/>
									<div>
										<Button type='submit' variant='info'>
											Buscar
										</Button>
									</div>
								</div>
							</Row> */}
							{/* {pagination && Object.keys(pagination).length > 0 && (
								<Row className='d-flex w-100 m-0 p-0'>
									<PaginationRoomList
										pagination={pagination}
									/>
								</Row>
							)} */}
							<Row>
								{loading && <Loading name='crescent' />}
								{!loading && (
									<ServiceList
										services={[
											...services.hoteles,
											...services.autos,
											...services.vuelos,
										]}
									/>
								)}
							</Row>
							{/* {pagination && Object.keys(pagination).length > 0 && (
								<Row className='d-flex flex-row w-100 m-0'>
									<PaginationRoomList
										pagination={pagination}
									/>
								</Row>
							)} */}
						</Col>
					</Row>
				</Form>
			</Container>
		</>
	);
}
