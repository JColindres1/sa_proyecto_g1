import { Form, Row, Col, Button, Container } from 'react-bootstrap';
import {
	IonContent,
	IonCard,
	IonCardHeader,
	IonCardTitle,
	IonCardSubtitle,
} from '@ionic/react';
import NavBar from '../../../components/NavBar/NavBar';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { API_AUTOS } from '../../../helpers/endpoint';
const Mis_reservaciones: React.FC = () => {
	const [autos, setAutos] = useState([]);
	const history = useHistory();
	const objeto = JSON.parse(localStorage.getItem('userInfo'));

	useEffect(() => {
		const Mostrar_reservaciones = () => {
			axios({
				method: 'GET',
				url: `${API_AUTOS}/reservaciones/` + objeto.Usuario_id,
			}).then((response) => {
				//console.log(response.data.mensaje);
				setAutos(response.data.data);
			});
		};
		Mostrar_reservaciones();
	}, [objeto.Usuario_id]);

	return (
		<>
			<IonContent className='background'>
				<NavBar />
				<Container
					className='d-flex w-100 h-100 justify-content-start align-items-center flex-column '
					style={{ paddingTop: '56px' }}>
					<Form className='w-100 h-auto'>
						<Row className='text-start d-flex flex-row w-100 m-0 justify-content-start align-items-start'>
							<Col>
								<h1>Automoviles Disponibles</h1>
							</Col>
							<Col>
								<Button
									variant='danger'
									onClick={() =>
										history.push('/turista/home')
									}>
									Regresar
								</Button>
							</Col>
						</Row>

						<div className='d-flex flex-column w-100 m-0 justify-content-start align-items-center'>
							<Row className='text-start d-flex flex-row w-100 m-0 justify-content-start align-items-start'>
								{autos.map((elemento, key) => (
									<IonCard key={key}>
										<IonCardHeader>
											<IonCardTitle>
												Marca: {elemento.marca}{' '}
												{elemento.modelo} {'  -  '}{' '}
												Linea: {elemento.linea}{' '}
											</IonCardTitle>
											<IonCardSubtitle>
												Fecha de inicio:{' '}
												{elemento.fecha_i} {' - '} Fecha
												final: {elemento.fecha_f}{' '}
											</IonCardSubtitle>
										</IonCardHeader>
									</IonCard>
								))}
							</Row>
						</div>
					</Form>
					<div className='d-flex flex-column w-100 m-0 justify-content-end align-items-center mt-5'></div>
				</Container>
			</IonContent>
		</>
	);
};

export default Mis_reservaciones;
