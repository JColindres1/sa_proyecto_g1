import { Form, Row, Col, Button, Container,Card } from 'react-bootstrap';
import { useIonAlert,IonItem,IonLabel,IonInput,IonContent,IonDatetime,IonButton,IonIcon,IonModal } from '@ionic/react';
import NavBar from '../../../components/NavBar/NavBar';
import React, { useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import axios from "axios";
import { API_AUTOS,API_BANCO } from '../../../helpers/endpoint';
import { calendar } from 'ionicons/icons';
import { format, parseISO } from 'date-fns';
import swal from 'sweetalert';
import { GrFormAdd } from 'react-icons/gr';
import './Cars_reservacion.css';
const Cars_reservacion: React.FC = () => {
    
    const [startDate, setStartDate] = useState("");
    const [endDate, setEndDate] = useState("");
    const [total,setTotal] = useState(0);
    const [popoverDate2, setPopoverDate2] = useState('');
    const [popoverDate1, setPopoverDate1] = useState('');
    
    const history = useHistory();

    const temp = JSON.parse(localStorage.getItem("autoinfo"));
    console.log(temp);
    const objeto = JSON.parse(localStorage.getItem("userInfo"));


    const Regresar = () => {
        history.push('/turista/autos');
    }

    const Confirmar = () =>{
        if(total !== 0){
            var fechai = startDate.split('T')[0]; 
            var fechaf = endDate.split('T')[0];
            let mensaje = {fecha_i:fechai,fecha_f:fechaf,estado:1,precio:total,usuario_id:objeto.Usuario_id,auto_id:temp.Auto_id}
            axios({
                method: "POST",
                url: `${API_AUTOS}/reservacion`,
                data: mensaje,
                headers: {
                    'Content-Type':'application/json'
                }
            }).then((response) => {
                alert(response.data.mensaje)
            });
        }else{
            alert('Verifique el precio de la reservacion');
        }
        history.push('/turista/autos');
    }


    // const CalcularPrecio = () => {
    //     var fechai = new Date(startDate.split('T')[0]); 
    //     var fechaf = new Date(endDate.split('T')[0]);
    //     var resultado = Calculo(fechai,fechaf);
    //     var total = resultado * parseFloat(temp.Precio);
    //     setTotal(total)
    // }

    const formatDate = (value: string) => {
        return format(parseISO(value), 'MMM dd yyyy');
    };

    const setStartDateValue = (value)=>{
        setPopoverDate1(value);
         if(popoverDate2 !== ''){
            let result = Calculo(value,popoverDate2);
            setTotal(result*temp.precio_dia)
         }
    }

    const setEndDateValue = (value)=>{
        
        setPopoverDate2(value);
        if(popoverDate1 !== ''){
          let result  = Calculo(popoverDate1,value);
          setTotal(result*temp.precio_dia)
        }
   }

    const messageSucces =()=>{
        swal(
            'Exitoso!',
            'Reservación realizada con exito',
            'success'
        );
    }

    const messageError =()=>{
        swal(
            'Error!',
            'Parece que hubo un problema :(',
            'error'
        );
    }

    const Calculo = (dateStartValue,dateEndValue) => { 
        console.log("Calculo") 
        let date1 = new Date(dateStartValue);
        let date2 = new Date(dateEndValue);
        console.log(date1);
        console.log(date2);
        const date1utc = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
        const date2utc = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());
        console.log(date1utc);
        console.log(date2utc);
        let day = 1000*60*60*24;
        console.log((date2utc - date1utc)/day);
        return(date2utc - date1utc)/day
    }

    const reserveComplete = () =>{

        let date1 = new Date(popoverDate1);
        let date2 = new Date(popoverDate2);

        let data = {
            "fecha_inicio":date1.getFullYear()+"-"+date1.getMonth()+"-"+date1.getDate(),
            "fecha_fin":date2.getFullYear()+"-"+date2.getMonth()+"-"+date2.getDate(),
            "idusuario":objeto.id_usuario,
            "idvehiculo":temp.placa,
            "idtarjeta":1,
            "total":total
        }

        console.log(data);
   

        // axios.get(``).then(
            
        // )
    } 

    const Reserve = async (type) =>{
        let date1 = new Date(popoverDate1);
        let date2 = new Date(popoverDate2);

        let data = {
            "fecha_inicio":date1.getFullYear()+"-"+date1.getMonth()+"-"+date1.getDate(),
            "fecha_fin":date2.getFullYear()+"-"+date2.getMonth()+"-"+date2.getDate(),
            "idusuario":objeto.id_usuario,
            "idvehiculo":temp.auto_id,
            "idtarjeta":type,
            "total":total
        }
        let response = await axios.get(`${API_BANCO}/users/${objeto.id_usuario}/${total}`)
        console.log(data);
        console.log(response);
        // if(type == 1){
        // let response:any = await axios.get(`http://34.133.128.186:4000/esb/banco/users/${objeto.id_usuario}/${total}`);
        // if(response.code == 200 && response.mensaje){
        //    let reserveResponse:any =  await axios.post(`http://34.133.128.186:4000/esb/autos/reservacion`,data)
        //    if(reserveResponse.code == 200){
        //        //correcto
        //        messageSucces()
        //       return
        //     }
        //     messageError()
        //     //incorrecto
        //   }
        //    // no se ha podido realizar reservación
        // }else{
        //     await axios.post(`http://34.133.128.186:4000/esb/autos/reservacion`,data)
        //     let reserveResponse:any =  await axios.post(`http://34.133.128.186:4000/esb/autos/reservacion`,data)
        //     if(reserveResponse.code == 200){
        //         //correcto reservación parcial
        //         messageSucces()
        //        return
        //      }
        //      //incorrecto
        //      messageError()
        // }



    }

    return (
        <>
        <IonContent className='background'>

        
        <NavBar/>
            <Container className='d-flex w-100 h-100 justify-content-start align-items-center flex-column '
            style={{ paddingTop: '56px' }}>
                            <h1 className='title-custom title-center'>{temp.marca} {" "} {temp.modelo} {" - "} {temp.linea}</h1>
                            <img className="img-size" src={temp.imagen} />
                <div className='card-container'>
              
                <Card className="login-card">    
                <div className='margin-login'>
                <Row className='mb-5'>
                    <Col sm={6}>
                        <IonItem>
                            <IonLabel position="floating"> Placa  </IonLabel>
                            <br />
                            <IonInput disabled> {temp.placa}</IonInput>
                        </IonItem>
                    </Col>

                </Row>


                <Row className='mb-5'>
                    <Col sm={6}>
                    <Form.Group controlId="Datesi">
                        <Form.Label>Fecha inicial de reservacion</Form.Label>
                        <IonItem>
                                        <IonInput id="date-input-1" value={popoverDate1} />
                                        <IonButton fill="clear" id="open-modal-1">
                                            <IonIcon icon={calendar} />
                                        </IonButton>
                                    </IonItem>
                                    <IonModal trigger="open-modal-1">
                                        <IonContent  force-overscroll="false">
                                            <div className='center-calendar'>
                                                <IonDatetime  presentation="date"
                                                    onIonChange={ev => setStartDateValue(formatDate(ev.detail.value!))}></IonDatetime>
                                            </div>
                                        </IonContent>
                                    </IonModal>
                    </Form.Group>
                    </Col>
                    <Col sm={6}>
                    <Form.Group controlId="Datesf">
                    <Form.Label>Fecha final de reservacion</Form.Label>
                    <IonItem>
                                        <IonInput id="date-input-2" value={popoverDate2} />
                                        <IonButton fill="clear" id="open-modal">
                                            <IonIcon icon={calendar} />
                                        </IonButton>
                                    </IonItem>
                                    <IonModal trigger="open-modal">
                                        <IonContent  force-overscroll="false">
                                            <div className='center-calendar'>
                                                <IonDatetime  presentation="date"
                                                    onIonChange={ev => setEndDateValue(formatDate(ev.detail.value!))}></IonDatetime>
                                            </div>
                                        </IonContent>
                                    </IonModal>
                    </Form.Group>
                        
                    </Col>
                </Row>
   
                <Row className='mb-5'>
                    <Col sm={6}>
                        <IonItem>
                            <IonLabel position="floating"> Precio por dia  </IonLabel>
                            <br />
                            <IonInput disabled> {temp.precio_dia}</IonInput>
                        </IonItem>
                    </Col>
                    <Col sm={6}>
                        <IonItem>
                            <IonLabel position="floating"> Precio Total  </IonLabel>
                            <br />
                            <IonInput disabled value={total}>  </IonInput>
                        </IonItem>
                    </Col>
                </Row>

                <Button variant="primary"  onClick={() => Reserve(0)}>
                        Reservación Parcial
                </Button> {"  "}
                <Button variant="primary"  onClick={() => Reserve(1)}>
                        Reservación Completa
                </Button>
                </div>
                </Card>
                </div>
         
                
                
            </Container>
            </IonContent>
        </>
    );
};

export default Cars_reservacion;
