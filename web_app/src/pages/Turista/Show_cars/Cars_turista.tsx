import { /* Form, Row, Col, Button, */ Container } from 'react-bootstrap';
import { useIonAlert, IonContent/* , IonCard, IonCardHeader, IonCardTitle, IonCardContent, IonCardSubtitle */ } from '@ionic/react';
import NavBar from '../../../components/NavBar/NavBar';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from "axios";
import { API_AUTOS } from '../../../helpers/endpoint';
import './Cars_turista.css';
/* import { motion } from "framer-motion"; */
import CardContent from '../../../components/CardContent/CardContent';
const Cars_turista: React.FC = () => {

    const [autos, setAutos] = useState([]);
    const history = useHistory();
    const objeto = JSON.parse(localStorage.getItem("userInfo"));
    const [present] = useIonAlert();

    const Mostrar_vehiculos = () => {
        axios({
            method: "GET",
            url: `${API_AUTOS}`
        }).then((response) => {
            //console.log(response.data.mensaje);
            let data_response  =  JSON.parse(response.data.replaceAll('\'','"'));
            //console.log(JSON.parse(response.data.datos));
            //let data = response.data.mensaje; 
            console.log(data_response.datos);
            let textList = []
            for(let car of data_response.datos){
                let text = {
                  title : `${car.marca} ${car.modelo}`,
                  subtitle: `${car.linea}  IDE ${car.placa}`,
                  description: ` Precio: ${car.precio_dia}  Ciudad :${car.idciudad}`,
                  img: car.imagen,
                  obj: car
                }
                textList.push(text);  
            }

            setAutos(textList);
        });
    }

    const get_reservation = (obj) => {
        console.log("object: ")
        console.log(obj);
        let informacion = { Auto_id: obj.placa, Marca: obj.marca, Modelo: obj.modelo, Linea: obj.linea, Precio: obj.precio_dia};
        console.log(obj)
        localStorage.setItem("autoinfo", JSON.stringify(obj));
        history.push({
            pathname: '/turista/reservacion/autos',
        });
    }

    useEffect(() => {
        if (localStorage.getItem("userInfo") === null) {
            history.push('/home')
        }
        else if (objeto.rol_id !== 1) {
            present({
                cssClass: 'my-css',
                header: '¡Oops!',
                message: 'No tienes permisos para esta vista',
                buttons: [
                    { text: 'Ok', handler: (d) => console.log('ok pressed') },
                ],
            })
            if (objeto.rol_id === 2) {
                history.push('/admin/home')
            }
            else if (objeto.rol_id === 3) {
                history.push('/schedules')
            }
            else if (objeto.rol_id === 4) {
                history.push('/cars/home')
            }
            else if (objeto.rol_id === 5) {
                history.push('/flight_register')
            }
        }
        Mostrar_vehiculos();
    }, [history, objeto.rol_id, present]);

    return (
        <>
            <NavBar />
            <IonContent className='background'>
                <Container className='d-flex w-100 h-100 justify-content-start align-items-center flex-column '
                    style={{ paddingTop: '56px' }}>
                    <CardContent content={autos} get_reservation={get_reservation} title="Automóviles Disponibles"/>

                </Container>
            </IonContent>
        </>
    );
};

export default Cars_turista;
