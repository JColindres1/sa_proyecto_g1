import { Row, Container, Badge, Button } from 'react-bootstrap';
import { IonContent, IonModal, IonItem, IonLabel } from '@ionic/react';
import NavBar from '../../../components/NavBar/NavBar';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './show_flight.css';
import {
	API_VUELOS,
	API_FLIGHTS,
	API_USERS,
	API_ESB,
} from '../../../helpers/endpoint';
import swal from 'sweetalert';
import CardContent from '../../../components/CardContent/CardContent';

const Show_flight: React.FC = () => {
	const [data, setData] = useState({
		id_pais_origen: 0,
		id_ciudad_origen: 0,
		id_pais_destino: 0,
		id_ciudad_destino: 0,
		precio_mayor: 0,
		precio_menor: 0,
		fecha_salida: '',
	});
	const [flights, setFlights] = useState([]);
	const [loading, setLoading] = useState(false);
	const [currentElement, setCurrent] = useState<any>();
	const [showModal, setShowModal] = useState(false);

	const clickHandle = (value: any) => {
		setCurrent(value);
		setShowModal(true);
	};

	const handleError = (error: any) => {
		if (error?.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error?.response.data);
			console.log(error?.response.status);
			console.log(error?.response.headers);
		} else if (error?.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error?.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error);
			console.log('Error', error?.message);
		}
		console.log(error?.config);
	};

	useEffect(() => {
		const getFlights = () => {
			setLoading(true);
			axios
				.get(`${API_VUELOS}`, {
					params: { ...data },
				})
				.then((res) => res.data)
				.then((res) => {
					setFlights(res);
				})
				.catch(handleError)
				.finally(() => setLoading(false));
		};
		getFlights();
	}, []);

	const search = () => {
		console.log(data);
		setLoading(true);
		axios
			.get(`${API_VUELOS}`, {
				params: { ...data },
			})
			.then((res) => res.data)
			.then((res) => {
				setFlights(res);
			})
			.catch(handleError)
			.finally(() => setLoading(false));
	};

	const Reserve = (element) => {
		const obj = JSON.parse(localStorage.getItem('userInfo'));
		console.log(element);
		axios
			.get(
				`${API_ESB}/esquema/pais/usuario/${element.id_pais_destino}/${obj.id_usuario}`
			)
			.then((response) => response.data)
			.then((res) => {
				console.log(res);
			})
			.catch(handleError);
	};

	const get_reservation = (obj) => {
		console.log('object: ');
		console.log(obj);
		clickHandle(obj);
	};

	return (
		<>
			<NavBar />

			<IonModal isOpen={showModal} swipeToClose={true}>
				<Container>
					<Row className='mb-5'>
						<h1 className='title-color'>Viaje</h1>
					</Row>
					<Row className='mb-5'>
						<IonItem>
							<IonLabel>
								{' '}
								<span className='text-size'>
									<Badge bg='info'>Origen:</Badge>
								</span>{' '}
							</IonLabel>
							<IonLabel className='center-text-infor'>
								<span className='text-infor'>
									{`${currentElement?.nombre_ciudad_origen} (${currentElement?.nombre_pais_origen})`}
								</span>
							</IonLabel>
						</IonItem>
						<IonItem>
							<IonLabel>
								<span className='text-size'>
									<Badge bg='info'> Destino: </Badge>
								</span>
							</IonLabel>
							<IonLabel className='center-text-infor'>
								<span className='text-infor'>
									{`${currentElement?.nombre_ciudad_destino} (${currentElement?.nombre_pais_destino})`}
								</span>
							</IonLabel>
						</IonItem>
						<IonItem>
							<IonLabel>
								<span className='text-size'>
									<Badge bg='info'>Tipo Vuelo: </Badge>
								</span>
							</IonLabel>
							<IonLabel className='center-text-infor'>
								<span className='text-infor'>IDA</span>
							</IonLabel>
						</IonItem>
						<IonItem>
							<IonLabel>
								<span className='text-size'>
									<Badge bg='info'>Precio:</Badge>
								</span>
							</IonLabel>
							<IonLabel className='center-text-infor'>
								<span className='text-infor'>
									Q. {currentElement?.precio}
								</span>
							</IonLabel>
						</IonItem>
					</Row>
					<Button
						variant='primary'
						onClick={(e) => Reserve(currentElement)}>
						Reservar
					</Button>
					<Button
						variant='primary'
						className='btn-position'
						onClick={(e) => setShowModal(false)}>
						Close
					</Button>
				</Container>
			</IonModal>

			<IonContent className='background'>
				<Container
					className='d-flex w-100 h-100 justify-content-start align-items-center flex-column '
					style={{ paddingTop: '56px' }}>
					<CardContent
						content={flights}
						get_reservation={get_reservation}
						title='Vuelos Disponibles'
						loading={loading}
						data={data}
						setData={setData}
						search={search}
					/>
				</Container>
			</IonContent>
		</>
	);
};

export default Show_flight;
