import { Form, Row, Col, Button, Card } from 'react-bootstrap';
import {
	IonInput,
	IonItem,
	IonLabel,
	IonSelect,
	IonSelectOption,
	IonContent,
	IonIcon,
	IonDatetime,
	IonModal,
	IonButton,
} from '@ionic/react';
import NavBar from '../../../components/NavBar/NavBar';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
	API_FLIGHTS,
	API_HOTEL,
	API_CARGA_VUELOS,
} from '../../../helpers/endpoint';
import swal from 'sweetalert';
import Select from 'react-select';
import { calendar } from 'ionicons/icons';
import { format, parseISO } from 'date-fns';
import './Flight_register.css';

const Flight_register: React.FC = () => {
	const [flight_name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [people, setPeople] = useState('');
	const [country_from, setCountry_from] = useState('');
	const [city_from, setCity_from] = useState('');
	const [city_to, setCity_to] = useState('');
	const [country_to, setCountry_to] = useState('');
	const [type, setType] = useState('');

	const [isNameTouched, setNameTouched] = useState<boolean>();
	const [isPriceTouched, setPriceTouched] = useState<boolean>();
	const [isPeopleTouched, setPeopleTouched] = useState<boolean>();
	const [country, setCountry] = useState([]);
	const [city, setCity] = useState([]);
	const [popoverDate2, setPopoverDate2] = useState('');
	const [temDate, setTempDate] = useState('');
	const [cityToo, setCityTo] = useState([]);

	const submit = async () => {
		console.log(popoverDate2);
		let date = new Date(temDate);
		let formatDate =
			date.getFullYear() +
			'-' +
			date.getMonth() +
			'-' +
			date.getDay() +
			' ' +
			date.getHours() +
			':' +
			date.getMinutes();
		console.log(temDate);

		let data = [
			{
				aerolinea: flight_name,
				salida: formatDate,
				id_ciudad_origen: city_from,
				id_ciudad_destino: city_to,
				precio: parseInt(price),
				capacidad: people,
				id:
					flight_name +
					date.getFullYear() +
					'-' +
					date.getMonth() +
					'-' +
					date.getDay(),
			},
		];

		console.log(data);
		try {
			if (data) {
				axios.post(`${API_CARGA_VUELOS}`, data).then((res) => {
					console.log(res);
					console.log(res.data);
					if (res.data.code == 200) {
						swal('Good job!', 'Registro exitoso!', 'success');
						return;
					}
					swal('Error!', 'Parece que hubo un error :(', 'error');
				});
			}
		} catch (e) {
			console.log(e);
			swal('Error!', 'Parece que hubo un error :(', 'error');
		}
	};

	useEffect(() => {
		// Update the document title using the browser API
		getCountrys();
	}, []);

	const getCountrys = () => {
		axios.get(`${API_HOTEL}/paises`).then((res) => {
			const data = res.data.paises;
			console.log(data);
			setCountry(data);
		});
	};

	const getCitys = (id) => {
		axios.get(`${API_HOTEL}/ciudades/${id}`).then((res) => {
			console.log('puta madre');
			console.log(res);
			const data = res.data.ciudades;
			console.log(data);
			setCity(data);
		});
	};

	const getCitysTo = (id) => {
		axios.get(`${API_HOTEL}/ciudades/${id}`).then((res) => {
			const data = res.data.ciudades;
			console.log(data);
			setCityTo(data);
		});
	};

	const validate = () => {
		return (
			flight_name !== '' &&
			price !== '' &&
			people !== '' &&
			country_from !== '' &&
			country_to !== ''
		);
	};

	const carge = () => {
		let fileUpload: any = document.getElementById('fileUpload');
		console.log(fileUpload.value);

		var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
		if (regex.test(fileUpload.value.toLowerCase())) {
			if (typeof FileReader !== 'undefined') {
				var reader = new FileReader();
				reader.onload = function (e: any) {
					//var table = document.createElement("table");
					console.log(e.target.result);
					var rows = e.target.result?.split('\r\n');
					let list_flights = [];
					console.log(rows);
					let cont = 0;
					for (let value of rows) {
						if (cont !== 0) {
							let data = value.split(';');
							if (value !== '') {
								let fligt_object = {
									nombre_viaje: data[0],
									precio: parseInt(data[1]),
									cantidad_personas: parseInt(data[2]),
									ciudad_origen: parseInt(data[3]),
									ciudad_destino: parseInt(data[4]),
									tipo: data[5],
								};
								list_flights.push(fligt_object);

								console.log(fligt_object);
							}
						}
						cont++;
					}

					send_charge(list_flights);
					//  console.log(rows[1].replace("\r","").split(";"));
				};
				reader.readAsText(fileUpload.files[0]);
			} else {
				alert('This browser does not support HTML5.');
			}
		} else {
			alert('Please upload a valid CSV file.');
		}
	};

	const handleChange3 = (e) => {
		console.log(e);
		console.log('añvv');
		getCitys(e.value);
		setCountry_from(e.value);
		setCity_from('');
	};

	const handleChange4 = (e) => {
		console.log(e.value);
		setCity_from(e.value);
	};

	const countryTo = (e) => {
		console.log(e.value);
		getCitysTo(e.value);
		setCountry_to(e.value);
		setCity_to('');
	};

	const cityTo = (e) => {
		console.log(e.value);
		setCity_to(e.value);
	};

	const formatDate = (value: string) => {
		console.log(value);
		setTempDate(value);
		return format(parseISO(value), 'MMM dd yyyy HH mm');
	};

	const send_charge = (list_flights: any) => {
		try {
			if (list_flights) {
				axios
					.post(`${API_FLIGHTS}/charge`, list_flights)
					.then((res) => {
						console.log(res);
						console.log(res.data);
						swal('Good job!', 'You clicked the button!', 'success');
					});
			}
		} catch (e) {
			console.log(e);
		}
	};

	return (
		<>
			<NavBar />
			<IonContent>
				<h1 className='title-custom title-center'>Registro</h1>
				<div className='card-container'>
					<Card className='login-card'>
						<div className='margin-login'>
							<Form>
								<Form.Group
									controlId='formFile'
									className='mb-3'>
									<Form.Label className='sub-title'>
										Carga de vuelos
									</Form.Label>
									<Form.Control id='fileUpload' type='file' />
								</Form.Group>
								<Button
									variant='primary'
									type='submit'
									onClick={(e) => {
										e.preventDefault();
										carge();
									}}>
									Registrar
								</Button>
								<Row>
									<Col sm={6}>
										<IonItem>
											<IonLabel
												color={
													flight_name === '' &&
													isNameTouched
														? 'danger'
														: 'dark'
												}
												position='floating'>
												Aereolinea *
											</IonLabel>
											<IonInput
												onClick={(e) =>
													setNameTouched(true)
												}
												onIonChange={(e) =>
													setName(e.detail.value)
												}></IonInput>
										</IonItem>
									</Col>
									<Col sm={6}>
										<IonItem>
											<IonLabel
												color={
													price === '' &&
													isPriceTouched
														? 'danger'
														: 'dark'
												}
												position='floating'>
												Precio
											</IonLabel>
											<IonInput
												onClick={(e) =>
													setPriceTouched(true)
												}
												onIonChange={(e) =>
													setPrice(e.detail.value)
												}></IonInput>
										</IonItem>
									</Col>
								</Row>

								<Row>
									<Col sm={6}>
										<IonItem>
											<IonLabel
												color={
													people === '' &&
													isPeopleTouched
														? 'danger'
														: 'dark'
												}
												position='floating'>
												Cantidad de Personas
											</IonLabel>
											<IonInput
												onClick={(e) =>
													setPeopleTouched(true)
												}
												onIonChange={(e) =>
													setPeople(e.detail.value)
												}></IonInput>
										</IonItem>
									</Col>
									<IonItem>
										<IonInput
											id='date-input-2'
											value={popoverDate2}
										/>
										<IonButton fill='clear' id='open-modal'>
											<IonIcon icon={calendar} />
										</IonButton>
									</IonItem>
									<IonModal trigger='open-modal'>
										<IonContent force-overscroll='false'>
											<div className='center-calendar'>
												<IonDatetime
													onIonChange={(ev) =>
														setPopoverDate2(
															formatDate(
																ev.detail.value!
															)
														)
													}></IonDatetime>
											</div>
										</IonContent>
									</IonModal>
								</Row>
								<div className='country-position'>
									<Row className='mb-5'>
										<Col xs={6}>
											<Select
												options={country?.map(
													(pais) => ({
														value: pais?.pais_id,
														label: pais?.nombre_pais,
													})
												)}
												className='mb-5'
												placeholder='Pais origen'
												value={country.find(
													(obj) =>
														obj.value ===
														country_from
												)}
												onChange={handleChange3}
											/>

											<Select
												options={city?.map(
													(ciudad) => ({
														value: ciudad?.ciudad_id,
														label: ciudad?.nombre_ciudad,
													})
												)}
												placeholder='Ciudad'
												value={city.find(
													(obj) =>
														obj.value === city_from
												)}
												onChange={handleChange4}
											/>
										</Col>
										<Col xs={6}>
											<Select
												options={country?.map(
													(pais) => ({
														value: pais?.pais_id,
														label: pais?.nombre_pais,
													})
												)}
												className='mb-5'
												placeholder='Pais destino'
												value={country.find(
													(obj) =>
														obj.value === country_to
												)}
												onChange={countryTo}
											/>

											<Select
												options={cityToo?.map(
													(ciudad) => ({
														value: ciudad?.ciudad_id,
														label: ciudad?.nombre_ciudad,
													})
												)}
												placeholder='Ciudad'
												value={cityToo.find(
													(obj) =>
														obj.value === city_to
												)}
												onChange={cityTo}
											/>
										</Col>
									</Row>
								</div>
								<Button
									variant='primary'
									className='button-position'
									disabled={!validate()}
									type='submit'
									onClick={(e) => {
										e.preventDefault();
										submit();
									}}>
									Registrar
								</Button>
							</Form>
						</div>
					</Card>
				</div>
			</IonContent>
		</>
	);
};

export default Flight_register;
