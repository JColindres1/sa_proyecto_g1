import { Row, Col, Container, Badge, Button, ListGroup,Table, Form } from 'react-bootstrap';
import {
	IonContent,
	IonModal,
	IonItem,
	IonLabel,
	IonList,
	IonButton,
} from '@ionic/react';
import NavBar from '../../../../../components/NavBar/NavBar';
import React, { useState, useEffect } from 'react';

import axios from 'axios';
import '../Dose/dose.css';
import Select from 'react-select';
import { BsPlusCircle, BsFillTrashFill } from 'react-icons/bs';
import { API_FLIGHTS,API_VACUNAS,API_ESQUEMA,API_ESQUEMA_SIN_S } from '../../../../../helpers/endpoint';
import swal from 'sweetalert';
import { GrFormAdd } from 'react-icons/gr';
import './Schedule.css';
const Schedule: React.FC = () => {
	const [vaccine, setvaccine] = useState([]);

	const [showModal, setShowModal] = useState(false);
    const [ShowModalSchedule, setShowModalSchedule] = useState(false);
	const [countrySelect, setcountrySelect] = useState('');
	const [vaccineSelect, setVaccineSelect] = useState('');

	const [listVaccine, setListVaccine] = useState([]);
    const [listDose,setListDose] = useState([]);
	const [country, setCountry] = useState([]);
    const [newVaccine,setnewVaccine] = useState('');
	const [data, setData] = useState({
        id_vacuna:'',
        nombre_vacuna:''
	});

    const [tempDosis, setTempDosis] = useState([]);

    const [currentElement, setCurrent] = useState<any>();

    const [listSchedules,setListSchedules] = useState([]);

    const [vacc,setVacc] = useState('')
	const clickHandle = () => {
		setShowModal(true);
	};

    const clickHandleSchedule = (value) => {
        setCurrent(value)
		setShowModalSchedule(true);
	};

	useEffect(() => {
		//getCountrys();
        getVaccines();
        getDose();
        getSchedules();
	}, []);

    const getVaccines = () =>{
        axios.get(`${API_VACUNAS}`).then((res)=>{
            console.log(res);
            setListVaccine(res.data)
        })
    }

    const getDose = () =>{
        axios.get(`${API_VACUNAS}/dosis`).then((res)=>{
            console.log("dosis:")
            console.log(res);
            setListDose(res.data)
        })
    }

    const getSchedules = () =>{
        axios.get(`${API_ESQUEMA}`).then((res)=>{
            console.log(res.data);
            setListSchedules(res.data);
        })
    }

	const getVaccine = (id_con) => {
		console.log(countrySelect);
		axios.get(`${API_FLIGHTS}/vaccination/${id_con}`).then((res) => {
			const data = res.data.data;
			console.log(data);
			setvaccine(data);
		});
	};

    const handleAdd = (todo) => {
        const newTodos = [...tempDosis];
        newTodos.push(todo);
        setTempDosis(newTodos);
      }
      

	const getCountrys = () => {
		axios.get(`${API_FLIGHTS}/country`).then((res) => {
			const data = res.data.data;
			console.log(data);
			setCountry(data);
		});
	};
	const handleChange3 = (e) => {
		console.log(e.id);
		setcountrySelect(e.id);
		getVaccine(e.id);
	};

	const addVaccine = () => {
	   console.log(newVaccine);
       console.log(vacc)
           axios.post(`${API_VACUNAS}/dosis`,{tipo:newVaccine,id_vacuna:vacc}).then((res)=>{
                console.log(res);
           })
	};

    

	// const myFligths = () => {
	// 	let x = 1;
	// 	const listItems = vaccine.map((value) => (
	// 		<IonItem>
	// 			<IonLabel>
	// 				<h2>
	// 					<strong>Esquema {x++} : </strong>
	// 				</h2>
	// 				<div className='information'>{Esquemas()}</div>
	// 			</IonLabel>
	// 		</IonItem>
	// 	));
	// 	return listItems;
	// };

	const handleChangeVaccine = (e) => {
		console.log(e.value);

		setVaccineSelect(e.value);
	};
    
    //let itemsAP : any;
   const  itemsAP = tempDosis.map((value) => (
        <>
            <span className='span-position'>
                <Badge bg='dark'>{value.nombre_vacuna}</Badge>
            </span>
        </>
    ));
    const prueba = () => {
		// itemsAP = tempDosis.map((value) => (
		// 	<>
		// 		<span className='span-position'>
		// 			<Badge bg='dark'>{value.nombre_vacuna}</Badge>
		// 		</span>
		// 	</>
		// ));
    }

    const addTemp = (value) => {
        
        tempDosis.push(value);
        console.log(tempDosis);

		setTempDosis(tempDosis);
        handleChangeData(1);
    }

	const deleteVaccine = () => {
		setListVaccine([]);
	};

	const sendSchedule = () => {
		let data = {
			id_pais: countrySelect,
			vacunas: listVaccine,
		};
		axios.post(`${API_FLIGHTS}/vaccination`, data).then((res) => {
			console.log(res);
			console.log(res.data);
			getVaccine(countrySelect);
		});
		console.log(data);
	};

	const listItems = listVaccine.map((value) => (
		<ListGroup as='ol' numbered>
			<ListGroup.Item as='li'>{value}</ListGroup.Item>
		</ListGroup>
	));

    const listItemSchedule = listSchedules.map((value) =>(
        <tr>
        <td>{value.id_esquema}</td>
        <td>{value.nombre_esquema}</td>
        <td><Button onClick={(e) => clickHandleSchedule(value)}><GrFormAdd/></Button></td>
      </tr>        
    ))


    const listItemDose = listDose.map((value) =>(
        <tr>
        <td>{value.id_dosis}</td>
        <td>{value.nombre_vacuna}</td>
        <td>{value.tipo}</td>
        <td><Button onClick={() => handleAdd(value)}><GrFormAdd/></Button></td>
      </tr>        
    ))

   const handleChangeData = (e: any) =>{
       console.log(e.target.value);
       setnewVaccine(e.target.value);
   }

   const sendData = async () => {
       let data = {

       }

     let ans = await axios.post(`${API_ESQUEMA_SIN_S}/registrar`,{nombre:newVaccine})
	 console.log(ans)
     let response = await  axios.get(`${API_ESQUEMA}`);
     console.log(response.data);
     let esquemas = response.data;
     let current_esquema = esquemas[esquemas.length - 1]
     console.log(current_esquema.id_esquema);
     console.log(tempDosis);
     for(let value of tempDosis){
         let data_schedule = {
             id_esquema:current_esquema.id_esquema,
             id_dosis:value.id_dosis
         }
         let response_schedule = await axios.post(`${API_ESQUEMA_SIN_S}/dosis`,data_schedule)
         
         
     }
     //await axios.post(`http://104.154.167.229:4000/esb/esquema/dosis`,{})
   }

	return (
        
		<>
		<div className='prueba'>
				<IonModal
					isOpen={showModal}
					swipeToClose={true}
					//  presentingElement={router || undefined}
				>
					<Container>
                    <IonItem>
				<IonLabel>
					<h2>
						<strong>Esquema</strong>
					</h2>
					<div className='information'> {tempDosis.map((value) => (
        <>
            <span className='span-position'>
                <Badge bg='dark'>{value.nombre_vacuna}</Badge>
            </span>
        </>
    ))}</div>
				</IonLabel>
			</IonItem>
                        <Form>
                        <Form.Group controlId="Name">
                                    <Form.Label className="form-text">Tipo</Form.Label>
                                    < Form.Control onChange={handleChangeData}/>
                                    <Button onClick={()=>sendData()}>Add</Button>
                            
                                </Form.Group>
						<Row className='mb-5'>
                        <Table striped bordered hover size="sm">
    <thead>
    <tr>
      <th>ID</th>
      <th>Nombre Vacuna</th>
      <th>Tipo</th>
    </tr>
  </thead>
  <tbody>
     {listItemDose}
  </tbody>
</Table>
                      
						</Row>
                        </Form>
						<Row className='mb-5'>
							<Col sm={5}>
							
							</Col>
						</Row>
						<IonButton onClick={(e) => addVaccine()}>
							Registrar Esquema
						</IonButton>
						<IonButton onClick={(e) => setShowModal(false)}>
							Close
						</IonButton>
					</Container>
				</IonModal>
				</div>

                {/* <IonModal
					isOpen={ShowModalSchedule}
					swipeToClose={true}
					//  presentingElement={router || undefined}
				>
					<Container>
                        <Form>
						<Row className='mb-5'>
                        <Select
						className='basic-single'
						classNamePrefix='select'
						isClearable
						isSearchable
						name='pais'
						value={{
							value: data?.id_vacuna,
							label: data?.nombre_vacuna,
						}}
						onChange={(vaccine) => {
							setVacc(vaccine.value);
							setData({
								...data,
								id_vacuna: vaccine.value,
								nombre_vacuna: vaccine.label,
							})
						}}
						options={listVaccine?.map((vaccine) => ({
							value: vaccine?.id_vacuna,
							label: vaccine?.nombre_vacuna,
						}))}
					/>
                        <Form.Group controlId="Name">
                                    <Form.Label className="form-text">Tipo</Form.Label>
                                    < Form.Control onChange={handleChangeData}/>
                                </Form.Group>
						</Row>
                        </Form>
						<Row className='mb-5'>
							<Col sm={5}>
							
							</Col>
						</Row>
						<IonButton onClick={(e) => addVaccine()}>
							Registrar Esquema
						</IonButton>
						<IonButton onClick={(e) => setShowModal(false)}>
							Close
						</IonButton>
					</Container>
				</IonModal> */}


			<IonContent>
				<NavBar />
           <Container>
             <Button className="button__add_vaccine" onClick={(e) => clickHandle()}>Agregar Esquema</Button>
                <Table striped bordered hover size="sm">
    <thead>
    <tr>
      <th>ID</th>
      <th>Nombre Vacuna</th>
      <th>Tipo</th>
    </tr>
  </thead>
  <tbody>
     {listItemSchedule}
  </tbody>
</Table>
</Container>
                {/* <Row>
                <Col sm={4}>
                <Button>Agregar Vacuna</Button>
                </Col>
                <Col sm={4}>
                <Button>Agregar Dosis</Button>
                </Col>
                <Col sm={4}>
                <Button>Agregar Esquema</Button>
                </Col>
                </Row> */}
				
			</IonContent>
		</>
	);
};

export default Schedule;

var vacunas = [
	{ label: 'sputnik', value: 'sputnik' },
	{ label: 'astrazeneca', value: 'astrazeneca' },
	{ label: 'moderna', value: 'moderna' },
];
