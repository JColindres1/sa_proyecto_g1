import { Row, Col, Container, Badge, Button, ListGroup,Table, Form } from 'react-bootstrap';
import {
	IonContent,
	IonModal,
	IonItem,
	IonLabel,
	IonList,
	IonButton,
} from '@ionic/react';
import NavBar from '../../../../components/NavBar/NavBar';
import React, { useState, useEffect } from 'react';

import axios from 'axios';
import './Vaccination.css';
import Select from 'react-select';
import { BsPlusCircle, BsFillTrashFill } from 'react-icons/bs';
import { API_FLIGHTS, API_VACUNA, API_VACUNAS} from '../../../../helpers/endpoint';
import swal from 'sweetalert';

const VaccinationModule: React.FC = () => {
	const [vaccine, setvaccine] = useState([]);

	const [showModal, setShowModal] = useState(false);
	const [countrySelect, setcountrySelect] = useState('');
	const [vaccineSelect, setVaccineSelect] = useState('');

	const [listVaccine, setListVaccine] = useState([]);
	const [country, setCountry] = useState([]);
    const [newVaccine,setnewVaccine] = useState('');

	const clickHandle = () => {
		setShowModal(true);
	};

	useEffect(() => {
		getCountrys();
        getVaccines();
	}, []);

    const getVaccines = () =>{
        axios.get(`${API_VACUNAS}`).then((res)=>{
            console.log(res);
            setListVaccine(res.data)
        })
    }
	const getVaccine = (id_con) => {
		console.log(countrySelect);
		axios.get(`${API_FLIGHTS}/vaccination/${id_con}`).then((res) => {
			const data = res.data.data;
			console.log(data);
			setvaccine(data);
		});
	};

	const getCountrys = () => {
		axios.get(`${API_FLIGHTS}/country`).then((res) => {
			const data = res.data.data;
			console.log(data);
			setCountry(data);
		});
	};
	const handleChange3 = (e) => {
		console.log(e.id);
		setcountrySelect(e.id);
		getVaccine(e.id);
	};

	const addVaccine = () => {
	       axios.post(`${API_VACUNA}/registrar`,{nombre_vacuna:newVaccine}).then((res)=>{
                console.log(res);
           })
	};

	const myFligths = () => {
		let x = 1;
		const listItems = vaccine.map((value) => (
			<IonItem>
				<IonLabel>
					<h2>
						<strong>Esquema {x++} : </strong>
					</h2>
					<div className='information'>{Esquemas(value)}</div>
				</IonLabel>
			</IonItem>
		));
		return listItems;
	};

	const handleChangeVaccine = (e) => {
		console.log(e.value);

		setVaccineSelect(e.value);
	};

	const Esquemas = (values) => {
		const listItems = values.map((value) => (
			<>
				<span className='span-position'>
					<Badge bg='dark'>{value.vacuna}</Badge>
				</span>
			</>
		));
		return listItems;
	};

	const deleteVaccine = () => {
		setListVaccine([]);
	};

	const sendSchedule = () => {
		let data = {
			id_pais: countrySelect,
			vacunas: listVaccine,
		};
		axios.post(`${API_FLIGHTS}/vaccination`, data).then((res) => {
			console.log(res);
			console.log(res.data);
			getVaccine(countrySelect);
		});
		console.log(data);
	};

	const listItems = listVaccine.map((value) => (
		<ListGroup as='ol' numbered>
			<ListGroup.Item as='li'>{value}</ListGroup.Item>
		</ListGroup>
	));

    const listItemVaccine = listVaccine.map((value) =>(
        <tr>
        <td>{value.id_vacuna}</td>
        <td>{value.nombre_vacuna}</td>
      </tr>        
    ))

   const handleChangeData = (e: any) =>{
       console.log(e.target.value);
       setnewVaccine(e.target.value);
   }

	return (
		<>
				<IonModal
					isOpen={showModal}
					swipeToClose={true}
					//  presentingElement={router || undefined}
				>
					<Container>
                        <Form>
						<Row className='mb-5'>
                        <Form.Group controlId="Name">
                                    <Form.Label className="form-text">Nombre Completo</Form.Label>
                                    < Form.Control onChange={handleChangeData}/>
                                </Form.Group>
						</Row>
                        </Form>
						<Row className='mb-5'>
							<Col sm={5}>
							
							</Col>
						</Row>
						<IonButton onClick={(e) => addVaccine()}>
							Registrar Esquema
						</IonButton>
						<IonButton onClick={(e) => setShowModal(false)}>
							Close
						</IonButton>
					</Container>
				</IonModal>


			<IonContent>
				<NavBar />
           <Container>
             <Button className="button__add_vaccine" onClick={(e) => clickHandle()}>Agregar Vacuna</Button>
                <Table striped bordered hover size="sm">
    <thead>
    <tr>
      <th>ID</th>
      <th>Nombre Vacuna</th>
    </tr>
  </thead>
  <tbody>
     {listItemVaccine}
  </tbody>
</Table>
</Container>
                {/* <Row>
                <Col sm={4}>
                <Button>Agregar Vacuna</Button>
                </Col>
                <Col sm={4}>
                <Button>Agregar Dosis</Button>
                </Col>
                <Col sm={4}>
                <Button>Agregar Esquema</Button>
                </Col>
                </Row> */}
				
			</IonContent>
		</>
	);
};

export default VaccinationModule;

var vacunas = [
	{ label: 'sputnik', value: 'sputnik' },
	{ label: 'astrazeneca', value: 'astrazeneca' },
	{ label: 'moderna', value: 'moderna' },
];
