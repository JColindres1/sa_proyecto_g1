import { Row, Col, Container, Badge, Button, ListGroup,Table, Form } from 'react-bootstrap';
import {
	IonContent,
	IonModal,
	IonItem,
	IonLabel,
	IonList,
	IonButton,
} from '@ionic/react';
import NavBar from '../../../../../components/NavBar/NavBar';
import React, { useState, useEffect } from 'react';

import axios from 'axios';
// import './Vaccination.css';
import Select from 'react-select';
import { BsPlusCircle, BsFillTrashFill } from 'react-icons/bs';
import { API_FLIGHTS, API_VACUNA, API_VACUNAS,API_HOTEL,API_ESQUEMA,API_ESQUEMA_SIN_S } from '../../../../../helpers/endpoint';
import swal from 'sweetalert';

const VaccPais: React.FC = () => {
	const [vaccine, setvaccine] = useState([]);

	const [showModal, setShowModal] = useState(false);
	const [countrySelect, setcountrySelect] = useState('');
	const [vaccineSelect, setVaccineSelect] = useState('');

	const [listVaccine, setListVaccine] = useState([]);
	const [country, setCountry] = useState([]);
    const [newVaccine,setnewVaccine] = useState('');
	const [data, setData] = useState({
		idpais: 0,
		nombre_pais: '',
        id_esquema: 0,
        nombre_esquema: ''
	});

    const [pais, setPais] = useState(0);
	const [paises, setPaises] = useState([]);
    const [object,setObject] = useState();
    const [dos,setDos] = useState([]);
	const clickHandle = async () => {
		console.log(data)
        let info = {
            id_esquema:data.id_esquema,
            id_pais:data.idpais
        }
       let response =  axios.post(`${API_ESQUEMA_SIN_S}/pais`,info)
       console.log(response)
	};

	useEffect(() => {
	
        getVaccines();
	}, []);

    useEffect(() => {

		axios
			.get(`${API_HOTEL}/paises`)
			.then((res) => res.data)
			.then((res) => {
				if (res.success) {
					setPaises(res.paises);
				} else {
					setPaises([]);
				}
			
			})
			.catch((e) => {
				
				console.log(e);
				setPais(0);
			});
	}, []);

    const getVaccines = () =>{
        axios.get(`${API_ESQUEMA}`).then((res)=>{
            console.log(res);
            setListVaccine(res.data)
        })
    }
	const getVaccine = (id_con) => {
		console.log(countrySelect);
		axios.get(`${API_FLIGHTS}/vaccination/${id_con}`).then((res) => {
			const data = res.data.data;
			console.log(data);
			setvaccine(data);
		});
	};

	const getCountrys = () => {
		axios.get(`${API_HOTEL}/paises`).then((res) => {
			const data = res.data.paises;
			console.log(data);
			setCountry(data);
		});
	};
	const handleChange3 = (e) => {
		console.log(e.id);
		setcountrySelect(e.id);
		getVaccine(e.id);
	};

	const addVaccine = () => {
	       axios.post(`${API_VACUNA}/registrar`,{nombre_vacuna:newVaccine}).then((res)=>{
                console.log(res);
           })
	};

	const myFligths = () => {
		let x = 1;
		const listItems = vaccine.map((value) => (
			<IonItem>
				<IonLabel>
					<h2>
						<strong>Esquema {x++} : </strong>
					</h2>
					<div className='information'>{Esquemas(value)}</div>
				</IonLabel>
			</IonItem>
		));
		return listItems;
	};

	const handleChangeVaccine = (e) => {
		console.log(e.value);

		setVaccineSelect(e.value);
	};

	const Esquemas = (values) => {
		const listItems = values.map((value) => (
			<>
				<span className='span-position'>
					<Badge bg='dark'>{value.vacuna}</Badge>
				</span>
			</>
		));
		return listItems;
	};

	const deleteVaccine = () => {
		setListVaccine([]);
	};

	const sendSchedule = () => {
		let data = {
			id_pais: countrySelect,
			vacunas: listVaccine,
		};
		axios.post(`${API_FLIGHTS}/vaccination`, data).then((res) => {
			console.log(res);
			console.log(res.data);
			getVaccine(countrySelect);
		});
		console.log(data);
	};

    const handleShow = async (value) =>{
        console.log(value.id_esquema)
        setObject(value);
       let response = await axios.get(`${API_ESQUEMA_SIN_S}/dosis/`+value.id_esquema)
       console.log(response.data)
       setDos(response.data); 
       setShowModal(true);
    }

	const listItems = listVaccine.map((value) => (
		<ListGroup as='ol' numbered>
			<ListGroup.Item as='li'>{value}</ListGroup.Item>
		</ListGroup>
	));

    const listItemVaccine = listVaccine.map((value) =>(
        <tr>
        <td>{value.id_esquema}</td>
        <td>{value.nombre_esquema}</td>
        <td><Button onClick={() => handleShow(value)}>Ver</Button></td>
      </tr>        
    ))


    const listItems2 = dos.map((value) =>(
        <tr>
        <td>{value.id_dosis}</td>
        <td>{value.nombre_vacuna}</td>
      </tr>        
    ))

   const handleChangeData = (e: any) =>{
       console.log(e.target.value);
       setnewVaccine(e.target.value);
   }

	return (
		<>
				<IonModal
					isOpen={showModal}
					swipeToClose={true}
					//  presentingElement={router || undefined}
				>
					<Container>
                      <Table striped bordered hover size="sm">
    <thead>
    <tr>
      <th>ID</th>
      <th>Nombre Vacuna</th>
    </tr>
  </thead>
  <tbody>
     {listItems2}
  </tbody>
</Table>
<IonButton onClick={(e) => setShowModal(false)}>
							Close
						</IonButton>
					</Container>
				</IonModal>


			<IonContent>
				<NavBar />
           <Container>
               <label>País: </label>
           <Select
						className='basic-single'
						classNamePrefix='select'
						isClearable
						isSearchable
						name='pais'
						value={{
							value: data?.idpais,
							label: data?.nombre_pais,
						}}
						onChange={(pais) => {
							setPais(pais.value);
							setData({
								...data,
								idpais: pais.value,
								nombre_pais: pais.label,
							})
						}}
						options={paises?.map((pais) => ({
							value: pais?.pais_id,
							label: pais?.nombre_pais,
						}))}
					/>
   <label>Esquema: </label>
          <Select
						className='basic-single'
						classNamePrefix='select'
						isClearable
						isSearchable
						name='pais'
						value={{
							value: data?.id_esquema,
							label: data?.nombre_esquema,
						}}
						onChange={(pais) => {
							
							setData({
								...data,
								id_esquema: pais.value,
								nombre_esquema: pais.label,
							})
						}}
						options={listVaccine?.map((value) => ({
							value: value?.id_esquema,
							label: value?.nombre_esquema,
						}))}
					/>      
             <Button className="button__add_vaccine" onClick={(e) => clickHandle()}>Asignar esquema</Button>
                <Table striped bordered hover size="sm">
    <thead>
    <tr>
      <th>ID</th>
      <th>Nombre Vacuna</th>
    </tr>
  </thead>
  <tbody>
     {listItemVaccine}
  </tbody>
</Table>
</Container>
                {/* <Row>
                <Col sm={4}>
                <Button>Agregar Vacuna</Button>
                </Col>
                <Col sm={4}>
                <Button>Agregar Dosis</Button>
                </Col>
                <Col sm={4}>
                <Button>Agregar Esquema</Button>
                </Col>
                </Row> */}
				
			</IonContent>
		</>
	);
};

export default VaccPais;

var vacunas = [
	{ label: 'sputnik', value: 'sputnik' },
	{ label: 'astrazeneca', value: 'astrazeneca' },
	{ label: 'moderna', value: 'moderna' },
];
