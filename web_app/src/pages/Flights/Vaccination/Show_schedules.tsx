import { Row, Col, Container, Badge, Button, ListGroup } from 'react-bootstrap';
import {
	IonContent,
	IonModal,
	IonItem,
	IonLabel,
	IonList,
	IonButton,
} from '@ionic/react';
import NavBar from '../../../components/NavBar/NavBar';
import React, { useState, useEffect } from 'react';

import axios from 'axios';
//import './show_schedules.css';
import Select from 'react-select';
import { BsPlusCircle, BsFillTrashFill } from 'react-icons/bs';
import { API_FLIGHTS } from '../../../helpers/endpoint';
import swal from 'sweetalert';

const Show_Schedules: React.FC = () => {
	const [vaccine, setvaccine] = useState([]);

	const [showModal, setShowModal] = useState(false);
	const [countrySelect, setcountrySelect] = useState('');
	const [vaccineSelect, setVaccineSelect] = useState('');

	const [listVaccine, setListVaccine] = useState([]);
	const [country, setCountry] = useState([]);
	const clickHandle = () => {
		setShowModal(true);
	};

	useEffect(() => {
		getCountrys();
	}, []);

	const getVaccine = (id_con) => {
		console.log(countrySelect);
		axios.get(`${API_FLIGHTS}/vaccination/${id_con}`).then((res) => {
			const data = res.data.data;
			console.log(data);
			setvaccine(data);
		});
	};

	const getCountrys = () => {
		axios.get(`${API_FLIGHTS}/country`).then((res) => {
			const data = res.data.data;
			console.log(data);
			setCountry(data);
		});
	};
	const handleChange3 = (e) => {
		console.log(e.id);
		setcountrySelect(e.id);
		getVaccine(e.id);
	};
    

	const addVaccine = () => {
		if (listVaccine.length === 3) {
			swal(
				'Error',
				'No se puede ingresar un esquema con más de 3 vacunas!',
				'error'
			);
			return;
		}
		listVaccine.push(vaccineSelect);
		console.log(listVaccine);
		setListVaccine(listVaccine);
		handleChangeVaccine(3);
	};

	const myFligths = () => {
		let x = 1;
		const listItems = vaccine.map((value) => (
			<IonItem>
				<IonLabel>
					<h2>
						<strong>Esquema {x++} : </strong>
					</h2>
					<div className='information'>{Esquemas(value)}</div>
				</IonLabel>
			</IonItem>
		));
		return listItems;
	};

	const handleChangeVaccine = (e) => {
		console.log(e.value);

		setVaccineSelect(e.value);
	};

	const Esquemas = (values) => {
		const listItems = values.map((value) => (
			<>
				<span className='span-position'>
					<Badge bg='dark'>{value.vacuna}</Badge>
				</span>
			</>
		));
		return listItems;
	};

	const deleteVaccine = () => {
		setListVaccine([]);
	};

	const sendSchedule = () => {
		let data = {
			id_pais: countrySelect,
			vacunas: listVaccine,
		};
		axios.post(`${API_FLIGHTS}/vaccination`, data).then((res) => {
			console.log(res);
			console.log(res.data);
			getVaccine(countrySelect);
		});
		console.log(data);
	};

	const listItems = listVaccine.map((value) => (
		<ListGroup as='ol' numbered>
			<ListGroup.Item as='li'>{value}</ListGroup.Item>
		</ListGroup>
	));

	return (
		<>
			<IonContent>
				<NavBar />

				<IonModal
					isOpen={showModal}
					swipeToClose={true}
					//  presentingElement={router || undefined}
				>
					<Container>
						<Row className='mb-5'>
							<h1>Esquemas de vacunación </h1>
						</Row>
						<Row className='mb-5'>
							<Col sm={5}>
								<Select
									options={vacunas}
									className=''
									placeholder='Pais origen'
									value={vacunas.find(
										(obj) => obj.value === vaccineSelect
									)}
									onChange={handleChangeVaccine}
								/>
							</Col>
							<Col sm={3}>
								<Button onClick={(e) => addVaccine()}>
									<BsPlusCircle />
								</Button>
								<Button
									variant='danger'
									onClick={(e) => deleteVaccine()}>
									<BsFillTrashFill />
								</Button>
							</Col>
						</Row>
						<Row className='mb-5'>{listItems}</Row>
						<IonButton onClick={(e) => sendSchedule()}>
							Registrar Esquema
						</IonButton>
						<IonButton onClick={(e) => setShowModal(false)}>
							Close
						</IonButton>
					</Container>
				</IonModal>

				<div className='text-center'>
					<Row className='mb-5'>
						<Col sm={12}>
							<h1 className='title-custom'>
								Esquemas de vacunación
							</h1>
						</Col>
					</Row>
					<div className='list-size'>
						<Row className='mb-5'>
							<Col sm={3}>
								<Select
									options={country}
									className='select-size'
									placeholder='Pais origen'
									value={country.find(
										(obj) => obj.value === countrySelect
									)}
									onChange={handleChange3}
								/>
							</Col>
							<Col sm={3}>
								<Button
									variant='info'
									onClick={(e) => clickHandle()}>
									<BsPlusCircle />
								</Button>
							</Col>
						</Row>

						<IonList>{myFligths()}</IonList>
					</div>
				</div>
			</IonContent>
		</>
	);
};

export default Show_Schedules;

var vacunas = [
	{ label: 'sputnik', value: 'sputnik' },
	{ label: 'astrazeneca', value: 'astrazeneca' },
	{ label: 'moderna', value: 'moderna' },
];
