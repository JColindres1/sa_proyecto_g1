import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

import Home from './pages/Home/Home';
import Login from './pages/Login/Login';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';
import 'bootstrap/dist/css/bootstrap.min.css';
/* Theme variables */
import './theme/variables.css';
import './theme/custom.scss';
import Register from './pages/Register/Register';
import Flight_register from './pages/Flights/Register/Flight_register';
import Show_flight from './pages/Flights/Show/Show_flight';
import axios from 'axios';
import Car_register from './pages/Cars/Register/Car_register';
//import Menu_register from './pages/Cars/Register/Menu_register';
import RegisterRoom from './pages/Rooms/Register/RegisterRoom';
import RoomsScreen from './pages/Rooms/RoomsScreen/RoomsScreen';
import Show_Schedules from './pages/Flights/Vaccination/Show_schedules';
import Menu_register from './pages/Cars/Show/Menu_register';
import Car_register_file from './pages/Cars/Register/Car_register_file';
import Show_cars from './pages/Cars/Show/Show_cars';
import InicioT from './pages/Turista/InicioT';
import EsquemaT from './pages/Turista/EsquemaT';
import HomeA from './pages/Admin/HomeA';
import UpdateRoom from './pages/Rooms/Update/UpdateRoom';
import ViewRoom from './pages/Rooms/View/ViewRoom';
import RoomCalendar from './pages/Rooms/RoomCalendar/RoomCalendar';
import Cars_turista from './pages/Turista/Show_cars/Cars_turista';
import Cars_reservacion from './pages/Turista/Show_cars/Cars_reservacion';
import Mis_reservaciones from './pages/Turista/Show_cars/Mis_reservaciones';
import Tourist_profile from './pages/Turista/Profile/Profile';
import HotelRoute from './validators/HotelRoute';
import AdminRoute from './validators/AdminRoute';
import TuristaRoute from './validators/TuristaRoute';
import CarrosRoute from './validators/CarrosRoute';
import Reservaciones from './pages/Turista/Reservaciones/Reservaciones';
import Results from './pages/Results/Results';
import VaccinationModule from './pages/Flights/Vaccination/VaccinationModule/VaccinationModule';
import Dose from './pages/Flights/Vaccination/VaccinationModule/Dose/Dose';
import Schedule from './pages/Flights/Vaccination/VaccinationModule/Schedule/Schedule';
import VaccPais from './pages/Flights/Vaccination/VaccinationModule/VacPais/VaccPais';
setupIonicReact();

//esto servira para mandar el token de autorización
axios.interceptors.request.use((request: any) => {
	console.log(request.url.includes('/users/paises'));
	let item = localStorage.getItem('bearer');
	console.log(item);
	if (item && !request.url.includes('/users/paises')) {
		request.headers.common.Authentication = `${item}`;
	}
	return request;
});

const App: React.FC = () => (
	<div className='background-blue'>
		<IonApp>
			<IonReactRouter>
				<IonRouterOutlet>
					<Route exact path='/home' component={Home} />
					<Route exact path='/login' component={Login} />
					<Route exact path='/register' component={Register} />
					<Route exact path='/show_flight' component={Show_flight} />
					<Route exact path='/rooms' component={RoomsScreen} />
					<Route
						exact
						path='/rooms/view/:habitacion_id'
						component={ViewRoom}
					/>
					<Route
						exact
						path='/rooms/calendar/:habitacion_id'
						component={RoomCalendar}
					/>
					<Route exact path='/'>
						<Redirect to='/home' />
					</Route>

					<TuristaRoute
						exact
						path='/turista/home'
						component={InicioT}
						children={null}
					/>
					<TuristaRoute
						exact
						path='/turista/mis/reservaciones/autos'
						component={Mis_reservaciones}
						children={null}
					/>
					<TuristaRoute
						exact
						path='/turista/esquema'
						component={EsquemaT}
						children={null}
					/>
					<TuristaRoute
						exact
						path='/turista/perfil'
						component={Tourist_profile}
						children={null}
					/>
					<TuristaRoute
						exact
						path='/turista/reservacion/autos'
						component={Cars_reservacion}
						children={null}
					/>
					<TuristaRoute
						exact
						path='/turista/autos'
						component={Cars_turista}
						children={null}
					/>
					<TuristaRoute
						exact
						path='/turista/reservaciones'
						component={Reservaciones}
						children={null}
					/>

					<HotelRoute
						exact
						path='/rooms/register'
						component={RegisterRoom}
						children={null}
					/>
					<HotelRoute
						exact
						path='/rooms/update/:habitacion_id'
						component={UpdateRoom}
						children={null}
					/>

					<AdminRoute
						exact
						path='/admin/home'
						component={HomeA}
						children={null}
					/>
					<AdminRoute
						exact
						path='/schedules'
						component={Show_Schedules}
						children={null}
					/>
					<AdminRoute
						exact
						path='/flight_register'
						component={Flight_register}
						children={null}
					/>
					<AdminRoute
						exact
						path='/results'
						component={Results}
						children={null}
					/>

					<AdminRoute
						exact
						path='/vaccination'
						component={VaccinationModule}
						children={null}
					/>

					<AdminRoute
						exact
						path='/dosis'
						component={Dose}
						children={null}
					/>

					<AdminRoute
						exact
						path='/schedule'
						component={Schedule}
						children={null}
					/>

					<AdminRoute
						exact
						path='/vacpais'
						component={VaccPais}
						children={null}
					/>

					<CarrosRoute
						exact
						path='/cars/register_file'
						component={Car_register_file}
						children={null}
					/>
					<CarrosRoute
						exact
						path='/cars/all_cars'
						component={Show_cars}
						children={null}
					/>
					<CarrosRoute
						exact
						path='/cars/register'
						component={Car_register}
						children={null}
					/>
					<CarrosRoute
						exact
						path='/cars/home'
						component={Menu_register}
						children={null}
					/>
				</IonRouterOutlet>
			</IonReactRouter>
		</IonApp>
	</div>
);

export default App;
