import React, { useState, useEffect } from 'react';
import { Col, Row, Form, Button } from 'react-bootstrap';
import Select from 'react-select';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { API_HOTEL } from '../../helpers/endpoint';
export default function RoomForm(props: any) {
	console.log(props);
	const history = useHistory();
	const [data, setData] = useState({
		nombre: '',
		precio: 0,
		cantidad_personas: 0,
		descripcion: '',
		estado: 1,
		usuario_id: props?.user?.id_usuario,
		ciudad_id: 0,
		pais_id: 0,
		nombre_pais: '',
		nombre_ciudad: '',
		habitacion_id: null,
	});
	const [loadingCountry, setLoadingCountry] = useState(false);
	const [pais, setPais] = useState(0);
	const [paises, setPaises] = useState([]);
	const [loadingCity, setLoadingCity] = useState(true);
	const [ciudades, setCiudades] = useState([]);

	useEffect(() => {
		props?.action === 'update' && setData({ ...props?.room });
		props?.action === 'update' && setPais(props?.room?.pais_id);
	}, [props.room, props?.action]);

	useEffect(() => {
		setLoadingCountry(true);
		axios
			.get(`${API_HOTEL}/paises`)
			.then((res) => res.data)
			.then((res) => {
				if (res.success) {
					setPaises(res.paises);
				} else {
					setPaises([]);
				}
				setLoadingCountry(false);
			})
			.catch((e) => {
				setLoadingCountry(false);
				console.log(e);
				setPais(0);
			});
	}, []);

	useEffect(() => {
		if (pais) {
			setLoadingCity(true);
			axios
				.get(`${API_HOTEL}/ciudades/${pais}`)
				.then((res) => res.data)
				.then((res) => {
					if (res.success) {
						setCiudades(res.ciudades);
					} else {
						setCiudades([]);
					}
					setLoadingCity(false);
				});
		}
	}, [pais]);

	const handleChangeData = (e: any) =>
		setData({ ...data, [e.target.name]: e.target.value });
	console.log(data);
	const handleSubmit = (e: any) => {
		e.preventDefault();
		if (props?.action === 'create')
			axios
				.post(`${API_HOTEL}`, data)
				.then((res) => res.data)
				.then((res) => {
					if (res.success) {
						history.push(`/rooms/calendar/${res.habitacion}`);
					}
				})
				.catch(handleError);
		else if (props?.action === 'update')
			axios
				.put(`${API_HOTEL}`, data)
				.then((res) => res.data)
				.then((res) => {
					if (res.success) {
						history.push(`/rooms/calendar/${data?.habitacion_id}`);
					}
				})
				.catch(handleError);
	};

	const handleError = (error: any) => {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error.response.data);
			console.log(error.response.status);
			console.log(error.response.headers);
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error.message);
		}
		console.log(error.config);
	};

	return (
		<Form onSubmit={handleSubmit}>
			<Row className='mb-3'>
				<Form.Group
					as={Col}
					controlId='formName'
					className='text-start'
					sm={12}
					md={6}
					lg={4}>
					<Form.Label>Nombre</Form.Label>
					<Form.Control
						type='text'
						required
						name='nombre'
						value={data.nombre}
						onChange={handleChangeData}
						placeholder='Ingrese el nombre'
					/>
				</Form.Group>
				<Form.Group
					as={Col}
					controlId='formPrecio'
					className='text-start'
					sm={12}
					md={6}
					lg={4}>
					<Form.Label>Precio</Form.Label>
					<Form.Control
						type='number'
						required
						name='precio'
						value={data.precio}
						onChange={handleChangeData}
						placeholder='Ingrese el precio'
					/>
				</Form.Group>
				<Form.Group
					as={Col}
					controlId='formPersonas'
					className='text-start'
					sm={12}
					md={6}
					lg={4}>
					<Form.Label>Cantidad de Personas</Form.Label>
					<Form.Control
						type='number'
						required
						name='cantidad_personas'
						value={data.cantidad_personas}
						onChange={handleChangeData}
						placeholder='Ingrese cantidad de personas máximo'
					/>
				</Form.Group>
			</Row>
			<Row className='mb-3'>
				<Form.Group
					as={Col}
					controlId='formDescrption'
					className='text-start'>
					<Form.Label>Descripción</Form.Label>
					<Form.Control
						as='textarea'
						rows={3}
						required
						name='descripcion'
						value={data.descripcion}
						onChange={handleChangeData}
						placeholder='Ingrese una descripción'
					/>
				</Form.Group>
			</Row>
			<Row className='mb-3'>
				<Form.Group
					as={Col}
					controlId='formDescrption'
					className='text-start'
					sm={12}
					md={6}>
					<Form.Label>Pais</Form.Label>
					<Select
						className='basic-single'
						classNamePrefix='select'
						isLoading={loadingCountry}
						isClearable
						isSearchable
						name='pais'
						value={{
							value: data?.pais_id,
							label: data?.nombre_pais,
						}}
						onChange={(pais) => {
							setPais(pais.value);
							setData({
								...data,
								pais_id: pais.value,
								nombre_pais: pais.label,
							});
						}}
						options={paises?.map((pais) => ({
							value: pais?.pais_id,
							label: pais?.nombre_pais,
						}))}
					/>
				</Form.Group>
				<Form.Group
					as={Col}
					controlId='formDescrption'
					className='text-start'
					sm={12}
					md={6}>
					<Form.Label>Ciudad</Form.Label>
					<Select
						className='basic-single'
						classNamePrefix='select'
						isLoading={loadingCity}
						isDisabled={pais === 0}
						isClearable
						isSearchable
						name='ciudad'
						value={{
							value: data?.ciudad_id,
							label: data?.nombre_ciudad,
						}}
						onChange={(ciudad) =>
							setData({
								...data,
								ciudad_id: ciudad.value,
								nombre_ciudad: ciudad.label,
							})
						}
						options={ciudades?.map((ciudad) => ({
							value: ciudad?.ciudad_id,
							label: ciudad?.nombre_ciudad,
						}))}
					/>
				</Form.Group>
			</Row>
			<Row className='text-end'>
				<div>
					<Button
						variant='danger'
						type='button'
						className='me-2'
						onClick={() => history.push('/rooms')}>
						Cancelar
					</Button>
					<Button variant='primary' type='submit' className='ms-2'>
						{props?.action === 'create'
							? 'Crear Habitación'
							: 'Actualizar Habitación'}
					</Button>
				</div>
			</Row>
		</Form>
	);
}
