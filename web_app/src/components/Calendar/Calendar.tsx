import React, { useEffect } from 'react';
import { Table } from 'react-bootstrap';
import { v4 as uuidv4 } from 'uuid';

const mes_dias = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

export default function Calendar(props: any) {
	const { calendario, setCalendario } = props;
	useEffect(() => {
		const days: any[] = [];
		const [year, month] = props.month.split('-');
		for (let i = 1; i <= mes_dias[+month - 1]; i++) {
			days.push(new Date(`${+year}-${+month}-${i}`));
		}
		const added = [];
		if (days[0].getDay() !== 1) {
			for (let i = days[0].getDay() - 1; i > 0; i--) {
				added.push({ weekday: i, day: '', value: '' });
			}
		}
		const dias = [...added, ...days];
		let semana = 0;
		let dia = 0;
		let cal: any[][] = [[]];
		for (let d of dias) {
			if (d.hasOwnProperty('weekday')) {
				cal[semana][dia] = d;
			} else {
				let mes = d.getMonth() + 1;
				mes = mes < 10 ? `0${mes}` : mes;
				let date = d.getDate();
				date = date < 10 ? `0${date}` : date;
				let comprobacion = props?.calendarios?.filter(
					(item) =>
						item.fecha_i ===
						`${d.getFullYear()}/${mes}/${date}`
				);
				console.log(comprobacion);
				cal[semana][dia] = {
					weekday: d.getDay(),
					day: date,
					value: `${d.getFullYear()}/${mes}/${date}`,
					estado:
						comprobacion.lenght === 0 ? 1 : comprobacion[0]?.estado,
				};
			}
			dia++;
			if (dia === 7) {
				dia = 0;
				semana++;
				cal.push([]);
			}
		}
		setCalendario(cal);
	}, [props.month, setCalendario, props?.calendarios]);

	const selectDay = (id: any, dia: any, x: number, y: number) => {
		if (!dia.value) return;
		const element = document.getElementById(id);
		console.log(element);
		console.log(dia.day);
		if (+element?.innerHTML !== +dia.day) return;
		if (element.classList.contains('bg-primary')) {
			element.classList.remove('bg-primary');
		} else {
			element.classList.add('bg-primary');
		}
	};

	return (
		<div className='d-flex w-sm-100 w-md-75 flex-column justify-content-center'>
			<Table striped bordered id='calendario' responsive>
				<thead>
					<tr>
						<th>Lunes</th>
						<th>Martes</th>
						<th>Miércoles</th>
						<th>Jueves</th>
						<th>Viernes</th>
						<th>Sábado</th>
						<th>Domingo</th>
					</tr>
				</thead>
				<tbody>
					{calendario.map((semana, y) => (
						<tr key={uuidv4()}>
							{semana.map((dia: any, x) => {
								const id = uuidv4();
								const date = props?.calendarios?.filter(
									(item) =>
										item.fecha_i === dia.value &&
										item.estado === 0
								);
								console.log(date);
								console.log(dia.value);
								return (
									<td
										id={id}
										key={id}
										className={
											date.length > 0
												? 'bg-primary'
												: dia.estado === 0
												? 'bg-primary'
												: ''
										}
										onClick={() => selectDay(id, dia, x, y)}
										style={{ cursor: 'pointer' }}>
										{dia.day}
									</td>
								);
							})}
						</tr>
					))}
				</tbody>
			</Table>
		</div>
	);
}
