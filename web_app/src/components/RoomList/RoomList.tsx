import React from 'react';
import RoomItemList from '../RoomItemList/RoomItemList';

export default function RoomList(props: any) {
	return (
		<div className='d-flex w-100 p-4 flex-column'>
			{props?.rooms &&
				props?.rooms?.map((room: any, i: number) => (
					<RoomItemList room={room} key={i} />
				))}
		</div>
	);
}
