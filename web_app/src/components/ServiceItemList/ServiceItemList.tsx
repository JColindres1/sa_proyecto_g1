import React from 'react';
import { Badge, Button } from 'react-bootstrap';
import { NavLink, useHistory } from 'react-router-dom';
import { FaDollarSign } from 'react-icons/fa';
/* import swal from 'sweetalert';
import axios from 'axios';
import { API_HOTEL } from '../../helpers/endpoint'; */

export default function ServiceItemList(props: any) {
	/* let userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo'))
		: null; */
	const history = useHistory();

	/* 	const handleError = (error: any) => {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error.response.data);
			console.log(error.response.status);
			console.log(error.response.headers);
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error.message);
		}
		console.log(error.config);
	}; */

	return (
		<div className='d-flex flex-row w-100 justify-content-between border-top border-bottom p-4'>
			<div className='d-flex flex-column w-75 w-md-50 align-items-start justify-content-start'>
				<div className='text-start'>
					<NavLink to={`/rooms/view/${props?.service?.service_id}`}>
						<strong>{props?.service?.nombre}</strong>
					</NavLink>
				</div>
				<div className='text-start'>
					Tipo de Servicio: {props?.service?.tipo}
				</div>
				<div className='text-start'>
					Fecha Inicio: {props?.service?.fecha_inicio}
				</div>
				<div className='text-start'>
					Fecha Fin: {props?.service?.fecha_fin}
				</div>
				{props?.service?.fecha_pago && (
					<div className='text-start'>
						Fecha Pago: {props?.service?.fecha_pago}
					</div>
				)}
				<Badge bg={props?.service?.estado === 1 ? 'success' : 'danger'}>
					{props?.service?.estado === 1 ? 'Finalizada' : 'Pendiente'}
				</Badge>
			</div>
			<div className='d-flex flex-column w-25 w-md-50 align-items-end justify-content-between'>
				<div>Q.{props?.service?.precio.toFixed(2)}</div>
				<div className='d-flex flex-column flex-md-row w-100 justify-content-center justify-content-md-end align-items-end align-items-md-center'>
					{props?.service?.estado === 0 && (
						<div className='ms-2'>
							<Button
								variant='info'
								onClick={() =>
									history.push(
										`/rooms/view/${props?.service?.habitacion_id}`
									)
								}>
								<FaDollarSign style={{ fontSize: '1.2rem' }} />
							</Button>
						</div>
					)}
				</div>
			</div>
		</div>
	);
}
