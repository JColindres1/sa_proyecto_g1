import React, { useState, useEffect } from 'react';
import { Col, Row, Form, Button } from 'react-bootstrap';
import Select from 'react-select';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { API_AUTOS,API_HOTEL } from '../../helpers/endpoint';
import { cpuUsage } from 'process';
export default function CarForm(props: any) {
	const history = useHistory();
	const [data, setData] = useState({
		placa: '',
		precio: '',
        marca:'',
		linea: '',
		modelo: '',
        precio_dia:0,
        imagen:'',
		idusuario: props?.user?.id_usuario,
		idciudad: 0,
		idpais: 0,
		nombre_pais: '',
		nombre_ciudad: ''
	});
	const [imgB64,setimgB64] = useState('');
	const [loadingCountry, setLoadingCountry] = useState(false);
	const [pais, setPais] = useState(0);
	const [paises, setPaises] = useState([]);
	const [loadingCity, setLoadingCity] = useState(true);
	const [ciudades, setCiudades] = useState([]);
	
	useEffect(() => {
        console.log(props)
		props?.action === 'update' && setData({ ...props?.room });
		props?.action === 'update' && setPais(props?.room?.pais_id);
	}, [props.room, props?.action]);

	useEffect(() => {
		setLoadingCountry(true);
		axios
			.get(`${API_HOTEL}/paises`)
			.then((res) => res.data)
			.then((res) => {
				if (res.success) {
					setPaises(res.paises);
				} else {
					setPaises([]);
				}
				setLoadingCountry(false);
			})
			.catch((e) => {
				setLoadingCountry(false);
				console.log(e);
				setPais(0);
			});
	}, []);

	useEffect(() => {
		if (pais) {
			setLoadingCity(true);
			axios
				.get(`${API_HOTEL}/ciudades/${pais}`)
				.then((res) => res.data)
				.then((res) => {
					if (res.success) {
						setCiudades(res.ciudades);
					} else {
						setCiudades([]);
					}
					setLoadingCity(false);
				});
		}
	}, [pais]);

	const handleChangeData = (e: any) =>
		setData({ ...data, [e.target.name]: e.target.value });
	console.log(data);
	const handleSubmit = async (e: any) => {
		e.preventDefault();
        console.log(data);
		console.log(imgB64)
	  let response = await axios.post(`${API_AUTOS}img-s3`,{'imagen':imgB64})	
	      console.log(response.data.ruta);
          data.imagen = response.data.ruta;
		  data.precio_dia = parseInt(data?.precio)
			axios
				.post(`${API_AUTOS}registrar`, data)
				.then((res) => res.data)
				.then((res) => {
					console.log(res);
				})
				.catch(handleError);
	
	};
	const handleImageChange = async (e) => {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];
        let idCardBase64 = '';
        await getBase64(file, (result) => {
            idCardBase64 = result;
        });
        reader.onloadend = () => {
			console.log(file);
			console.log(reader.result)
			console.log(idCardBase64);
			setimgB64(idCardBase64);
        }
        reader.readAsDataURL(file)
    }

	const getBase64 = async (file, cb)=> {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

	const handleError = (error: any) => {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error.response.data);
			console.log(error.response.status);
			console.log(error.response.headers);
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error.message);
		}
		console.log(error.config);
	};

	return (
		<Form onSubmit={handleSubmit}>
			<Row className='mb-3'>
				<Form.Group
					as={Col}
					controlId='formName'
					className='text-start'
					sm={12} md={6}>
					<Form.Label>Placa</Form.Label>
					<Form.Control
						type='text'
						required
						name='placa'
						value={data.placa}
						onChange={handleChangeData}
						placeholder='Ingrese el nombre'
					/>
				</Form.Group>
				<Form.Group
					as={Col}
					controlId='formPrecio'
					className='text-start'
					sm={12} md={6}>
					<Form.Label>Precio</Form.Label>
					<Form.Control
						type='number'
						required
						name='precio'
						value={data.precio}
						onChange={handleChangeData}
						placeholder='Ingrese el precio'
					/>
				</Form.Group>
			</Row>
			<Row className='mb-3'>
            <Form.Group
					as={Col}
					controlId='formPersonas'
					className='text-start'
					sm={12} md={6} lg={4}>
					<Form.Label>Marca</Form.Label>
					<Form.Control
						type='text'
						required
						name='marca'
						value={data.marca}
						onChange={handleChangeData}
						placeholder='Ingrese cantidad de personas máximo'
					/>
				</Form.Group>

                <Form.Group
					as={Col}
					controlId='formPersonas'
					className='text-start'
					sm={12} md={6} lg={4}>
					<Form.Label>Modelo</Form.Label>
					<Form.Control
						type='text'
						required
						name='modelo'
						value={data.modelo}
						onChange={handleChangeData}
						placeholder='Ingrese cantidad de personas máximo'
					/>
				</Form.Group>

                <Form.Group
					as={Col}
					controlId='formPersonas'
					className='text-start'
					sm={12} md={6} lg={4}>
					<Form.Label>Linea</Form.Label>
					<Form.Control
						type='text'
						required
						name='linea'
						value={data.linea}
						onChange={handleChangeData}
						placeholder='Ingrese cantidad de personas máximo'
					/>
				</Form.Group>
		
			</Row>
            <Row className='mb-3'>
            <Form.Group
					as={Col}
					controlId='formPersonas'
					className='text-start'
					sm={12} md={6} lg={4}>
					<Form.Label>Imagen</Form.Label>
					
    <Form.Label>Default file input example</Form.Label>
    <Form.Control type="file"  onChange={handleImageChange}/>

				</Form.Group>
            </Row>
			<Row className='mb-3'>
				<Form.Group
					as={Col}
					controlId='formDescrption'
					className='text-start'
					sm={12} md={6}>
					<Form.Label>Pais</Form.Label>
					<Select
						className='basic-single'
						classNamePrefix='select'
						isLoading={loadingCountry}
						isClearable
						isSearchable
						name='pais'
						value={{
							value: data?.idpais,
							label: data?.nombre_pais,
						}}
						onChange={(pais) => {
							setPais(pais.value);
							setData({
								...data,
								idpais: pais.value,
								nombre_pais: pais.label,
							})
						}}
						options={paises?.map((pais) => ({
							value: pais?.pais_id,
							label: pais?.nombre_pais,
						}))}
					/>
				</Form.Group>
				<Form.Group
					as={Col}
					controlId='formDescrption'
					className='text-start'
					sm={12} md={6}>
					<Form.Label>Ciudad</Form.Label>
					<Select
						className='basic-single'
						classNamePrefix='select'
						isLoading={loadingCity}
						isDisabled={pais === 0}
						isClearable
						isSearchable
						name='ciudad'
						value={{
							value: data?.idciudad,
							label: data?.nombre_ciudad,
						}}
						onChange={(ciudad) =>
							setData({
								...data,
								idciudad: ciudad.value,
								nombre_ciudad: ciudad.label,
							})
						}
						options={ciudades?.map((ciudad) => ({
							value: ciudad?.ciudad_id,
							label: ciudad?.nombre_ciudad,
						}))}
					/>
				</Form.Group>
			</Row>
			<Row className='text-end'>
				<div>
					<Button
						variant='danger'
						type='button'
						className='me-2'
						onClick={() => history.push('/')}>
						Cancelar
					</Button>
					<Button variant='primary' type='submit' className='ms-2'>
					      Registrar Auto
					</Button>
				</div>
			</Row>
		</Form>
	);
}
