
import { Form, Row, Col, Button, Container,Card } from 'react-bootstrap';
import NavBar from '../../components/NavBar/NavBar';
import React, { useState, useEffect } from 'react';
import { useIonAlert } from '@ionic/react';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from "axios";
import { API_USERS } from '../../helpers/endpoint';
import { useHistory } from 'react-router-dom';
import { IonContent, IonDatetime, IonItem, IonInput, IonButton, IonModal, IonIcon, IonPopover, IonLabel, IonText } from '@ionic/react';
import { format, parseISO } from 'date-fns';
import { calendar } from 'ionicons/icons';
import { Camera, CameraResultType } from '@capacitor/camera';
import WebCam from "react-webcam";
import { FcCompactCamera } from "react-icons/fc";
import { defineCustomElements } from '@ionic/pwa-elements/loader';
const FormRegister = (props) => {
    defineCustomElements(window);
    
    const history = useHistory();
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(null);
    const onChange = (dates) => {
        const [start, end] = dates;
        setStartDate(start);
        setEndDate(end);
    };
    const [selectedValue1, setSelectedValue1] = useState();
    const [selectedValue2, setSelectedValue2] = useState();
    const [selectedRol, setSelectedRol] = useState(2);
    const [popoverDate2, setPopoverDate2] = useState('');
    const [imgB64,setImgB64] = useState('')
    const [present] = useIonAlert();
    const videoConstraints = { width: 400, height: 400, facingMode: "user" };
    useEffect(() => {
        const obj = JSON.parse(localStorage.getItem("userInfo"))
        if (obj?.rol_id === 1) {
            history.push('/turista/home')
        }
        else if (obj?.rol_id === 2) {
            history.push('/admin/home')
        }
        else if (obj?.rol_id === 3) {
            history.push('/rooms')
        }
        else if (obj?.rol_id === 4) {
            history.push('/cars/home')
        }
        else if (obj?.rol_id === 5) {
            history.push('/flight_register')
        }
    }, []);

    let handleChange1 = selectedValue1 => {
        setSelectedValue1(selectedValue1.value);
        ciudades = []
        axios
            .get(`${API_USERS}/ciudades`, {
                params: { pais: selectedValue1.value }
            })
            .then((response) => {
                if (response.data) {
                    for (var i = 0; i < response.data.length; i++) {
                        ciudades.push({ value: response.data[i].Ciudad_id, label: response.data[i].Nombre });
                    }
                }
                else {
                    ciudades.push({ value: "error", label: "Error" });
                }
            });

    };

    let handleChange2 = selectedValue2 => {
        setSelectedValue2(selectedValue2.value);
    };

    let handleChange3 = selectedValue3 => {
        console.log(selectedValue3.value,"rol");
        setSelectedRol(selectedValue3.value);
    };

    const formatDate = (value: string) => {
        return format(parseISO(value), 'MMM dd yyyy');
    };

    let getMessages = () => {
        axios
            .get(`${API_USERS}/roles`, {
                //va el json a enviar en post
            })
            .then((response) => {
                console.log(response)
                if (response) {
                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].Descripcion !=='Administrador') {
                           
                            roles.push({ value: response.data[i].Rol_id, label: response.data[i].Descripcion });
                        }
                    }
                }
                else {
                    roles.push({ value: "error", label: "Error" });
                }
            });

        axios
            .get(`${API_USERS}/paises`, {
                //va el json a enviar en post
            })
            .then((response) => {
                if (response) {
                    for (var i = 0; i < response.data.length; i++) {
                        paises.push({ value: response.data[i].Pais_id, label: response.data[i].Nombre });
                    }

                }
                else {
                    paises.push({ value: "error", label: "Error" });
                }
            });
    }

    const takePicture = async () => {
        const image = await Camera.getPhoto({
            quality: 90,
            allowEditing: false,
            resultType: CameraResultType.DataUrl
            });
            var imageUrl = image;
             console.log(imageUrl);
            // Can be set to the src of an image now
            console.log(imageUrl.dataUrl);
            setImgB64(imageUrl.dataUrl);
      };

    useEffect(() => {
        getMessages();
    }, []);

    const setCountrys = (type) =>{
    if(type == 1){
        return(
    <Col sm={6}>
        <Form.Group controlId="Rol">
            <Form.Label className="form-text">Tipo de cuenta</Form.Label>
            <Select options={roles}
                placeholder="Select Option"
                value={roles.find(obj => obj.value === selectedRol)}
                onChange={handleChange3}
            />
        </Form.Group>
    </Col>)
        }else{
          
        }
    }

    
    const setRol = (valor) =>{
        setSelectedRol(valor);
    }

    const setRef = (webcam) => {
        webcam = webcam;
      };



    let handleSubmit = (event) => {
        event.preventDefault();
        console.log(imgB64);
           let data = {
           nombre: event.target.elements.Name.value,
           correo: event.target.elements.Mail.value,
           contrasena: event.target.elements.Password.value,
           //Fecha:new Date(popoverDate2).toISOString().substring(0, 10),
           ciudad_id: selectedValue2,
           rol_id: selectedRol,
           Imagen: imgB64}

           console.log(data);
           
 
        if (event.target.elements.Password.value === event.target.elements.ConfirmPassword.value) {
            console.log(new Date(popoverDate2).toISOString().substring(0, 10),);
            axios
                .post(`${API_USERS}/register`, {
                    nombre: event.target.elements.Name.value,
                    correo: event.target.elements.Mail.value,
                    contrasena: event.target.elements.Password.value,
                    //Fecha:new Date(popoverDate2).toISOString().substring(0, 10),
                    ciudad_id: selectedValue2,
                    rol_id: selectedRol,
                    Imagen: imgB64
                })
                .then((response) => {
                    console.log(response);
                    if (response.data === 201) {
                        present({
                            cssClass: 'my-css',
                            header: '¡Éxito!',
                            message: 'Usuario creado con éxito',
                            buttons: [
                                { text: 'Ok', handler: (d) => console.log('ok pressed') },
                            ],
                         })

                         history.push('/')

                    }
                    else if (response.data === 202) {
                        present({
                            cssClass: 'my-css',
                            header: '¡Alerta!',
                            message: 'El correo ya está en uso',
                            buttons: [
                                { text: 'Ok', handler: (d) => window.location.reload() },
                            ],
                        })
                    }
                });
        }
        else {
            console.log("La contraseña no coincide")
            present({
                cssClass: 'my-css',
                header: '¡Alerta!',
                message: 'La contraseña no coincide',
                buttons: [
                    { text: 'Ok', },
                ],
            })
        }
    }

    const handleScreen = (e) => {
      //  this.setState({ base64: webcam.getScreenshot().split(",")[1] });
      };

    return (
        <>

            <h1 className='title-custom title-center'>Registro</h1>
                <div className='card-container'>
              
                <Card className="login-card">    
                <div className='margin-login'>
                    <Form onSubmit={handleSubmit}>
                        <Row className="mb-3">
                            <Col >
                                <Form.Group controlId="Name">
                                    <Form.Label className="form-text">Nombre Completo</Form.Label>
                                    <Form.Control />
                                </Form.Group>
                            </Col>
                        </Row>

                        <Row className="mb-3">
                            <Col xs={6}>
                                <Form.Group controlId="Country">
                                    <Form.Label className="form-text">Pais</Form.Label>
                                    <Select
                                        placeholder=""
                                        value={paises.find(obj => obj.value === selectedValue1)}
                                        onChange={handleChange1}
                                        options={paises}
                                    />
                                </Form.Group>
                            </Col>
                            <Col xs={6}>
                                <Form.Group controlId="Cities">
                                    <Form.Label className="form-text">Ciudad</Form.Label>
                                    <Select
                                        placeholder=""
                                        value={ciudades.find(obj => obj.value === selectedValue2)}
                                        onChange={handleChange2}
                                        options={ciudades}
                                    />
                                </Form.Group>
                            </Col>
                        </Row>

                        <Row className="mb-3">
                            <Col sm={6}>
                                <Form.Group controlId="Mail">
                                    <Form.Label className="form-text">Correo</Form.Label>
                                    <Form.Control type="email" />
                                </Form.Group>
                            </Col>
                             {setCountrys(props.formType)}
                        </Row>

                        <Row className="mb-3">
                            <Col sm={6}>
                                <Form.Group controlId="Password">
                                    <Form.Label className="form-text">Password</Form.Label>
                                    <Form.Control type="password" />
                                </Form.Group>
                            </Col>
                            <Col sm={6}>
                                <Form.Group controlId="ConfirmPassword">
                                    <Form.Label className="form-text">Confirm Password</Form.Label>
                                    <Form.Control type="password" />
                                </Form.Group>
                            </Col>
                        </Row>

                  


                        <IonButton color="primary" onClick={() => takePicture()}><FcCompactCamera size={30}/></IonButton>

                        <Row className="mb-3">
                            <Col sm={6}>
                                <Form.Group controlId="Date">
                                    <Form.Label className="form-text">Fecha de Nacimiento</Form.Label>

                                    <IonItem>
                                        <IonInput id="date-input-2" value={popoverDate2} />
                                        <IonButton fill="clear" id="open-modal">
                                            <IonIcon icon={calendar} />
                                        </IonButton>
                                    </IonItem>
                                    <IonModal trigger="open-modal">
                                        <IonContent  force-overscroll="false">
                                            <div className='center-calendar'>
                                                <IonDatetime presentation="date"
                                                    onIonChange={ev => setPopoverDate2(formatDate(ev.detail.value!))}></IonDatetime>
                                            </div>
                                        </IonContent>
                                    </IonModal>
                                </Form.Group>
                            </Col>

                        </Row>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                    </div>
                    </Card>
                    </div>
     
        </>
    );
};

export default FormRegister;

var roles = []
var paises = []
var ciudades = []