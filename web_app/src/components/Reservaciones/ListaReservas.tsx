import React from 'react';
import ReservaItem from './ReservaItem';
const reservas = [
	{
		id_reserva: 1,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		id_usuario: 1,
		precio: 'Q100.00',
		usuario: 'Juan Perez',
		estado: 0,
	},
	{
		id_reserva: 2,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		id_usuario: 1,
		precio: 'Q100.00',
		usuario: 'Juan Perez',
		estado: 1,
	},
	{
		id_reserva: 3,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		id_usuario: 1,
		precio: 'Q100.00',
		usuario: 'Juan Perez',
		estado: 1,
	},
	{
		id_reserva: 4,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		id_usuario: 1,
		precio: 'Q100.00',
		usuario: 'Juan Perez',
		estado: 1,
	},
	{
		id_reserva: 5,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		id_usuario: 6,
		precio: 'Q100.00',
		usuario: 'Juan Perez',
		estado: 0,
	},
	{
		id_reserva: 7,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		id_usuario: 1,
		precio: 'Q100.00',
		usuario: 'Juan Perez',
		estado: 0,
	},
	{
		id_reserva: 8,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		id_usuario: 1,
		precio: 'Q100.00',
		usuario: 'Juan Perez',
		estado: 1,
	},
	{
		id_reserva: 9,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		id_usuario: 1,
		precio: 'Q100.00',
		usuario: 'Juan Perez',
		estado: 0,
	},
	{
		id_reserva: 10,
		fecha_inicio: '2020-05-01',
		fecha_fin: '2020-05-02',
		id_usuario: 1,
		precio: 'Q100.00',
		usuario: 'Juan Perez',
		estado: 0,
	},
];

export default function ListaReservas(props: any) {
	return (
		<div className='d-flex flex-column w-100'>
			<div className='d-flex flex-row w-100 justify-content-between'>
				<h4>Reservaciones</h4>
			</div>
			{reservas.map((reserva: any) => (
				<ReservaItem key={reserva.id_reserva} reserva={reserva} />
			))}
		</div>
	);
}
