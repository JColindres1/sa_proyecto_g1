import React from 'react';
import { Badge } from 'react-bootstrap';

export default function Reserva(props: any) {
	return (
		<div className='d-flex flex-column w-100 p-4 border boder-primary rounded mb-2'>
			<div className='d-flex flex-row justify-content-between'>
				<div className='d-flex flex-column'>
					{props?.reserva?.usuario}
				</div>
				<div className='d-flex flex-column'>
					<b className='p-0 m-0'>{props?.reserva?.precio}</b>
				</div>
			</div>
			<div className='d-flex flex-row justify-content-between'>
				<div className='d-flex flex-column justify-content-center align-items-start'>
					<p className='text-start'>{`Del ${props?.reserva?.fecha_inicio} al ${props?.reserva?.fecha_fin}`}</p>
					<Badge
						bg={
							props?.reserva?.estado === 1 ? 'success' : 'danger'
						}>
						{props?.reserva?.estado === 1
							? 'Finalizada'
							: 'Pendiente'}
					</Badge>
				</div>
			</div>
		</div>
	);
}
