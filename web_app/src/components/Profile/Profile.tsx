import './Profile.css';
import { Form, Row, Col, Button, Container, Card } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { IonContent } from '@ionic/react';
import Typewriter from "typewriter-effect";
import { motion } from "framer-motion";
interface ContainerProps { }

const Profile = () => {
    const history = useHistory();
    return (
        <>
        
        <div className='container-profile'>
            <Row>
                <Col sm={12} style={{ display: 'flex', justifyContent: 'center' }}>
                <span className='profile_title'><strong>Perfil</strong></span> <br></br>
                </Col>
                <Col sm={12} style={{ display: 'flex', justifyContent: 'center' }}>
 
                    <Card className="Card">
                        <Card.Body>
                        <motion.div
                                           
                                           initial={{ opacity: 0, scale: 0 }}
                                           animate={{ opacity: 1, scale: 1 }}
                                       >
                            <Card.Title>Mis Datos</Card.Title>
                            <Col sm={12}  className="photo-position">
                            <div className="porfile-container">

                            </div>
                            </Col>
                            <Row>
                                <div className='information-section'>
                                    <Col sm={12}  style={{ display: 'flex', justifyContent: 'left' }}>
                                        <label>Nombre: Kevin Estuardo Cardona López</label>
                                    </Col>
                                    <Col sm={12} style={{ display: 'flex', justifyContent: 'left' }}>
                                        <label>Correo: kevsemp6812@gmail.com</label>
                                    </Col>
                                </div>
                            </Row>

                            </motion.div>
                        </Card.Body>
                    </Card>
                    
                    <p className="main-title">
                    </p>
                    {/* <Button variant="outline-primary" className='custom-btn' onClick={() => history.push('/login')}>Perfil</Button> */}

                </Col>

            </Row>

            <section className="bubble">
 
    </section>

            </div>
        </>
    );
};

export default Profile;
