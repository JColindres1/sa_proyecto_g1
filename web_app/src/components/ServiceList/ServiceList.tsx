import React from 'react';
import ServiceItemList from '../ServiceItemList/ServiceItemList';

export default function ServiceList(props: any) {
	return (
		<div className='d-flex w-100 p-4 flex-column'>
			{props?.services &&
				props?.services?.map((service: any, i: number) => (
					<ServiceItemList service={service} key={i} />
				))}
		</div>
	);
}
