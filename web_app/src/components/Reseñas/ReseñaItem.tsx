import React from 'react';
import Estrellas from '../Estrellas/Estrellas';

export default function ReseñaItem(props: any) {
	return (
		<div className='d-flex flex-column w-100 p-4 border boder-primary rounded mb-2'>
			<div className='d-flex flex-row justify-content-between'>
				<div className='d-flex flex-column'>
					{props?.reseña?.usuario}
				</div>
				<div className='d-flex flex-column'>
					<Estrellas stars={props?.reseña?.calificacion} />
				</div>
			</div>
			<div className='d-flex flex-row justify-content-between'>
				<div className='d-flex flex-column justify-content-start align-items-start'>
					<b>Comentario:</b>
					<p className='text-start'>{props?.reseña?.reseña}</p>
				</div>
			</div>
		</div>
	);
}
