import React from 'react';
import Estrellas from '../Estrellas/Estrellas';
import ReseñaItem from './ReseñaItem';
const reseñas = [
	{
		id_reseña: 1,
		reseña: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat1',
		calificacion: 5,
		usuario: 'Usuario 1',
	},
	{
		id_reseña: 2,
		reseña: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat2',
		calificacion: 4,
		usuario: 'Usuario 2',
	},
	{
		id_reseña: 3,
		reseña: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat3',
		calificacion: 5,
		usuario: 'Usuario 3',
	},
	{
		id_reseña: 4,
		reseña: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat4',
		calificacion: 4,
		usuario: 'Usuario 4',
	},
	{
		id_reseña: 5,
		reseña: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat5',
		calificacion: 5,
		usuario: 'Usuario 5',
	},
	{
		id_reseña: 6,
		reseña: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat6',
		calificacion: 4,
		usuario: 'Usuario 6',
	},
	{
		id_reseña: 7,
		reseña: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat7',
		calificacion: 1,
		usuario: 'Usuario 7',
	},
	{
		id_reseña: 8,
		reseña: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat8',
		calificacion: 5,
		usuario: 'Usuario 8',
	},
	{
		id_reseña: 9,
		reseña: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat9',
		calificacion: 2,
		usuario: 'Usuario 9',
	},
	{
		id_reseña: 10,
		reseña: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat10',
		calificacion: 4,
		usuario: 'Usuario 10',
	},
];

export default function ListaReseñas(props: any) {
	return (
		<div className='d-flex flex-column w-100'>
			<div className='d-flex flex-row w-100 justify-content-between'>
				<h4>Reseñas</h4>
				<Estrellas stars={reseñas.reduce(
					(acc, curr) => ({
						calificacion: acc.calificacion + curr.calificacion,
					}),
					{ calificacion: 0 }
				).calificacion / reseñas.length} />
			</div>
			{reseñas.map((reseña: any) => (
				<ReseñaItem key={reseña.id_reseña} reseña={reseña} />
			))}
		</div>
	);
}
