import React, { useState } from 'react';
import { Button, Form, Modal, Row } from 'react-bootstrap';

import { Rating } from 'react-simple-star-rating';

export default function Reseña(props: any) {
	const [rating, setRating] = useState(0);
	const handleRating = (rate: number) => {
		setRating(rate);
	};
	return (
		<Modal
			{...props}
			size='xl'
			aria-labelledby='contained-modal-title-vcenter'
			centered>
			<Modal.Header closeButton>
				<Modal.Title id='contained-modal-title-vcenter'>
					Reseña de servicio
				</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Row className='d-flex flex-row'>
					<Form.Group
						className='mb-3'
						controlId='exampleForm.ControlTextarea1'>
						<Form.Label className='me-2'>Calificación </Form.Label>
						<Rating onClick={handleRating} ratingValue={rating} />
					</Form.Group>
				</Row>
				<Row className='d-flex flex-row'>
					<Form.Group
						className='mb-3'
						controlId='exampleForm.ControlTextarea1'>
						<Form.Label>Reseña</Form.Label>
						<Form.Control as='textarea' rows={3} />
					</Form.Group>
				</Row>
			</Modal.Body>
			<Modal.Footer>
				<Button variant='secondary' onClick={props.onHide}>
					Cancelar
				</Button>
				<Button variant='primary' onClick={props.onHide}>
					Realizar
				</Button>
			</Modal.Footer>
		</Modal>
	);
}
