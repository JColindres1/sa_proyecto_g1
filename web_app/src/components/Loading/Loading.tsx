import React from 'react';
import { IonSpinner, IonContent } from '@ionic/react';

export default function Loading(props: any) {
	return (
		<div className='d-flex w-100 justify-content-center h-auto align-items-center'>
			<span className='me-2'>Loading</span>
			<IonSpinner name={props?.name || 'crescent'} />
		</div>
	);
}
