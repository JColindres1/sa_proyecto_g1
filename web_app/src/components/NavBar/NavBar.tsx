import { Navbar, Nav, Container, Offcanvas } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import './NavBar.css';

const NavBar: React.FC = () => {
	const history = useHistory();
	let userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo'))
		: null;

	let cierreSesion = () => {
		localStorage.clear();
		history.push('/login');
	};
	return (
		<>
			{/* 
<Navbar collapseOnSelect expand="lg" bg="primary" variant="dark">
  <Container fluid>
  <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="me-auto">

    </Nav>
    <Nav>
      <Nav.Link href="#deets">Habitaciones</Nav.Link>
	  <Nav.Link href="#deets">Vuelos</Nav.Link>
	  <Nav.Link href="#deets">Habitaciones</Nav.Link>
	  <Nav.Link href="#deets">Habitaciones</Nav.Link>
      <Nav.Link eventKey={2} href="#memes">
        Dank memes
      </Nav.Link>
    </Nav>
  </Navbar.Collapse>
  </Container>
</Navbar> */}
			<Navbar
				style={{ zIndex: '3' }}
				bg='primary'
				variant='dark'
				expand={false}>
				<Container fluid>
					<Navbar.Brand href='#'>Full Trip</Navbar.Brand>
					<Navbar.Toggle aria-controls='offcanvasNavbar' />
					<Navbar.Offcanvas
						id='offcanvasNavbar'
						aria-labelledby='offcanvasNavbarLabel'
						placement='end'>
						<Offcanvas.Header closeButton>
							<Offcanvas.Title id='offcanvasNavbarLabel'>
								Menu
							</Offcanvas.Title>
						</Offcanvas.Header>
						<Offcanvas.Body>
							{!userInfo && (
								<Nav className='justify-content-end flex-grow-1 pe-3'>
									<Nav.Link href='/login'>Login</Nav.Link>
									<Nav.Link
										className='linkText'
										href='/register'>
										Registrose
									</Nav.Link>
								</Nav>
							)}
							{userInfo && userInfo?.rol_id === 1 && (
								<Nav className='text-Nav justify-content-end flex-grow-1 pe-3'>
									<Nav.Link href='/rooms'>
										Habitaciones
									</Nav.Link>
									<Nav.Link href='/show_flight'>
										Vuelos
									</Nav.Link>
									<Nav.Link href='/turista/autos'>
										Autos
									</Nav.Link>
									<Nav.Link href='/turista/reservaciones'>
										Reservaciones
									</Nav.Link>
									{/* <Nav.Link href='/turista/mis/reservaciones/autos'>
										Reservaciones Autos
									</Nav.Link> */}
									<Nav.Link href='/turista/esquema'>
										Esquema de Vacunación
									</Nav.Link>
									<Nav.Link href='/turista/perfil'>
										Perfil
									</Nav.Link>
									<Nav.Link
										href='#'
										onClick={() => cierreSesion()}>
										Cerrar Sesión
									</Nav.Link>
								</Nav>
							)}
							{userInfo && userInfo?.rol_id === 2 && (
								<Nav className='justify-content-end flex-grow-1 pe-3'>
									<Nav.Link href='/rooms'>
										Habitaciones
									</Nav.Link>
									<Nav.Link href='/rooms/register'>
										Registro Habitación
									</Nav.Link>
									<Nav.Link href='/show_flight'>
										Vuelos
									</Nav.Link>
									<Nav.Link href='/flight_register'>
										Registro Vuelo
									</Nav.Link>
									<Nav.Link href='/turista/autos'>
										Autos
									</Nav.Link>
									<Nav.Link href='/cars/register'>
										Registro Auto
									</Nav.Link>
									<Nav.Link href='/cars/register_file'>
										Cargar Autos
									</Nav.Link>
									<Nav.Link href='/vacpais'>
										Esquema País
									</Nav.Link>
									<Nav.Link href='/schedule'>
										Esquemas
									</Nav.Link>

									<Nav.Link href='/dosis'>
										Dosis
									</Nav.Link>

									<Nav.Link href='/vaccination'>
									    Vacunas
									</Nav.Link>

									<Nav.Link href='/turista/autos'>
										Autos
									</Nav.Link>
									<Nav.Link href='/results'>
										Resultados
									</Nav.Link>
									<Nav.Link
										href='#'
										onClick={() => cierreSesion()}>
										Cerrar Sesión
									</Nav.Link>
								</Nav>
							)}
							{userInfo && userInfo?.rol_id === 3 && (
								<Nav className='justify-content-end flex-grow-1 pe-3'>
									<Nav.Link href='/rooms'>
										Habitaciones
									</Nav.Link>
									<Nav.Link href='/rooms/register'>
										Registro Habitación
									</Nav.Link>
									<Nav.Link
										href='#'
										onClick={() => cierreSesion()}>
										Cerrar Sesión
									</Nav.Link>
								</Nav>
							)}
							{userInfo && userInfo?.rol_id === 4 && (
								<Nav className='justify-content-end flex-grow-1 pe-3'>
									<Nav.Link href='/cars/all_cars'>
										Autos
									</Nav.Link>
									<Nav.Link href='/cars/register'>
										Registro Auto
									</Nav.Link>
									<Nav.Link href='/cars/register_file'>
										Cargar Autos
									</Nav.Link>
									<Nav.Link
										href='#'
										onClick={() => cierreSesion()}>
										Cerrar Sesión
									</Nav.Link>
								</Nav>
							)}
						</Offcanvas.Body>
					</Navbar.Offcanvas>
				</Container>
			</Navbar>
		</>
	);
};

export default NavBar;
