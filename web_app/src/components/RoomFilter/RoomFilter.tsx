import {
	IonButton,
	IonDatetime,
	IonIcon,
	IonInput,
	IonItem,
	IonLabel,
	IonPopover,
	IonRange,
} from '@ionic/react';
import React from 'react';
import SearchBar from '../SearchBar/SearchBar';
import { format, parseISO } from 'date-fns';

import { calendar } from 'ionicons/icons';
export default function RoomFilter(props: any) {
	const formatDate = (value: string) => {
		return format(parseISO(value), 'yyyy/MM/dd');
	};

    const changeRange = (e: any)=>{
        console.log(e)
        props?.setMin(e?.detail?.value?.lower)
        props?.setMax(e?.detail?.value?.upper)
    }

	return (
		<div className='d-flex flex-column w-100 border rounded p-4 text-start'>
			<div>
				<SearchBar
					placeholder='Pais'
					setSearchText={props?.setPais}
					searchText={props?.pais}
				/>
			</div>
			<div>
				<SearchBar
					placeholder='Ciudad'
					setSearchText={props?.setCiudad}
					searchText={props?.ciudad}
				/>
			</div>
			<div>
				<SearchBar
					placeholder='Personas'
					setSearchText={props?.setPersonas}
					searchText={props?.personas}
					mode='number'
				/>
			</div>
			<div className='p-2'>
				<IonLabel>
					Precio: min: {props?.min} max: {props?.max}
				</IonLabel>
				<IonRange
					dualKnobs={true}
                    value={{lower: props?.min, upper: props?.max}}
					min={0}
					max={5000}
					step={100}
					snaps={true}
					onIonChange={changeRange}
				/>
			</div>

			<div className='p-2'>
				<IonLabel>Fecha Inicial</IonLabel>
				<IonItem>
					<IonInput id='date-input-1' value={props?.fecha_inicio} />
					<IonButton fill='clear' id='open-date-input-1'>
						<IonIcon icon={calendar} />
					</IonButton>
					<IonPopover
						trigger='open-date-input-1'
						showBackdrop={false}>
						<IonDatetime
							presentation='date'
							onIonChange={(ev) =>
								props?.setFechaInicio(formatDate(ev.detail.value!))
							}
						/>
					</IonPopover>
				</IonItem>
			</div>
			<div className='p-2'>
				<IonLabel>Fecha Final</IonLabel>
				<IonItem>
					<IonInput id='date-input-2' value={props?.fecha_fin} />
					<IonButton fill='clear' id='open-date-input-2'>
						<IonIcon icon={calendar} />
					</IonButton>
					<IonPopover
						trigger='open-date-input-2'
						showBackdrop={false}>
						<IonDatetime
							presentation='date'
							onIonChange={(ev) =>
								props?.setFechaFin(formatDate(ev.detail.value!))
							}
						/>
					</IonPopover>
				</IonItem>
			</div>
		</div>
	);
}
