import './ExploreContainer.css';
import { Row, Col, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { IonContent } from '@ionic/react';
import Typewriter from 'typewriter-effect';

const ExploreContainer = (props) => {
	const history = useHistory();
	return (
		<>
			<IonContent>
				<div className='container'>
					<Row>
						<Col sm={5}>
							<span className='title_size'>
								<strong>{props.title}</strong>
							</span>{' '}
							<br></br>
							<p className='main-title'>
								<Typewriter
									options={{
										autoStart: true,
										loop: true,
									}}
									onInit={(typewriter) => {
										typewriter
											.typeString('Get started')
											.pauseFor(800)
											.deleteAll()
											.typeString('Explore')
											.pauseFor(800)
											.deleteAll()
											.typeString('Travel')
											.pauseFor(800)
											.deleteAll()
											.typeString('Discover')
											.pauseFor(800)
											.deleteAll()
											.typeString('All...')
											.pauseFor(800)
											.deleteAll()
											.typeString('in one place ;)')
											.pauseFor(2000)
											.deleteAll()
											.start();
									}}
								/>
							</p>
							<Button
								variant='outline-primary'
								className='custom-btn'
								onClick={() => history.push(props?.route || '/login')}>
								{' '}
								{props.name}
							</Button>
						</Col>

						<Col sm={7}>
							<img src={props.imgIcon} alt='Logo' />
						</Col>
					</Row>
				</div>
			</IonContent>
		</>
	);
};

export default ExploreContainer;
