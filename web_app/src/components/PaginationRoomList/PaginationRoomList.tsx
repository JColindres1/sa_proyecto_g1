import React, {useState, useEffect} from 'react';
import { Pagination } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';

export default function PaginationRoomList(props: any) {
	const { search } = useLocation();

    const [params, setParams] = useState({page: 1});

    useEffect(() => {
		const groupParamsByKey = (params) =>
			[...params.entries()].reduce((acc, [key, val]) => {
				if (acc.hasOwnProperty(key)) {
					// if the current key is already an array, we push the value to it
					if (Array.isArray(acc[key])) {
						acc[key] = [...acc[key], val];
					} else {
						// if it's not an array, we will convert it into an array and add the current value to it
						acc[key] = [acc[key], val];
					}
				} else if (val !== '') {
					// plain assignment if no special case is present
					acc[key] = val;
				}

				return acc;
			}, {});
		const paramsToObject = (params) => {
			try {
				const urlParams = new URLSearchParams(params);
				const paramsObj = groupParamsByKey(urlParams);
				return paramsObj;
			} catch (e) {
				console.log(e);
				return {};
			}
		};

		const params = paramsToObject(search);
		console.log(params);
        setParams(params);
	}, [search]);

    const handlePage = (pagina:number) =>{
        params.page = pagina;
        const uri = Object.entries(params)
			.map(([k, v]) => `${k}=${encodeURIComponent(v)}`)
			.join('&');
		window.location.href = `/rooms?${uri}`;
    }
	return (
		<Pagination className='justify-content-end m-0'>
			{props?.pagination?.page > 1 && (
				<>
					<Pagination.First onClick={()=>handlePage(1)} />
					<Pagination.Prev onClick={()=>handlePage(props?.pagination?.page - 1)} />
				</>
			)}
			{props?.pagination?.page - 2 > 0 && (
				<Pagination.Item onClick={()=>handlePage(props?.pagination?.page-2)}>{props?.pagination?.page - 2}</Pagination.Item>
			)}
			{props?.pagination?.page - 1 > 0 && (
				<Pagination.Item onClick={()=>handlePage(props?.pagination?.page-1)}>{props?.pagination?.page - 1}</Pagination.Item>
			)}
			{props?.pagination?.page && (
				<Pagination.Item active>{props?.pagination?.page }</Pagination.Item>
			)}
			{props?.pagination?.page + 1 <= props?.pagination?.total_pages && (
				<Pagination.Item onClick={()=>handlePage(props?.pagination?.page + 1)}>{props?.pagination?.page + 1}</Pagination.Item>
			)}
			{props?.pagination?.page + 2 <= props?.pagination?.total_pages && (
				<Pagination.Item onClick={()=>handlePage(props?.pagination?.page + 2)}>{props?.pagination?.page + 2}</Pagination.Item>
			)}
			{props?.pagination?.page < props?.pagination?.total_pages && (
				<>
					<Pagination.Next onClick={()=>handlePage(props?.pagination?.page + 1)} />
					<Pagination.Last onClick={()=>handlePage(props?.pagination?.total_pages)} />
				</>
			)}
		</Pagination>
	);
}
