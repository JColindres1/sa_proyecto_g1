import { IonSearchbar } from '@ionic/react';
import React from 'react';

export default function SearchBar(props: any) {
	return (
		<IonSearchbar
			value={props?.searchText}
			onIonChange={(e) => props?.setSearchText(e.detail.value!)}
			debounce={1000}
			type={props?.mode || 'search'}
			placeholder={props?.placeholder || 'Search'}></IonSearchbar>
	);
}
