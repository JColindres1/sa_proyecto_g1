import React, { useEffect, useState } from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';
import Loading from '../Loading/Loading';
import axios from 'axios';
import { API_REPORTES } from '../../helpers/endpoint';

ChartJS.register(ArcElement, Tooltip, Legend);

export default function PieChart(props: any) {
	const [data, setData] = useState({
		labels: [],
		datasets: [
			{
				label: '# of Users',
				data: [],
				backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(255, 159, 64, 0.2)',
				],
				borderColor: [
					'rgba(255, 99, 132, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)',
				],
				borderWidth: 1,
			},
		],
	});
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		setLoading(true);
		axios
			.get(`${API_REPORTES}${props.reporte}`)
			.then((res) => res.data)
			.then((res) => {
				setData({
					...data,
					labels: res.map(
						(item: any) => item?.nombre || item?.ciudad
					),
					datasets: [
						{
							...data.datasets[0],
							data: res.map(
								(item: any) =>
									item?.autos_rentados || item?.boletos
							),
						},
					],
				});
			})
			.catch(handleError)
			.finally(() => setLoading(false));
	}, [props.reporte]);

	const handleError = (error: any) => {
		if (error?.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error?.response.data);
			console.log(error?.response.status);
			console.log(error?.response.headers);
		} else if (error?.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error?.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error);
			console.log('Error', error?.message);
		}
		console.log(error?.config);
		setLoading(false);
	};

	return (
		<>
			{loading && <Loading name='crescent' />}
			{data?.datasets[0]?.data?.length > 0 && <Pie data={data} />}
		</>
	);
}
