import React from 'react';
import { Button } from 'react-bootstrap';
import { NavLink, useHistory } from 'react-router-dom';
import { FaEye, FaPencilAlt, FaTrashAlt } from 'react-icons/fa';
import swal from 'sweetalert';
import axios from 'axios';
import { API_HOTEL } from '../../helpers/endpoint';
export default function RoomItemList(props: any) {
	let userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo'))
		: null;
	const history = useHistory();

	const deleteItem = (habitacion_id: number) => {
		swal({
			title: 'Seguro que desea eliminar?',
			text: 'Esta acción no se puede repetir',
			icon: 'info',
			buttons: [true, 'Eliminar'],
		}).then((value) => {
			if (value) {
				axios
					.delete(`${API_HOTEL}`, {
						data: { habitacion_id },
					})
					.then((res) => res.data)
					.then((res) => {
						if (res?.success) {
							swal('Eliminado!', res?.message, 'success').then(
								() => window.location.reload()
							);
						} else {
							swal('Error al eliminar!', res?.message, 'success');
						}
					})
					.then(handleError);
			}
		});
	};

	const handleError = (error: any) => {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error.response.data);
			console.log(error.response.status);
			console.log(error.response.headers);
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error.message);
		}
		console.log(error.config);
	};

	return (
		<div className='d-flex flex-row w-100 justify-content-between border-top border-bottom p-4'>
			<div className='d-flex flex-column w-75 w-md-50 align-items-start justify-content-start'>
				<div className='text-start'>
					<NavLink to={`/rooms/view/${props?.room?.habitacion_id}`}>
						<strong>{props?.room?.nombre}</strong>
					</NavLink>
				</div>
				<div className='text-start'>
					Cantidad máxima de personas:{' '}
					{props?.room?.cantidad_personas}
				</div>
				<div className='text-start'>
					País: {props?.room?.nombre_pais}
				</div>
				<div className='text-start'>
					Ciudad: {props?.room?.nombre_ciudad}
				</div>
				<div className='text-start'>{props?.room?.descripcion}</div>
			</div>
			<div className='d-flex flex-column w-25 w-md-50 align-items-end justify-content-between'>
				<div>Q.{props?.room?.precio.toFixed(2)}</div>
				<div className='d-flex flex-column flex-md-row w-100 justify-content-center justify-content-md-end align-items-end align-items-md-center'>
					<div className='ms-2'>
						<Button
							variant='info'
							onClick={() =>
								history.push(
									`/rooms/view/${props?.room?.habitacion_id}`
								)
							}>
							<FaEye style={{ fontSize: '1.2rem' }} />
						</Button>
					</div>
					{userInfo && [2, 3].includes(userInfo?.rol_id) && (
						<div className='ms-2'>
							<Button
								variant='warning'
								onClick={() =>
									history.push(
										`/rooms/update/${props?.room?.habitacion_id}`
									)
								}>
								<FaPencilAlt style={{ fontSize: '1.2rem' }} />
							</Button>
						</div>
					)}
					{userInfo && [2, 3].includes(userInfo?.rol_id) && (
						<div className='ms-2'>
							<Button
								variant='danger'
								onClick={() =>
									deleteItem(props?.room?.habitacion_id)
								}>
								<FaTrashAlt
									style={{
										fontSize: '1.2rem',
										color: 'black',
									}}
								/>
							</Button>
						</div>
					)}
				</div>
			</div>
		</div>
	);
}
