import React from 'react';
import './Estrellas.css';

export default function Estrellas(props: any) {
	const maxStars = 5;
	const starPercentage = (props?.stars / maxStars) * 100;
	const starPercentageRounded = Math.round(starPercentage);
	const StarStyles = () => {
		return {
			width: starPercentageRounded + '%',
		};
	};

	return (
		<div className='stars-gray'>
			<div className='stars-yellow' style={StarStyles()}></div>
		</div>
	);
}
