import { Form, Row, Col, Button } from 'react-bootstrap';
import {
	IonCard,
	IonCardHeader,
	IonCardTitle,
	IonCardContent,
	IonCardSubtitle,
	IonLabel,
	IonRange,
	IonItem,
	IonInput,
	IonButton,
	IonIcon,
	IonPopover,
	IonDatetime,
} from '@ionic/react';

import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import { format, parseISO } from 'date-fns';

import './CardComponent.css';
import { motion } from 'framer-motion';
import Loading from '../Loading/Loading';
import axios from 'axios';
import { API_HOTEL } from '../../helpers/endpoint';
import { calendar } from 'ionicons/icons';
const CardContent = (props) => {
	const [data, setData] = useState({
		paises_origen: [],
		paises_destino: [],
		ciudades_origen: [],
		ciudades_destino: [],
	});
	const [loading, setLoading] = useState({
		loading_paises_origen: false,
		loading_paises_destino: false,
		loading_ciudades_origen: true,
		loading_ciudades_destino: true,
	});

	useEffect(() => {
		console.log('entra');
		setLoading({
			...loading,
			loading_paises_origen: true,
			loading_paises_destino: true,
		});
		axios
			.get(`${API_HOTEL}/paises`)
			.then((res) => res.data)
			.then((res) => {
				if (res.success) {
					setData({
						...data,
						paises_origen: res.paises,
						paises_destino: res.paises,
					});
				} else {
					setData({ ...data, paises_origen: [], paises_destino: [] });
				}
			})
			.catch(handleError)
			.finally(() =>
				setLoading({
					...loading,
					loading_paises_origen: false,
					loading_paises_destino: false,
				})
			);
	}, []);

	const getCity = (pais, tipo) => {
		if (tipo === 'origen') {
			setLoading({
				...loading,
				loading_ciudades_origen: true,
			});
			axios
				.get(`${API_HOTEL}/ciudades/${pais}`)
				.then((res) => res.data)
				.then((res) => {
					if (res.success) {
						setData({ ...data, ciudades_origen: res.ciudades });
					} else {
						setData({ ...data, ciudades_origen: [] });
					}
				})
				.catch(handleError)
				.finally(() =>
					setLoading({
						...loading,
						loading_ciudades_origen: false,
					})
				);
		} else {
			setLoading({
				...loading,
				loading_ciudades_destino: true,
			});
			axios
				.get(`${API_HOTEL}/ciudades/${pais}`)
				.then((res) => res.data)
				.then((res) => {
					if (res.success) {
						setData({ ...data, ciudades_destino: res.ciudades });
					} else {
						setData({ ...data, ciudades_destino: [] });
					}
				})
				.catch(handleError)
				.finally(() =>
					setLoading({
						...loading,
						loading_ciudades_destino: false,
					})
				);
		}
	};

	const handleError = (error) => {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error.response.data);
			console.log(error.response.status);
			console.log(error.response.headers);
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error.message);
		}
		console.log(error.config);
	};

	const formatDate = (value) => {
		return format(parseISO(value), 'yyyy/MM/dd');
	};

	const changeRange = (e) => {
		props?.setData({
			...props.data,
			precio_menor: e?.detail?.value?.lower,
			precio_mayor: e?.detail?.value?.upper,
		});
	};

	return (
		<>
			<Form className='w-100 h-auto'>
				<Row className='text-start d-flex flex-row w-100 m-0 justify-content-start align-items-start'>
					<Col>
						<h1 className='profile_title'>
							<strong>{props.title}</strong>
						</h1>
					</Col>
				</Row>
				<Row className='text-center d-flex flex-row w-100 m-0 justify-content-between align-items-start'>
					<Col xs={12} md={6}>
						<div className='d-flex flex-column w-100'>
							<div className='d-flex flex-row justify-content-start align-items-center'>
								Pais Origen:
							</div>
							<Select
								className='basic-single'
								classNamePrefix='select'
								isLoading={loading.loading_paises_origen}
								isSearchable
								name='pais'
								value={
									data?.paises_origen
										?.filter(
											(pais) =>
												pais?.pais_id ===
												props.data.id_pais_origen
										)
										.map((pais) => ({
											value: pais?.pais_id,
											label: pais?.nombre_pais,
										}))[0] || { value: '', label: '' }
								}
								styles={{
									// Fixes the overlapping problem of the component
									menu: (provided) => ({
										...provided,
										zIndex: 9999,
									}),
								}}
								onChange={(pais) => {
									getCity(pais?.value, 'origen');
									props?.setData({
										...props.data,
										id_pais_origen: pais?.value,
									});
								}}
								options={data.paises_origen?.map((pais) => ({
									value: pais?.pais_id,
									label: pais?.nombre_pais,
								}))}
							/>
						</div>
					</Col>
					<Col xs={12} md={6}>
						<div className='d-flex flex-column w-100'>
							<div className='d-flex flex-row justify-content-start align-items-center'>
								Ciudad Origen:
							</div>
							<Select
								className='basic-single'
								classNamePrefix='select'
								isLoading={loading.loading_ciudades_origen}
								isClearable
								isSearchable
								isDisabled={!data.ciudades_origen?.length}
								name='pais'
								styles={{
									// Fixes the overlapping problem of the component
									menu: (provided) => ({
										...provided,
										zIndex: 9999,
									}),
								}}
								value={
									data?.ciudades_origen
										?.filter(
											(ciudad) =>
												ciudad?.ciudad_id ===
												props.data.id_ciudad_origen
										)
										.map((ciudad) => ({
											value: ciudad?.ciudad_id,
											label: ciudad?.nombre_ciudad,
										}))[0] || { value: '', label: '' }
								}
								onChange={(pais) =>
									props?.setData({
										...props?.data,
										id_ciudad_origen: pais?.value || 0,
									})
								}
								options={data.ciudades_origen?.map((pais) => ({
									value: pais?.ciudad_id,
									label: pais?.nombre_ciudad,
								}))}
							/>
						</div>
					</Col>
					<Col xs={12} md={6}>
						<div className='d-flex flex-column w-100'>
							<div className='d-flex flex-row justify-content-start align-items-center'>
								Pais Destino:
							</div>
							<Select
								className='basic-single'
								classNamePrefix='select'
								isLoading={loading.loading_paises_destino}
								isSearchable
								name='pais'
								styles={{
									// Fixes the overlapping problem of the component
									menu: (provided) => ({
										...provided,
										zIndex: 9999,
									}),
								}}
								value={
									data?.paises_destino
										?.filter(
											(pais) =>
												pais?.pais_id ===
												props.data.id_pais_destino
										)
										.map((pais) => ({
											value: pais?.pais_id,
											label: pais?.nombre_pais,
										}))[0] || { value: '', label: '' }
								}
								onChange={(pais) => {
									getCity(pais.value, 'destino');
									props?.setData({
										...props.data,
										id_pais_destino: pais?.value,
									});
								}}
								options={data.paises_destino?.map((pais) => ({
									value: pais?.pais_id,
									label: pais?.nombre_pais,
								}))}
							/>
						</div>
					</Col>
					<Col xs={12} md={6}>
						<div className='d-flex flex-column w-100'>
							<div className='d-flex flex-row justify-content-start align-items-center'>
								Ciudad Destino:
							</div>
							<Select
								className='basic-single'
								classNamePrefix='select'
								isLoading={loading.loading_ciudades_destino}
								isClearable
								isSearchable
								isDisabled={!data.ciudades_destino?.length}
								name='pais'
								styles={{
									// Fixes the overlapping problem of the component
									menu: (provided) => ({
										...provided,
										zIndex: 9999,
									}),
								}}
								value={
									data?.ciudades_destino
										?.filter(
											(ciudad) =>
												ciudad?.ciudad_id ===
												props.data.id_ciudad_destino
										)
										.map((ciudad) => ({
											value: ciudad?.ciudad_id,
											label: ciudad?.nombre_ciudad,
										}))[0] || { value: '', label: '' }
								}
								onChange={(pais) =>
									props?.setData({
										...props?.data,
										id_ciudad_destino: pais?.value || 0,
									})
								}
								options={data.ciudades_destino?.map((pais) => ({
									value: pais?.ciudad_id,
									label: pais?.nombre_ciudad,
								}))}
							/>
						</div>
					</Col>
					<Col xs={12}>
						<div className='d-flex flex-column w-100'>
							<IonLabel>
								Precio: min: {props?.data?.precio_menor} max:{' '}
								{props?.data?.precio_mayor}
							</IonLabel>
							<IonRange
								dualKnobs={true}
								value={{
									lower: props?.data?.precio_menor,
									upper: props?.data?.precio_mayor,
								}}
								min={0}
								max={10000}
								step={100}
								snaps={true}
								onIonChange={changeRange}
							/>
						</div>
					</Col>
					<Col xs={12} md={6}>
						<div className='d-flex flex-column w-100'>
							<IonLabel>Fecha Salida</IonLabel>
							<IonItem>
								<IonInput
									id='date-input-1'
									value={props?.data?.fecha_salida}
								/>
								<IonButton fill='clear' id='open-date-input-1'>
									<IonIcon icon={calendar} />
								</IonButton>
								<IonPopover
									trigger='open-date-input-1'
									showBackdrop={false}>
									<IonDatetime
										presentation='date'
										onIonChange={(ev) =>
											props?.setData({
												...props?.data,
												fecha_salida: formatDate(
													ev.detail.value
												),
											})
										}
									/>
								</IonPopover>
							</IonItem>
						</div>
					</Col>
					<Col xs={12} md={6} className='my-auto'>
						<div className='d-flex flex-row w-100 h-100 my-auto justify-content-end align-items-center'>
							<Button
								className='w-auto align-self-center'
								onClick={props?.search}>
								Buscar
							</Button>
						</div>
					</Col>
				</Row>

				<div className='d-flex flex-column w-100 m-0 justify-content-start align-items-center'>
					<Row className='text-start d-flex flex-row w-100 m-0 justify-content-start align-items-start'>
						<div className='card-list'>
							{props?.loading && <Loading name='crescent' />}
							{!props?.loading &&
								props.content.map((elemento, key) => (
									<IonCard className='custom-card' key={key}>
										<motion.div
											key={key}
											initial={{ opacity: 0, scale: 0 }}
											animate={{ opacity: 1, scale: 1 }}>
											<IonCardHeader>
												<IonCardTitle>
													{`Aerolinea: ${elemento.aerolinea}`}
												</IonCardTitle>
												<IonCardSubtitle>
													{`País: ${elemento.nombre_pais_origen} -> ${elemento.nombre_pais_destino}`}
													<br />
													{`Ciudad: ${elemento.nombre_ciudad_origen} -> ${elemento.nombre_ciudad_destino}`}
													<br />
													{`Capacidad: ${elemento.capacidad} personas`}
													<br />
													{`Precio: Q${elemento.precio}`}
													<br />
													{`Fecha Salida: ${elemento.salida}`}
												</IonCardSubtitle>
											</IonCardHeader>
										</motion.div>
										<IonCardContent>
											<Button
												variant='outline-primary'
												onClick={() => {
													props.get_reservation(
														elemento
													);
												}}>
												Solicitar reservacion
											</Button>
										</IonCardContent>
									</IonCard>
								))}
						</div>
					</Row>
				</div>
			</Form>
			<div className='d-flex flex-column w-100 m-0 justify-content-end align-items-center mt-5'></div>
		</>
	);
};

export default CardContent;
