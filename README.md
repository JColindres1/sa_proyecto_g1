# Proyecto - Fase 2

**UNIVERSIDAD DE SAN CARLOS DE GUATEMALA**<br>
**FACULTAD DE INGENIERÍA**<br>
**ESCUELA DE INGENIERÍA EN CIENCIAS Y SISTEMAS**<br>
**SOFTWARE AVANZADO**<br>
**SECCIÓN A**<br>

<br>
<br>
<p align="center"> 
  <img align="center" width="440px" src="Documentacion/images/logo_usac.svg" />
</p>

**Grupo # 1 - Estudiantes:**
| Nombre                             | Carné     |
|------------------------------------|-----------|
| JONATAN ALEJANDRO AZURDIA AJÚ | 201512786 |
| JOSÉ PABLO COLINDRES ORELLANA | 201602713 |
| KEVIN ESTUARDO CARDONA LÓPEZ | 201800596 |
| JUAN PABLO ESTRADA ALEMÁN | 201800709 |

**Catedrático:** ING. EVEREST DARWIN MEDINILLA RODRIGUEZ<br>
**Auxiliar:** SANDY FABIOLA MÉRIDA HERNÁNDEZ<br>
**Fecha:** 19/03/2021<br>

<br>
<br>
<br>

------------

# Documentación

[Documentación](./Documentacion)
