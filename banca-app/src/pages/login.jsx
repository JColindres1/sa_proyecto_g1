
import { Card,Form,Col,Row,Container,Button } from "react-bootstrap";
import axios from "axios";
import { useNavigate } from 'react-router-dom';
function Login()  {
    const navigate = useNavigate();

    const handleSubmit = async (event) =>{
        event.preventDefault();
        let data={
			Correo: event.target.elements.formBasicEmail.value,
			Contrasena: event.target.elements.formBasicPassword.value
		}
        console.log(data);
        let response = await axios
		.post(`http://35.202.37.198:4000/esb/users/login`, data)
        console.log(response);
        
        if(response.status == 200){
		    console.log(response.data.JWT)
			localStorage.setItem('bearer', response.data.JWT);
		}
        
        let userResponse = await axios.get(`http://35.202.37.198:4000/esb/users/login`)
        
        console.log(userResponse);
        if(userResponse.data.rol_id > -1){
        localStorage.setItem('userInfo', JSON.stringify(userResponse.data));
        console.log(localStorage.getItem('userInfo'))
        navigate('/register');
    }else{
        window.alert('Usuario no encontrado!')
    }
    }
	return (
		<>
			<Container>
				<h1 className='title-custom'>Login</h1>
				<Col sm={{ span: 6, offset: 3 }}>
					<Card className='login-card'>
						<div className='margin-login'>
							<Form onSubmit={handleSubmit}>
								<Form.Group
									className='mb-3'
									controlId='formBasicEmail'
									style={{ textAlign: 'start' }}>
									<Form.Label className='form-text'>
										Usuario:
									</Form.Label>
									<Form.Control
										type='email'
										placeholder='Enter email'
									/>
								</Form.Group>

								<Form.Group
									className='mb-3'
									controlId='formBasicPassword'
									style={{ textAlign: 'start' }}>
									<Form.Label className='form-text'>
										Password:
									</Form.Label>
									<Form.Control
										type='password'
										placeholder='Password'
									/>
								</Form.Group>
								<Button variant='primary' type='submit'>
									Login
								</Button>
								<Form.Group>
									<Form.Label className='form-sub-text'>
										No posees una cuenta? registrate{' '}
										<a href='/register'>aquí</a>
									</Form.Label>
								</Form.Group>
							</Form>
						</div>
					</Card>
				</Col>
			</Container>
		</>
	);
}

export default Login;

