
import { Card,Form,Col,Row,Container,Button } from "react-bootstrap";
import axios from "axios";

function Register()  {

    const handleSubmit = async (event) =>{
        event.preventDefault();
        let userInfo = localStorage.getItem('userInfo')
		? JSON.parse(localStorage.getItem('userInfo'))
		: null;
       let data = {
            "nombre": event.target.elements.nombre.value,
            "numero": event.target.elements.tarjeta.value,
            "cvv": parseInt(event.target.elements.cvv.value),
            "fecha": event.target.elements.fecha.value,
            "idUsuario": userInfo.id_usuario,
            "monto": parseInt(event.target.elements.monto.value)
        }
        console.log(data);
  
        console.log(userInfo.id_usuario);

        let response = axios.post(`http://35.202.37.198:4000/esb/banco/addtarjeta`,data)
        console.log(response);
    }
	return (
		<>
			<Container>
				<h1 className='title-custom'>Registro Tarjeta</h1>
				<Col sm={{ span: 6, offset: 3 }}>
					<Card className='login-card'>
						<div className='margin-login'>
							<Form onSubmit={handleSubmit}>
								<Form.Group
									className='mb-3'
									controlId='tarjeta'
									style={{ textAlign: 'start' }}>
									<Form.Label className='form-text'>
										No. Tarjeta:
									</Form.Label>
									<Form.Control
										type='text'
										placeholder='00000000000'
									/>
								</Form.Group>

								<Form.Group
									className='mb-3'
									controlId='nombre'
									style={{ textAlign: 'start' }}>
									<Form.Label className='form-text'>
										Nombre:
									</Form.Label>
									<Form.Control
										type='text'
										placeholder='Nombre'
									/>
								</Form.Group>

                                <Form.Group
									className='mb-3'
									controlId='cvv'
									style={{ textAlign: 'start' }}>
									<Form.Label className='form-text'>
										CVV:
									</Form.Label>
									<Form.Control
										type='number'
										placeholder='CVV'
									/>
                                    </Form.Group>
                                    <Form.Group
									className='mb-3'
									controlId='fecha'
									style={{ textAlign: 'start' }}>
									<Form.Label className='form-text'>
									   Fecha:
									</Form.Label>
									<Form.Control
										type='text'
										placeholder='Fecha'
									/>      
								</Form.Group>    
                                <Form.Group
									className='mb-3'
									controlId='monto'
									style={{ textAlign: 'start' }}>
									<Form.Label className='form-text'>
									   Monto:
									</Form.Label>
									<Form.Control
										type='text'
										placeholder='Monto'
									/>      
								</Form.Group>
								<Button variant='primary' type='submit'>
									Registrar 
								</Button>
							
							</Form>
						</div>
					</Card>
				</Col>
			</Container>
		</>
	);
}

export default Register;

