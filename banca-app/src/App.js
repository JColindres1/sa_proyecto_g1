import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from './pages/login';
import axios from 'axios';
import Register from './pages/FormRegister';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
axios.interceptors.request.use((request) => {
	console.log(request);
	let item = localStorage.getItem('bearer');
	console.log(item);
	if (item) {
		request.headers.common.Authentication = `${item}`;
	}
	return request;
});

function App() {
  return (
    <>
    <BrowserRouter>
    <Routes>
    <Route path="/" element={<Login />} />
    <Route path="/register" element={<Register/>} />
      </Routes>
  </BrowserRouter>,
  </>
  );
}

export default App;
